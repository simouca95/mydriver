//
//  PasswordViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/12/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import Toaster

class PasswordViewController: UIViewController {

    static let storyboardId = "PasswordViewController"

    var user : [String: Any]!

    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var activateButton: LoadingButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        do {
            try Auth.auth().signOut()
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }

    }

    @IBAction func signIn(_ sender: UIButton) {
        activateButton.loading = true
        if password.text!.count > 0 {
            
            self.activateButton.loading = true
            
            let credential = EmailAuthProvider.credential(withEmail: user["email"] as! String, password: password.text!)
            
            Auth.auth().signIn(with: credential) { (result, error) in
                
                self.activateButton.loading = false
                
                if let error = error {
                    // An error happened.
                    self.password.shake()
                    self.handleError(error)
                } else {
                    // User re-authenticated.
                    if let newToken = UserDefaults.standard.string(forKey: "RegistrationToken"){
                        User.updateDeviceToken(result!.user.uid , newToken)
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
                }
                
            }
        }else {
            password.shake()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
