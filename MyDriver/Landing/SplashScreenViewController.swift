//
//  SplashScreenViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FirebaseAuth
import CoreData
import FirebaseDatabase
import Alamofire
import SwiftyJSON
import GoogleSignIn
import FBSDKCoreKit
import FacebookLogin
import NotificationBannerSwift

class SplashScreenViewController: UIViewController {
    
    static let storyboardId = "SplashScreenViewController"
    
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    
    var window: UIWindow!
    var timer: Timer!
    var count = 0
    var firstLoad = true
    
    var reachability = Reachability()!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingView.color = .white
        self.loadingView.type = .semiCircleSpin
        self.loadingView.startAnimating()
        
        let userDefaults = UserDefaults.standard
        
        if !userDefaults.bool(forKey: "hasRunBefore") {//first time
            do {
                if Auth.auth().currentUser != nil {
                    try Auth.auth().signOut()
                }
                
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            
            // Update the flag indicator
            userDefaults.set(true, forKey: "hasRunBefore")
        }else {
            self.firstLoad = false
        }
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(openMainPage), userInfo: nil, repeats: false)
        
        self.fetchConfiguration()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func openMainPage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if Auth.auth().currentUser == nil  {
            
            self.loadingView.stopAnimating()
            
            self.timer.invalidate()
            
            if firstLoad{
                self.present(storyboard.instantiateViewController(withIdentifier: DemoViewController.storyboardId) as! DemoViewController, animated: true, completion: nil)
                
            }else{
                let introMain = storyboard.instantiateViewController(withIdentifier:"landingNVC") as! UINavigationController
                self.present(introMain, animated: true, completion: nil)
            }
        }else{
            reachability = Reachability()!
            do {
                try reachability.startNotifier()
            } catch {
                print("Unable to start notifier")
            }
            
            if reachability.isReachable {

                User.verifyValidUser(userId: Auth.auth().currentUser!.uid) { (snap) in
                    self.loadingView.stopAnimating()
                    
                    if snap!.hasChild("email"){
                        
                        if let newToken = UserDefaults.standard.string(forKey: "RegistrationToken"){
                            
                            let userDict = snap!.value as! [String: Any]
                            let oldDeviceToken = userDict["deviceToken"] as! String
                            if oldDeviceToken == newToken {
                                // signedIn
                                self.timer.invalidate()
                                
                                Auth.auth().currentUser!.reload(completion: { (error) in
                                    if let error = error {
                                        print(error.localizedDescription)
                                        self.handleError(error)
                                        self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController , animated: true, completion: nil)

                                    }else {
                                        if !Auth.auth().currentUser!.isEmailVerified{
                                            let banner = NotificationBanner(title: "Email Verification", subtitle: "Please check your email.", style: .warning)
                                            banner.show()
                                        }
                                        self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController , animated: true, completion: nil)

                                    }

                                })
                                
                            }else {
                                do {
                                    try Auth.auth().signOut()
                                    
                                    GIDSignIn.sharedInstance().signOut()
                                    
                                    if let _ = AccessToken.current {
                                        // User is logged in, use 'accessToken' here.
                                        let loginManager = LoginManager()
                                        loginManager.logOut()
                                    }


                                    self.timer.invalidate()
                                    
                                    
                                    let introMain = storyboard.instantiateViewController(withIdentifier:"landingNVC") as! UINavigationController
                                    self.present(introMain, animated: true, completion: nil)
                                    
                                } catch let signOutError as NSError {
                                    print ("Error signing out: %@", signOutError)
                                }
                            }
                        }
                        
                    }else {
                        
                        do {
                            try Auth.auth().signOut()
                            
                            GIDSignIn.sharedInstance().signOut()
                            
                            if let _ = AccessToken.current {
                                // User is logged in, use 'accessToken' here.
                                let loginManager = LoginManager()
                                loginManager.logOut()
                            }
                            
                            self.timer.invalidate()
                            
                            if self.firstLoad{
                                
                                self.present(storyboard.instantiateViewController(withIdentifier: DemoViewController.storyboardId) as! DemoViewController, animated: true, completion: nil)
                                
                            }else {
                                
                                let introMain = storyboard.instantiateViewController(withIdentifier:"landingNVC") as! UINavigationController
                                self.present(introMain, animated: true, completion: nil)
                            }
                            
                        } catch let signOutError as NSError {
                            print ("Error signing out: %@", signOutError)
                        }
                        
                    }
                }
            }else {
                print("Not reachable")
                self.loadingView.stopAnimating()
                self.timer.invalidate()
                self.showAlertView(isOneButton: true, title: NSLocalizedString("Please verify your internet connexion", comment: ""), cancelButtonTitle: NSLocalizedString("Please retry", comment: ""), submitButtonTitle: "",cancelHandler:{
                    self.loadingView.startAnimating()
                    self.fetchConfiguration()
                })
                //self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController , animated: true, completion: nil)
            }
        }
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func fetchConfiguration(){
        Alamofire.request("https://mydriverdb.firebaseapp.com/api/update", method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(_):
                print(response.description)
                self.openMainPage()
                break;
            case .success(let value):
                let json = JSON(value)
                
                if  json.isEmpty{
                    print("no data available")
                    self.openMainPage()
                    break
                }
                
                self.configImported(config: json)
                
            }
        }
    }
    
    func configImported(config: JSON){
        
        if !config.exists() {
            openMainPage()
            return
        }
        self.loadingView.stopAnimating()
        
        if config["isInBreak"].bool != nil {
            let ios_maintenance_mode : Bool = config["isInBreak"].boolValue
            print("maintenance mode: ", ios_maintenance_mode)
            //ios_maintenance_mode = false
            if ios_maintenance_mode {
                
                self.timer.invalidate()
                
                self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: MaintenanceViewController.storyboardId) as! MaintenanceViewController, animated: true, completion: nil)
                
            }else if let _ = config["ios"].dictionary{
                let iosVersion = config["ios"].dictionaryValue
                if iosVersion["latestVersion"]?.string != nil {
                    let ios_latest_version : String = iosVersion["latestVersion"]!.stringValue
                    print("latest version: ", ios_latest_version)
                    if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String  , let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String{
                        let current_ios_version = version + "." + build
                        
                        print("version: ", version)
                        print("build : ", build)
                        print("current_ios_version", current_ios_version)
                        
                        if Float(current_ios_version.replacingOccurrences(of: ".", with: ""))! < Float(ios_latest_version.replacingOccurrences(of: ".", with: ""))! {
                            if iosVersion["isOptional"]?.bool != nil {
                                
                                let ios_upgrade_mandatory : Bool = iosVersion["isOptional"]!.boolValue
                                print("ios_upgrade_mandatory: ", ios_upgrade_mandatory)
                                let upgradeVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UpgradeViewController.storyboardId) as! UpgradeViewController
                                upgradeVc.isUpgradeMondatory = !ios_upgrade_mandatory
                                self.timer.invalidate()
                                self.present(upgradeVc, animated: true, completion: nil)
                            }else {
                                let upgradeVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UpgradeViewController.storyboardId) as! UpgradeViewController
                                upgradeVc.isUpgradeMondatory = false
                                self.timer.invalidate()
                                self.present(upgradeVc, animated: true, completion: nil)
                            }
                        }
                        else {
                            openMainPage()
                        }
                        
                    }
                    else {
                        openMainPage()
                    }
                }
                else {
                    openMainPage()
                }
            }else {
                openMainPage()
            }
        }else if let _ = config["ios"].dictionary{
            let iosVersion = config["ios"].dictionaryValue
            if iosVersion["latestVersion"]?.string != nil {
                let ios_latest_version : String = iosVersion["latestVersion"]!.stringValue
                print("latest version: ", ios_latest_version)
                if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String  , let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String{
                    let current_ios_version = version + "." + build
                    
                    print("version: ", version)
                    print("build : ", build)
                    print("current_ios_version", current_ios_version)
                    
                    if Float(current_ios_version.replacingOccurrences(of: ".", with: ""))! < Float(ios_latest_version.replacingOccurrences(of: ".", with: ""))! {
                        if iosVersion["isOptional"]?.bool != nil {
                            
                            let ios_upgrade_mandatory : Bool = iosVersion["isOptional"]!.boolValue
                            print("ios_upgrade_mandatory: ", ios_upgrade_mandatory)
                            let upgradeVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UpgradeViewController.storyboardId) as! UpgradeViewController
                            upgradeVc.isUpgradeMondatory = !ios_upgrade_mandatory
                            self.timer.invalidate()
                            
                            self.present(upgradeVc, animated: true, completion: nil)
                        }else {
                            let upgradeVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UpgradeViewController.storyboardId) as! UpgradeViewController
                            upgradeVc.isUpgradeMondatory = false
                            self.timer.invalidate()
                            
                            self.present(upgradeVc, animated: true, completion: nil)
                        }
                    }
                    else {
                        openMainPage()
                    }
                    
                }
                else {
                    openMainPage()
                }
            }
            else {
                openMainPage()
            }
        }else {
            openMainPage()
        }
    }
}
