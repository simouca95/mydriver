//
//  ActivationViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import Toaster
import NotificationBannerSwift
import SwiftyJSON

class ActivationViewController: UIViewController {
    static let storyboardId = "ActivationViewController"
    var verificationID : String?
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var textfield2: UITextField!
    @IBOutlet weak var textfield3: UITextField!
    @IBOutlet weak var textfield4: UITextField!
    @IBOutlet weak var textfield5: UITextField!
    @IBOutlet weak var textfield6: UITextField!
    
    @IBOutlet weak var activateButton: LoadingButton!
    
    var ref: DatabaseReference!
    var timer : Timer!
    // This test verification code is specified for the given test phone number in the developer console.
    var testVerificationCode = "000000"
    var phoneNumberStr = "+21687555555"
    var value = 59
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        
        self.ref = Database.database(url: CURRENT_DB_REF).reference()
        textfield1.becomeFirstResponder()
        
        textfield1.delegate = self
        textfield2.delegate = self
        textfield3.delegate = self
        textfield4.delegate = self
        textfield5.delegate = self
        textfield6.delegate = self

        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimerLaber)), userInfo: nil, repeats: true)
        
    }
    @objc func updateTimerLaber(){
        DispatchQueue.main.async {
            self.timerLabel.text = String(format:"%02i:%02i", self.value / 60, self.value % 60) //This will update the label.
        }
        value -= 1
        if value == 0 {
            timer.invalidate()
            resendButton.isEnabled = true
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendSMS(_ sender: Any) {
        self.showSpinner(onView: self.view)

            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumberStr, uiDelegate: nil) { (verificationID, error) in
                self.removeSpinner()
                if let error = error {
                    print(error.localizedDescription)
                    self.handleError(error)
                    return
                }
                // Sign in using the verificationID and the code sent to the user
                // ...
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                
                self.resendButton.isEnabled = false
                
                self.value = 59
                
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimerLaber)), userInfo: nil, repeats: true)
                
            }
    }
    
    @IBAction func activate(_ sender: Any) {
        
        let activationCode = "\(textfield1.text!)\(textfield2.text!)\(textfield3.text!)\(textfield4.text!)\(textfield5.text!)\(textfield6.text!)"
        
        verificationID = UserDefaults.standard.string(forKey: "authVerificationID")

        if activationCode.count == 6 {
            activateButton.loading = true
            
            activateButton.isEnabled = false

            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID!,
                verificationCode: activationCode)
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let error = error {
                    print(error.localizedDescription)
                    self.handleError(error)
                    self.activateButton.loading = false
                    return
                }
                User.verifyValidUser(userId: authResult!.user.uid, completionBlock: { (snap) in
                    
                    if snap!.hasChild("email"){
                        self.activateButton.loading = false
                        
                        if let newToken = UserDefaults.standard.string(forKey: "RegistrationToken"){
                            
                            let userDict = snap!.value as! [String: Any]
                            let oldDeviceToken = userDict["deviceToken"] as! String
                            if oldDeviceToken == newToken {
                                // signedIn
                                
                                self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
                                
                            }else {
                                
                                let passwordVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: PasswordViewController.storyboardId) as! PasswordViewController
                                passwordVC.user = userDict
                                self.present(passwordVC, animated: true, completion: nil)
                            }
                        }else {
                            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
                        }
                    }else{
                        self.activateButton.loading = false
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: RegistrationTableViewController.storyboardId) as! RegistrationTableViewController
                        vc.isPhoneRegistration = true
                        self.present(vc, animated: true, completion: nil)
                    }
                })
            }
        }else {
            let banner = NotificationBanner(title: NSLocalizedString("Please check the code", comment: ""), subtitle: "", style: .danger)
            banner.show()
        }
        //}
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editingChanged(_ sender: UITextField) {
        print("editing changed")
        if sender == textfield1{
            if sender.text!.count >= 1 {
                textfield2.becomeFirstResponder()
            }
        }
        if sender == textfield2{
            if sender.text!.count >= 1 {
                textfield3.becomeFirstResponder()
            }else if sender.text?.count == 0{
                textfield1.becomeFirstResponder()
            }
        }
        if sender == textfield3{
            if sender.text!.count >= 1 {
                textfield4.becomeFirstResponder()
            }else if sender.text?.count == 0{
                textfield2.becomeFirstResponder()
            }
        }
        if sender == textfield4{
            if sender.text!.count >= 1 {
                textfield5.becomeFirstResponder()
            }else if sender.text?.count == 0{
                textfield3.becomeFirstResponder()
            }
        }
        if sender == textfield5{
            if sender.text!.count >= 1 {
                textfield6.becomeFirstResponder()
            }else if sender.text?.count == 0{
                textfield4.becomeFirstResponder()
            }
        }
        if sender == textfield6{
            if sender.text!.count >= 1 {
                textfield6.resignFirstResponder()
            }else if sender.text?.count == 0{
                textfield5.becomeFirstResponder()
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ActivationViewController : UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("textFieldShouldClear")
        if textField == textfield1{
            if textField.text?.count == 0 {
                textfield1.becomeFirstResponder()
            }
        }
        if textField == textfield2{
            if textField.text?.count == 0{
                textfield1.becomeFirstResponder()
                textfield1.text = ""
            }
        }
        if textField == textfield3{
            if textField.text?.count == 0{
                textfield2.becomeFirstResponder()
                textfield2.text = ""

            }
        }
        if textField == textfield4{
            if textField.text?.count == 0{
                textfield3.becomeFirstResponder()
                textfield3.text = ""

            }
        }
        if textField == textfield5{
            if textField.text?.count == 0{
                textfield4.becomeFirstResponder()
                textfield4.text = ""

            }
        }
        if textField == textfield6{
            if textField.text?.count == 0{
                textfield5.becomeFirstResponder()
                textfield5.text = ""

            }
        }
        return true
    }
}
