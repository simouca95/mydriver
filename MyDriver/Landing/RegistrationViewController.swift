//
//  RegistrationViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import MobileCoreServices
import FirebaseStorage
import Toaster

class RegistrationViewController: UIViewController {
    
    static let storyboardId = "RegistrationViewController"
    
    @IBOutlet weak var editPicture: UIImageView!
    
    @IBOutlet weak var activateButton: LoadingButton!
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var userPicture: UIImageView!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var username: UITextField!
    var ref: DatabaseReference!
    var imagePickerController: UIImagePickerController!
    
    var uploadPhoto = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        
        // Do any additional setup after loading the view.
        editPicture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.editUserPicture)))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openTermsWebView(_ sender: Any) {
        let webViewVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: LegalWebViewViewController.storyboardId) as! LegalWebViewViewController
        
        let use = NSLocalizedString("Conditions of use", comment: "")
        
        let arabicTerms = "https://mydriverkw.com/baec6461b0d69dde1b861aefbe375d8a/my_driver_legal_arabic/"
        
        webViewVC.titleVc = use
        webViewVC.fromRegistartion = true
        
        if Languages.currentLanguage() == "ar" {
            if arabicTerms.isValidURL(){
                webViewVC.content = arabicTerms
            }
        }else if Languages.currentLanguage() == "fr" {
            if arabicTerms.isValidURL(){
                webViewVC.content = arabicTerms
            }
        }else {
            if arabicTerms.isValidURL(){
                webViewVC.content = arabicTerms
            }
        }
         self.present(webViewVC, animated: true)
    }
    
    @IBAction func register(_ sender: Any) {
        if !username.text!.isBlank ,  !lastName.text!.isBlank{
            if userEmail.text!.replacingOccurrences(of: " ", with: "").isEmail{
                activateButton.loading = true
                let userId = Auth.auth().currentUser!.uid
                let phoneNum = Auth.auth().currentUser!.phoneNumber
                let email = userEmail.text!.replacingOccurrences(of: " ", with: "")
                if uploadPhoto {
                    var data = Data()
                    data = UIImageJPEGRepresentation(userPicture.image!, 0.8)!
                    let metaData = StorageMetadata()
                    metaData.contentType = "image/jpg"
                    let storageRef = Storage.storage().reference()
                    let profileImagesRef = storageRef.child("ProfilePictures/\(Date.timeIntervalSinceReferenceDate)")
                    
                    
                    // Upload the file to the path "images/rivers.jpg"
                    profileImagesRef.putData(data, metadata: metaData) { (metaData, error) in
                        if let error = error {
                            self.activateButton.loading = false
                            print(error.localizedDescription)
                            self.handleError(error)
                            return
                        }else{
                            // You can also access to download URL after upload.
                            profileImagesRef.downloadURL { (url, error) in
                                guard let downloadURL = url else {
                                    // Uh-oh, an error occurred!
                                    self.activateButton.loading = false
                                    
                                    self.handleError(error!)
                                    
                                    return
                                }
                                
                                let credential = EmailAuthProvider.credential(withEmail: email, password: self.password.text!)
                                
                                Auth.auth().currentUser!.linkAndRetrieveData(with: credential) { (authResult, error) in
                                    if let error = error {
                                        self.activateButton.loading = false
                                        
                                        print(error.localizedDescription)
                                        self.handleError(error)
                                        return
                                    }
                                    let user = User(id: userId, email: email, firstname: self.username.text!, lastname: self.lastName.text!, phoneNumber: phoneNum! , pictureURL : downloadURL)
                                    
                                    self.ref.child("User").child(userId).setValue(user.toDictionary())
                                    
                                    if let newToken = UserDefaults.standard.string(forKey: "RegistrationToken"){
                                        
                                        self.ref.child("User").child(userId).child("deviceToken").setValue(newToken)
                                        
                                    }else{
                                        
                                        self.ref.child("User").child(userId).child("deviceToken").setValue("")
                                        
                                    }
                                    self.activateButton.loading = false
                                    
                                    self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
                                }
                            }
                            
                        }
                    }
                }else{
                    
                    let credential = EmailAuthProvider.credential(withEmail: email, password: self.password.text!)
                    
                    Auth.auth().currentUser!.linkAndRetrieveData(with: credential) { (authResult, error) in
                        if let error = error {
                            self.activateButton.loading = false
                            
                            print(error.localizedDescription)
                            self.handleError(error)
                            return
                        }
                        
                        let user = User(id: userId, email: email, firstname: self.username.text!, lastname: self.lastName.text!, phoneNumber: phoneNum!)
                        
                        self.ref.child("User").child(userId).setValue(user.toDictionary())
                        
                        self.activateButton.loading = false
                        
                        self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
                    }
                }
            }else{
                userEmail.shake()
            }
        }else{
            if username.text!.isBlank {
                username.shake()
            }
            if lastName.text!.isBlank {
                lastName.shake()
            }
        }
    }
    
    @objc func editUserPicture ( ){
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("TakePhoto", comment:"TakePhoto"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("FromLibrary", comment:"FromLibrary"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = .photoLibrary
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.view.tintColor = UIColor.gray
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment:"Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}

extension RegistrationViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let mediaType = (info[UIImagePickerControllerMediaType] as! String)
        
        if mediaType == "public.image" {
            
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                self.editPicture.image = image
                self.uploadPhoto = true
            }
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}
