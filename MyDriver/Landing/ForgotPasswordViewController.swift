//
//  ForgotPasswordViewController.swift
//  MyDriver
//
//  Created by sami hazel on 8/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import NotificationBannerSwift

class ForgotPasswordViewController: UIViewController {

    static let storyboardId = "ForgotPasswordViewController"
    
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var resetButton: LoadingButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func reset(_ sender: Any) {
        if !email.text!.isBlank , email.text!.isEmail{
            Auth.auth().sendPasswordReset(withEmail:email.text! ) { error in
                if let error = error {
                    print(error.localizedDescription)
                    self.handleError(error)
                    return
                }
                let banner = NotificationBanner(title: "Reset password", subtitle: "Please check your email", style: .success)
                banner.show()
                self.navigationController?.popViewController(animated: true)
            }
        }else {
            self.email.shake()
            self.showAlertView(isOneButton: true, title: "Invalid email address", cancelButtonTitle: "Cancel", submitButtonTitle: "",cancelHandler: nil)
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
