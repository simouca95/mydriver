//
//  MainSignUpViewController.swift
//  MyDriver
//
//  Created by sami hazel on 8/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FacebookLogin
import FacebookCore
import FBSDKCoreKit

class MainSignUpViewController: UIViewController {
    
    
    @IBOutlet weak var googleBtn: LoadingButton!
    @IBOutlet weak var facebookBtn: LoadingButton!
    
    var userReg = User()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        if let accessToken = AccessToken.current{
            print("user facebook already logged in")
            print(accessToken.tokenString)
        }
    }

    @IBAction func googleLogIn(_ sender: Any) {
        self.googleBtn.loading = true

        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func facebookLogIn(_ sender: Any) {
        let loginManager = LoginManager()
        
        self.facebookBtn.loading = true
        
        loginManager.logIn(permissions: [ .publicProfile,.email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
                self.facebookBtn.loading = false

            case .cancelled:
                print("User cancelled login.")
                self.facebookBtn.loading = false

            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                self.firebaseLoginWithFacebook(accessToken: accessToken.tokenString)
            }
        }
    }
    
    func firebaseLoginWithFacebook(accessToken : String)  {
        GraphRequest(graphPath: "/me", parameters: ["fields": "id, email , first_name , last_name, picture.type(large)"]).start { httpResponse, result, error  in
            if error != nil {
                self.facebookBtn.loading = false
                print("Failed to start graph request:", error!)
                return
            }
            let userDict = result as! NSDictionary
            print(userDict)

            self.userReg.email = userDict.value(forKey: "email") as? String
            self.userReg.firstName = userDict.value(forKey: "first_name") as? String
            self.userReg.lastName = userDict.value(forKey: "last_name") as? String
            self.userReg.pictureURL = ((userDict.value(forKey: "picture") as? NSDictionary)?.value(forKey: "data") as? NSDictionary)?.value(forKey: "url") as? String
            let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
            self.signInFirebase(with: credential)
        }

    }
    
    func signInFirebase(with credential : AuthCredential) {
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                self.facebookBtn.loading = false
                self.googleBtn.loading = false

                print(error.localizedDescription)
                return
            }
            // User is signed in
            // ...
            
            print(authResult!.user.uid)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            User.verifyValidUser(userId: authResult!.user.uid) { (snap) in
                
                self.facebookBtn.loading = false
                self.googleBtn.loading = false

                if snap!.hasChild("email"){
                    
                    if let newToken = UserDefaults.standard.string(forKey: "RegistrationToken"){
                        User.updateDeviceToken(authResult!.user.uid , newToken)
                    }
                    
                    self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController , animated: true, completion: nil)
                    
                }else {
                    let regVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: RegistrationTableViewController.storyboardId) as! RegistrationTableViewController
                    regVC.signedUser = self.userReg
                    regVC.isSocialRegistration = true
                    self.present(regVC, animated: true, completion: nil)

                }
            }
            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
extension MainSignUpViewController : GIDSignInUIDelegate{
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        //self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension MainSignUpViewController :  GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            // ...
            self.googleBtn.loading = false
            print(error.localizedDescription)
            return
        }
        print("appdelagate didSignInFor signIn delegate")
        
        guard let authentication = user.authentication else { return }
        
        userReg.email = user.profile.email
        userReg.firstName = user.profile.givenName
        userReg.lastName = user.profile.familyName
        userReg.pictureURL = user.profile.imageURL(withDimension: 150)?.absoluteString ?? ""
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        self.signInFirebase(with: credential)
        // ...
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
        print("appdelagate didDisconnectWith signIn delegate")
        
    }
    
}
