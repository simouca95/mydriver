//
//  PhoneNumberViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import STPopup
import FirebaseAuth
import NotificationBannerSwift

class PhoneNumberViewController: UIViewController {
    
    static let storyboardId = "PhoneNumberViewController"
    
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryFlag: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var phoneNumber: UITextField!
    
    let phoneNumberStr = "+21687555555"
    
    // This test verification code is specified for the given test phone number in the developer console.
    let testVerificationCode = "000000"
    
    var countryCode: String!
    var countrySelector: STPopupController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //init coutry code and flag
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            self.countryFlag.text = countryCode.emojiFlag()
            self.countryLabel.text = countryFromCountryCode(code: countryCode).dialCode

        }else{
            self.countryFlag.text = "KW".emojiFlag()
            self.countryLabel.text = countryFromCountryCode(code: "KW").dialCode
        }
        
        let countryVC = CountrySelectorViewController(nibName: "CountrySelectorViewController", bundle: nil)
        countryVC.delegate = self
        self.countrySelector = STPopupController(rootViewController: countryVC)
        self.countryFlag.isUserInteractionEnabled = true
        self.countryFlag.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectCountry(_:))))

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.transparentNavigationBar()
        
        shadowView.shadowWithCornder(raduis: 3, shadowColor: UIColor.black.cgColor, shadowOffset: .zero, shadowOpacity: 0.5, shadowRadius: 2)
        
    }
    
//    @IBAction func toEmailLogIn(_ sender: UIButton) {
//        presentVC(vc: LoginEmailViewController.storyboardId, self: self)
//    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func toActivation(_ sender: Any) {
        //Auth.auth().settings!.isAppVerificationDisabledForTesting = true

        let phoneNum = (countryLabel.text! + phoneNumber.text!).replacingOccurrences(of: " ", with: "")//phoneNumberStr//
        if phoneNum.isPhoneNumber{
            self.showSpinner(onView: self.view)
//                            let stb = UIStoryboard(name: "Main", bundle: nil)
//                            let introMain = stb.instantiateViewController(withIdentifier: ActivationViewController.storyboardId) as! ActivationViewController
//                            introMain.phoneNumberStr = phoneNum
//            introMain.testVerificationCode = "000000"
//                            introMain.modalPresentationStyle = .custom
//                            introMain.modalTransitionStyle = .crossDissolve
//                            self.present(introMain, animated: true, completion: nil)
            
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationID, error) in
                self.removeSpinner()
                if let error = error {
                    print(error.localizedDescription)
                    self.handleError(error)
                    return
                }
                // Sign in using the verificationID and the code sent to the user
                // ...
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")

                let stb = UIStoryboard(name: "Main", bundle: nil)
                let introMain = stb.instantiateViewController(withIdentifier: ActivationViewController.storyboardId) as! ActivationViewController
                introMain.phoneNumberStr = phoneNum
                self.navigationController?.pushViewController(introMain, animated: true)
                //self.present(introMain, animated: true, completion: nil)
            }
        }else{
            phoneNumber.shake()
            let banner = NotificationBanner(title: NSLocalizedString("Not a valid phone number", comment: ""), subtitle : "", style: .danger)
            banner.show()
        }
    }
    
    @objc func selectCountry(_ sender: UITapGestureRecognizer){
        self.countrySelector.present(in: self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
extension PhoneNumberViewController: CountrySelectorDelegate{
    
    func didSelectCountry(country: Country, viewController: UIViewController) {
        
        viewController.dismiss(animated: true) {
            
            self.countryFlag.text = country.code.emojiFlag()
            
            self.countryLabel.text = country.dialCode
            
        }
    }
}
