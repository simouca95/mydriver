//
//  RegistrationTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 6/12/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import MobileCoreServices
import FirebaseStorage
import Toaster
import Alamofire
import GoogleSignIn
import STPopup
import NotificationBannerSwift

class RegistrationTableViewController: UITableViewController {
    static let storyboardId = "RegistrationTableViewController"
    
    @IBOutlet weak var editPicture: UIImageView!
    
    @IBOutlet weak var activateButton: LoadingButton!
    
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryFlag: UILabel!
    var countrySelector: STPopupController!
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var userPicture: UIImageView!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var referencedBy: UITextField!
    
    @IBOutlet weak var acceptSwitch: UISwitch!
    @IBOutlet weak var validReference: UIImageView!
    
    var imagePickerController: UIImagePickerController!
    
    var referralCode : String!
    var isReferrencedBy = false
    
    var uploadPhoto = false
    
    var profilePictureTitle : String!
    
    var signedUser : User!
    
    var isEmailRegistration = false
    var isPhoneRegistration = false
    var isSocialRegistration = false
    
    var spinnerView : UIView!
    var ai : UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.getReferralCode()
        validReference.isHidden = true
        
        // Do any additional setup after loading the view.
        editPicture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.editUserPicture)))
        self.userPicture.layer.cornerRadius = 60
        self.userPicture.clipsToBounds = true
        
        let countryVC = CountrySelectorViewController(nibName: "CountrySelectorViewController", bundle: nil)
        countryVC.delegate = self
        self.countrySelector = STPopupController(rootViewController: countryVC)
        self.countryFlag.isUserInteractionEnabled = true
        self.countryFlag.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectCountry(_:))))
        
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            self.countryFlag.text = countryCode.emojiFlag()
            self.countryLabel.text = countryFromCountryCode(code: countryCode).dialCode
            
        }else{
            self.countryFlag.text = "KW".emojiFlag()
            self.countryLabel.text = countryFromCountryCode(code: "KW").dialCode
        }
        
        if !isEmailRegistration {
            self.initRegFromUser(user: signedUser)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activateButton.isEnabled = acceptSwitch.isOn

        spinnerView = UIView.init(frame: validReference.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.8)
        ai = UIActivityIndicatorView.init(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        ai.center = spinnerView.center
        
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func startAiRefenceCode()  {
        
        if !ai.isAnimating {
            ai.startAnimating()
            DispatchQueue.main.async {
                self.spinnerView.addSubview(self.ai)
                self.validReference.addSubview(self.spinnerView)
            }
        }
    }
    
    func stopAiRefenceCode()  {
        
        if ai.isAnimating {
            ai.stopAnimating()
            DispatchQueue.main.async {
                self.spinnerView.removeFromSuperview()
            }
        }
    }
    
    
    @objc func selectCountry(_ sender: UITapGestureRecognizer){
        self.countrySelector.present(in: self)
    }
    
    
    func initRegFromUser(user : User?) {
        
        if isPhoneRegistration{
            self.phoneNumber.text = Auth.auth().currentUser!.phoneNumber
            self.phoneNumber.isEnabled = false
            self.countryFlag.isHidden = true
            self.countryLabel.isHidden = true

        }else if let user = user{
            
            if user.email != nil {
                self.userEmail.text = user.email
                self.userEmail.isEnabled = false
            }
            if user.phoneNumber != nil {
                self.phoneNumber.text = user.phoneNumber
                self.phoneNumber.isEnabled = false
            }
            
            if user.firstName != nil {
                self.username.text = user.firstName
                self.username.isEnabled = false
            }
            if user.lastName != nil {
                self.lastName.text = user.lastName
                self.lastName.isEnabled = false
            }
            
            self.password.isEnabled = false
            
            if let pic = user.pictureURL , pic.count > 0 {
                self.uploadPhoto = true
                self.userPicture.sd_setImage(with: URL(string: pic)) { (image, error, cache, url) in
                    self.userPicture.image = image
                }
            }
        }
    }
    
    @IBAction func acceptTerms(_ sender: Any) {
        activateButton.isEnabled = acceptSwitch.isOn
    }
    
    @IBAction func openTermsWebView(_ sender: Any) {
        var arabicTerms = ""
        
        if Languages.currentLanguage().starts(with: "ar"){
            arabicTerms = "https://mydriverkw.com/ar/my_driver_conditions_legals/"
        }else {
            arabicTerms = "https://mydriverkw.com/en/my_driver_conditions_legals/"
            
        }
        
        guard let url = URL(string: arabicTerms) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func openPrivacyPolicyWebView(_ sender: Any) {
        var privacyURL = ""
        
        if Languages.currentLanguage().starts(with: "ar"){
            privacyURL = "https://mydriverkw.com/ar/my_driver_privacy_policy/"
        }else {
            privacyURL = "https://mydriverkw.com/en/my_driver_privacy_policy/"
            
        }
        guard let url = URL(string: privacyURL) else { return }
        UIApplication.shared.open(url)
    }
    
    
    func signUsingEmail() {
        let phoneNum = (self.countryLabel.text! + self.phoneNumber.text!).replacingOccurrences(of: " ", with: "")
        let email = userEmail.text!.replacingOccurrences(of: " ", with: "")
        let pwd = password.text!
        
        Auth.auth().createUser(withEmail: email, password: pwd) { (authResult, error) in
            self.activateButton.loading = false
            if let error = error {
                print(error.localizedDescription)
                self.handleError(error)
                return
            }
            
            let user = User(id: authResult!.user.uid,
                            email: email,
                            firstname: self.username.text!,
                            lastname: self.lastName.text!,
                            phoneNumber: phoneNum ,
                            referralCode: self.referralCode ,
                            referencedBy: self.isReferrencedBy ? self.referencedBy.text! : nil ,
                            pictureURL : self.profilePictureTitle)
            
            user.createUser()
            user.saveCurrentUserToLocal()
            self.sendEmailVerification()

            UserDefaults.standard.set(self.isReferrencedBy, forKey: "fromRegistrationWithReferalCode")
            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
        }

    }
    
    func signUsingSocials(){
        self.activateButton.loading = false

        let email = userEmail.text!.replacingOccurrences(of: " ", with: "")
        let phoneNum = (self.countryLabel.text! + self.phoneNumber.text!).replacingOccurrences(of: " ", with: "")
        
        let user = User(id: Auth.auth().currentUser!.uid,
                        email: email,
                        firstname: self.username.text!,
                        lastname: self.lastName.text!,
                        phoneNumber: phoneNum ,
                        referralCode: self.referralCode ,
                        referencedBy: self.isReferrencedBy ? self.referencedBy.text! : nil ,
                        pictureURL : self.profilePictureTitle)
        
        user.createUser()
        user.saveCurrentUserToLocal()
        self.sendEmailVerification()

        UserDefaults.standard.set(self.isReferrencedBy, forKey: "fromRegistrationWithReferalCode")
        self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
    }
    
    fileprivate func linkEmailAccount() {
        
        let userId = Auth.auth().currentUser!.uid
        let phoneNum = Auth.auth().currentUser!.phoneNumber
        let email = userEmail.text!.replacingOccurrences(of: " ", with: "")
        
        let credential = EmailAuthProvider.credential(withEmail: email, password: self.password.text!)
        
        Auth.auth().currentUser!.link(with: credential, completion: { (authResult, error) in
            self.activateButton.loading = false

            if let error = error {
                print(error.localizedDescription)
                self.handleError(error)
                return
            }
            
            
            let user = User(id: userId,
                            email: email,
                            firstname: self.username.text!,
                            lastname: self.lastName.text!,
                            phoneNumber: phoneNum! ,
                            referralCode: self.referralCode ,
                            referencedBy: self.isReferrencedBy ? self.referencedBy.text! : nil ,
                            pictureURL : self.profilePictureTitle)
            
            user.createUser()
            user.saveCurrentUserToLocal()
            self.sendEmailVerification()
            
            UserDefaults.standard.set(self.isReferrencedBy, forKey: "fromRegistrationWithReferalCode")
            self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
        })
    }
    
    func sendEmailVerification() {
        Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
            if let error = error {
                print(error.localizedDescription)
                self.handleError(error)
            }else {
                let banner = NotificationBanner(title: "Email Verification", subtitle: "Please check your email.", style: .warning)
                banner.show()
            }
        })
    }
    
    func verifInputs() -> Bool {
        
        if username.text!.isBlank || !username.text!.replacingOccurrences(of: " ", with: "").isValidName {
            username.shake()
            return false
        }
        if lastName.text!.isBlank  || !lastName.text!.replacingOccurrences(of: " ", with: "").isValidName{
            lastName.shake()
            return false
        }
        
        if userEmail.text!.isBlank  || !userEmail.text!.replacingOccurrences(of: " ", with: "").isEmail{
            userEmail.shake()
            return false
        }
        
        if phoneNumber.text!.isBlank  || !phoneNumber.text!.replacingOccurrences(of: " ", with: "").isPhoneNumber{
            phoneNumber.shake()
            return false
        }
        if password.text!.isBlank , !isSocialRegistration{
            password.shake()
            return false
        }
        return true

    }
    
    func uploadProfilePicture(completionHandler : @escaping ((StorageMetadata? , Error?) -> Void) ) {
        
        if let image = userPicture.image {
            var data = Data()
            data = UIImageJPEGRepresentation(image, 0.8)!
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            self.profilePictureTitle = "\(Date.timeIntervalSinceReferenceDate)"
            
            let storageRef = Storage.storage().reference()
            let profileImagesRef = storageRef.child("ProfilePictures/\(self.profilePictureTitle!)")
            
            // Upload the file to the path "images/rivers.jpg"
            profileImagesRef.putData(data, metadata: metaData, completion: completionHandler)
        }
        
    }
    
    @IBAction func register(_ sender: Any) {
        
        if self.referralCode == nil {
            self.showAlertView(isOneButton: true, title: "Referral Code Error. Retry Again!", cancelButtonTitle: "Cancel", submitButtonTitle: "",cancelHandler:{
                self.getReferralCode()
            })
            return
        }
        
        if self.verifInputs(){
            

            self.activateButton.loading = true
            
            if uploadPhoto {
                
                
                self.uploadProfilePicture { (metadata, error) in
                    if let error = error {
                        self.activateButton.loading = false
                        print(error.localizedDescription)
                        self.handleError(error)
                    }else {
                        
                        if self.isEmailRegistration{
                            
                            self.signUsingEmail()
                            
                        }else if self.isPhoneRegistration{
                            
                            self.linkEmailAccount()
                            
                        }else if self.isSocialRegistration{
                            
                            self.signUsingSocials()
                            
                        }
                    }
                }
            }else {
                
                if isEmailRegistration{
                    
                    self.signUsingEmail()
                    
                }else if isPhoneRegistration{
                    
                    self.linkEmailAccount()
                    
                }else if isSocialRegistration{
                    
                    self.signUsingSocials()
                    
                }
            }
        }
 
    }
    
    func getReferralCode(){
        
        Alamofire.request(CURRENT_DB_REF_APP + "api/referralCode", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseString { (response) in
            switch response.result {
            case .failure(_):
                print(response.description)
                break;
            case .success(let value):
                
                self.referralCode = value
                
            }
            
        }
    }
    
    func checkReferencedByCode(code : String, completionBlock: @escaping (DataResponse<String>) -> Void) {
        Alamofire.request(CURRENT_DB_REF_APP + "api/checkCode", method: .post, parameters: ["code":code], encoding: URLEncoding.default, headers: nil).responseString(completionHandler: completionBlock)
    }
    
    @objc func editUserPicture ( ){
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("TakePhoto", comment:"TakePhoto"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("FromLibrary", comment:"FromLibrary"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = .photoLibrary
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.view.tintColor = UIColor.gray
        
        if let popoverController = alert.popoverPresentationController {
            
            popoverController.sourceView = self.editPicture
            
        }
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment:"Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func editingChanged(_ sender: UITextField) {
        if sender == referencedBy{
            self.validReference.isHidden = false
            
            self.startAiRefenceCode()
            
            self.checkReferencedByCode(code: sender.text!) { (response) in
                
                self.stopAiRefenceCode()
                
                switch response.result {
                case .failure(_):
                    print(response.description)
                    self.isReferrencedBy = false
                    self.validReference.image = UIImage(named: "warning")
                    break;
                    
                case .success(let value):
                    print(value)
                    if value == "true"{
                        self.isReferrencedBy = true
                        self.validReference.isHidden = false
                        self.validReference.image = UIImage(named: "checked2")
                    }else {
                        self.isReferrencedBy = false
                        self.validReference.image = UIImage(named: "warning")
                        
                    }
                    
                }
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }
    
}
extension RegistrationTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let mediaType = (info[UIImagePickerControllerMediaType] as! String)
        
        if mediaType == "public.image" {
            
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                self.userPicture.image = image
                self.uploadPhoto = true
            }
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}
extension RegistrationTableViewController: CountrySelectorDelegate{
    
    func didSelectCountry(country: Country, viewController: UIViewController) {
        
        viewController.dismiss(animated: true) {
            
            self.countryFlag.text = country.code.emojiFlag()
            
            self.countryLabel.text = country.dialCode
            
        }
    }
}
