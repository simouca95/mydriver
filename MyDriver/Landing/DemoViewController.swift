//
//  DemoViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/30/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class DemoViewController: UIViewController {

    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var pageController: UIPageControl!
    
    var index = 0
    var firstLoad = true
    var didEndScrolling = true
    
    static let storyboardId = "DemoViewController"
 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        scrollView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (firstLoad){
            
            pageController.currentPage = 0

            if Languages.currentLanguage().starts(with: "ar") {

                pageController.currentPage = 3
            }
            
            firstLoad = false
        }
    }
    
    @IBAction func changePage(_ sender: Any) {
        let page = pageController.currentPage
        
        var frame: CGRect = scrollView.frame
        
        frame.origin.x = frame.size.width * CGFloat(page)
        
        frame.origin.y = 0
        
        scrollView.scrollRectToVisible(frame, animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        if didEndScrolling {
            
            didEndScrolling = false
            //
            //            if (UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft){
            //
            //                scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x - view.frame.width, y: 0), animated: true)
            //            }
            //            else {
            
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x + view.frame.width, y: 0), animated: true)
            //    }
        }

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return .lightContent
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DemoViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let page = scrollView.contentOffset.x/scrollView.frame.size.width
        
        //        if (UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft){
        //
        //            page = 3 - page
        //        }
        
        if Languages.currentLanguage().starts(with: "ar") {
            
            pageController.currentPage = 3 - Int(page)
        }
        else{
            
            pageController.currentPage = Int(page)
        }

//        if Languages.currentLanguage().starts(with: "ar") {
//
//            pageController.currentPage = 3 - Int(page)
//        }
//        else{
//
//            pageController.currentPage = Int(page)
//        }
        
        //        var finishPage = 2
        //
        //        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
        //
        //            finishPage = 0
        //        }
        
        if Int(page) == 3 {
            
            nextButton.isHidden = true
            
            nextButton.isEnabled = false
            
            goButton.isHidden = false
            
            goButton.isEnabled = true
            
        }else{
            
            nextButton.isHidden = false
            
            nextButton.isEnabled = true
            
            goButton.isHidden = true
            
            goButton.isEnabled = false
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
        didEndScrolling = true
    }
}
