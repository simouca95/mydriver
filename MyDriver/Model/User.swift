//
//  User.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import FirebaseDatabase
import SwiftyJSON
import FirebaseAuth
import CoreData

class User: NSObject {
    
    var uid : String!
    var deviceToken : String!
    
    var email : String!
    var firstName : String!
    var lastName : String!
    var pictureURL : String!
    var favoriteAddress : [FavoriteAddress]?
    var phoneNumber : String!
    var referralCode : String!
    var referencedBy : String!
    var promoWallet : Float!


    override init() {
        super.init()
    }
    
    convenience init(snap: DataSnapshot) {
        self.init()
        
        uid = snap.key
        
        let userDict = snap.value as! [String: Any]
        
        email = userDict["email"] as? String
        firstName = userDict["firstName"] as? String
        lastName = userDict["lastName"] as? String
        
        referralCode = userDict["referralCode"] as? String
        
        if let pr = userDict["promoWallet"] as? Float{
            promoWallet = pr
        }else {
            promoWallet = 0
        }

        
        if let pictureURL = userDict["pictureURL"] as? String{
            self.pictureURL = pictureURL
        }
        
        if let phoneNumber = userDict["phoneNumber"] as? String{
            self.phoneNumber = phoneNumber
        }
        
        if let referencedBy = userDict["referencedBy"] as? String{
            self.referencedBy = referencedBy
        }
        
        favoriteAddress = [FavoriteAddress]()
        if let userDict = userDict["favorites"] {
            let favoriteAddressArray = JSON(userDict).dictionaryValue
            for fav in favoriteAddressArray{
                let value = FavoriteAddress(fromJson: fav.value)
                value.uid = fav.key
                favoriteAddress!.append(value)
            }
        }
        
        self.saveCurrentUserToLocal()
    }
    
    
    
    init( id : String , email : String , firstname: String, lastname: String , phoneNumber : String , referralCode : String, referencedBy : String? , pictureURL : String? = nil) {
        self.uid = id
        self.email = email
        self.firstName = firstname
        self.lastName = lastname
        self.phoneNumber = phoneNumber
        self.referralCode = referralCode
        self.pictureURL = pictureURL
        
        
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        changeRequest?.displayName = firstname + " " + lastname
        if pictureURL != nil  {
            changeRequest?.photoURL =  URL(string: pictureURL!)
        }
        changeRequest?.commitChanges { (error) in
            if error != nil {
                print("error update user fullname and picture url")
            }
        }
        
        var config : Configs!
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchCountReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Config")
        do {
            let test = try context.fetch(fetchCountReq)
            if test.count > 0 {
                let object = test[0] as? NSManagedObject
                config = Configs(trafficModel: object?.value(forKey: "trafficModel") as! String, welcomeGift: object?.value(forKey: "welcomeGift") as! Float)
            }
        } catch  {
            print(error)
        }
        if referencedBy != nil  {
            self.referencedBy = referencedBy!
            if config != nil {
                self.promoWallet = config.welcomeGift
            }
        }else {
            self.promoWallet = 0
        }


    }
    
    static func verifyPhoneNumberExistence(phoneNumber : String, completionBlock: @escaping (DataSnapshot?)->Void){
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("User").queryOrdered(byChild: "phoneNumber").queryEqual(toValue: phoneNumber).observeSingleEvent(of: .value, with: completionBlock)

    }
    
    func createUser()  {
        let ref = Database.database(url: CURRENT_DB_REF).reference()

        ref.child("User").child(self.uid).setValue(self.toDictionary())
        
        if let newToken = UserDefaults.standard.string(forKey: "RegistrationToken"){
            
            User.updateDeviceToken(self.uid, newToken)
        }else{
            User.updateDeviceToken(self.uid, "")
        }
    }
    
    static func verifyValidUser(userId : String, completionBlock: @escaping (DataSnapshot?)->Void){
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("User").child(userId).observeSingleEvent(of: .value, with: completionBlock) { (error) in
            print(error.localizedDescription)
        }
    }    
    
    func updateUserPhoto(photoURL : String) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("User").child(Auth.auth().currentUser!.uid).child("pictureURL").setValue(photoURL)

    }
    
    func updateUserPhoneNumber(number : String)  {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("User").child(Auth.auth().currentUser!.uid).child("phoneNumber").setValue(number)
    }
    
    
    func addToUserFavourites(fav : FavoriteAddress) {
        
        if self.favoriteAddress == nil {
            self.favoriteAddress = [FavoriteAddress]()
        }else {
            self.favoriteAddress?.removeAll(where: { (oldFav) -> Bool in
                if fav.type != "fav"{
                    return oldFav.type == fav.type
                }
                return false
            })
        }
        
        //remote save
        self.favoriteAddress!.append(fav)
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        let newRef = ref.child("User").child(Auth.auth().currentUser!.uid).child("favorites").childByAutoId()
        newRef.setValue(fav.toDictionary())
        fav.uid = newRef.key!
        
//        //save to local
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//        let context = appDelegate.persistentContainer.viewContext
//        let entityFav = NSEntityDescription.entity(forEntityName: "Favorites", in: context)
//        let newFav = NSManagedObject(entity: entityFav!, insertInto: context)
//        newFav.setValue(fav.uid,forKey: "id")
//        newFav.setValue(fav.name, forKey: "address")
//        newFav.setValue(fav.latitude, forKey: "latitude")
//        newFav.setValue(fav.longitude, forKey: "longitude")
//        newFav.setValue(fav.type, forKey: "category")
//        newFav.setValue(self.uid, forKey: "idUser")
//

//
//        do {
//            try context.save()
//        } catch {
//            print("Failed saving")
//        }
        
    }
    
    
    func addRecentAddressSearch(fav : FavoriteAddress){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        // count recent races if >3 delete the oldest and add the newest
        let fetchCountReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorites")
        fetchCountReq.predicate = NSPredicate(format: "idUser = %@", Auth.auth().currentUser!.uid)
        fetchCountReq.predicate = NSPredicate(format: "category = %@", "rec")
        
        do {
            let test = try context.fetch(fetchCountReq)
            if test.count > 2{
                let objectTodelete = test[0] as! NSManagedObject
                context.delete(objectTodelete)
                do {
                    try context.save()
                } catch  {
                    print(error)
                }
            }
        } catch  {
            print(error)
        }
        // add the recent search address
        let entityFav = NSEntityDescription.entity(forEntityName: "Favorites", in: context)
        let newFav = NSManagedObject(entity: entityFav!, insertInto: context)
        
        newFav.setValue("\(Date.timeIntervalSinceReferenceDate)", forKey: "id")
        newFav.setValue(fav.name, forKey: "address")
        newFav.setValue(fav.latitude, forKey: "latitude")
        newFav.setValue(fav.longitude, forKey: "longitude")
        newFav.setValue(fav.type, forKey: "category")
        newFav.setValue(self.uid, forKey: "idUser")
        
        if let _ = self.favoriteAddress {
            self.favoriteAddress!.append(fav)
        }else{
            self.favoriteAddress = [fav]
        }
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
        
    }
    
    func deleteFavFromLocal(fav : FavoriteAddress) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorites")
        fetchReq.predicate = NSPredicate(format: "address = %@", fav.name!)

        do {
            let test = try context.fetch(fetchReq)
            let objectTodelete = test[0] as! NSManagedObject
            context.delete(objectTodelete)
            do {
                try context.save()
            } catch  {
                print(error)
            }
        } catch  {
            print(error)
        }
    }
    

    static func fetchCurrentUser(completionBlock: @escaping (DataSnapshot?)->Void ){
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("User").child(Auth.auth().currentUser!.uid).observe(.value,with : completionBlock)
    }

    static func fetchCurrentUserFromLocal() -> User {
        
        let userId = Auth.auth().currentUser!.uid
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let user = User()
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "id = %@", userId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                user.uid = userId
                user.email = data.value(forKey:"email") as? String
                user.firstName = data.value(forKey:"firstName") as? String
                user.lastName = data.value(forKey:"lastName") as? String
                user.promoWallet = data.value(forKey:"walletBalance") as? Float
                
                if let pic = data.value(forKey:"pictureUrl"){
                    user.pictureURL =  pic as? String
                }
                if let phoneNumber = data.value(forKey:"phoneNumber") as? String{
                    user.phoneNumber = phoneNumber
                }
                if let referralCode = data.value(forKey:"referralCode") as? String{
                    user.referralCode = referralCode
                }
                if let referencedBy = data.value(forKey:"referencedBy") as? String{
                    user.referencedBy = referencedBy
                }
            }
            
            let requestFav = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorites")
            requestFav.predicate = NSPredicate(format: "idUser = %@", userId)
            requestFav.returnsObjectsAsFaults = false
            let resultFav = try context.fetch(requestFav)
            user.favoriteAddress = [FavoriteAddress]()
            for dataFav in resultFav as! [NSManagedObject] {
                let fav = FavoriteAddress(name: dataFav.value(forKey:"address") as! String,
                                          lat: dataFav.value(forKey:"latitude") as! Double,
                                          lng: dataFav.value(forKey:"longitude") as! Double,
                                          type: dataFav.value(forKey:"category") as! String)
                fav.uid = dataFav.value(forKey:"id") as? String
                user.favoriteAddress?.append(fav)
            }
            
        } catch {
            print(" failed fetch \(error.localizedDescription)")
        }
        return user
    }
    
    func saveCurrentUserToLocal()  {
        
        self.deleteUser(userId: self.uid)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
        
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        newUser.setValue(self.uid, forKey: "id")
        newUser.setValue(self.firstName, forKey: "firstName")
        newUser.setValue(self.lastName, forKey: "lastName")
        newUser.setValue(self.email, forKey: "email")
        newUser.setValue(self.phoneNumber, forKey: "phoneNumber")
        newUser.setValue(self.promoWallet, forKey: "walletBalance")
        
        if let code = self.referralCode {
            newUser.setValue(code, forKey: "referralCode")
        }else {
            newUser.setValue("", forKey: "referralCode")
        }
        
        if let code = self.referencedBy {
            newUser.setValue(code, forKey: "referencedBy")
        }else {
            newUser.setValue("", forKey: "referencedBy")
            
        }

        
        if let pic = self.pictureURL {
            newUser.setValue(pic, forKey: "pictureUrl")
        }else {
            newUser.setValue(" ", forKey: "pictureUrl")

        }
        
        if let favAdrss = self.favoriteAddress{
            for fav in favAdrss {
                let entityFav = NSEntityDescription.entity(forEntityName: "Favorites", in: context)
                let newFav = NSManagedObject(entity: entityFav!, insertInto: context)
                newFav.setValue(fav.uid,forKey: "id")
                newFav.setValue(fav.name, forKey: "address")
                newFav.setValue(fav.latitude, forKey: "latitude")
                newFav.setValue(fav.longitude, forKey: "longitude")
                newFav.setValue(fav.type, forKey: "category")
                newFav.setValue(self.uid, forKey: "idUser")
            }
        }
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    func deleteFav(fav : FavoriteAddress, completionBlock : @escaping (Error? , DatabaseReference) -> Void)  {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("User").child(self.uid).child("favorites").child(fav.uid!).removeValue(completionBlock: completionBlock)
    }
    
    func deleteUser(userId : String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        fetchReq.predicate = NSPredicate(format: "id = %@", self.uid)
        do {
            let test = try context.fetch(fetchReq)
            if test.count > 0 {
                let objectTodelete = test[0] as! NSManagedObject
                context.delete(objectTodelete)
                do {
                    try context.save()
                    print("user deleted")

                } catch  {
                    print(error)
                }
            }

        } catch  {
            print(error)
            
        }
        if self.favoriteAddress != nil {
            let fetchReqFav = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorites")
            fetchReqFav.predicate = NSPredicate(format: "idUser = %@", self.uid)
            fetchReqFav.predicate = NSPredicate(format: "category != %@", "rec")
            
            do {
                let test = try context.fetch(fetchReqFav)
                
                for objectTodelete in test {
                    context.delete(objectTodelete as! NSManagedObject)
                }
                do {
                    try context.save()
                    print("user updated")
                } catch  {
                    print(error)
                }
            } catch  {
                print(error)
                
            }
        }
    }
    
    static func updateDeviceToken(_ userId : String , _ deviceToken : String) {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("User").child(userId).child("deviceToken").setValue(deviceToken)
    }
    
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if email != nil{
            dictionary["email"] = email
        }else {
            dictionary["email"] = ""
            
        }
        
        if promoWallet != nil{
            dictionary["promoWallet"] = promoWallet
        }else {
            dictionary["promoWallet"] = 0

        }
        
        if referralCode != nil{
            dictionary["referralCode"] = referralCode
        }else{
            dictionary["referralCode"] = ""
        }
        
        if referencedBy != nil{
            dictionary["referencedBy"] = referencedBy
        }
        
        if lastName != nil{
            dictionary["lastName"] = lastName
        }else{
            dictionary["lastName"] = ""
        }
            
        
        if firstName != nil{
            dictionary["firstName"] = firstName
        }else{
            dictionary["firstName"] = ""
        }
            
        
//        if password != nil{
//            dictionary["password"] = password
//        }
        
        if phoneNumber != nil{
            dictionary["phoneNumber"] = phoneNumber
        }else{
            dictionary["phoneNumber"] = ""
        }
            
        
        if pictureURL != nil{
            
            dictionary["pictureURL"] = pictureURL
        }else {
            
            dictionary["pictureURL"] = ""
        }
        
        return dictionary
    }
    
    
}
