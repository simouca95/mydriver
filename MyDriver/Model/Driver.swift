//
//  Driver.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

import FirebaseDatabase

class Driver : NSObject{
    
    var uid : String!
    
    var vehicle : Vehicle?
    var vehicleId : String?
    
    var agencyId : String?
    
    var fullName : String!
    var phoneNumber : String!
    var residence : String!
    var pictureURL : String?

    var status : Int!
    var currentPosition : Position?
    
    
    var deviceToken : String?

    
    var handleUint : UInt!

    override init() {
        super.init()
    }
    convenience init(snap: DataSnapshot) {
        
        //let userDict = JSON(snap.value!)
        //self.init(fromJson: userDict)
        
        self.init()
        
        self.uid = snap.key
        self.fullName = snap.childSnapshot(forPath: "fullName").value as? String
        
        let locationJson = snap.childSnapshot(forPath: "currentPosition")
        
        if locationJson.exists(){
            
            currentPosition = Position(fromJson: JSON(locationJson.value!))
        }
        agencyId = snap.childSnapshot(forPath: "agencyId").value as? String
        vehicleId = snap.childSnapshot(forPath: "vehicleId").value as? String
        deviceToken = snap.childSnapshot(forPath: "deviceToken").value as? String
        residence = snap.childSnapshot(forPath: "residence").value as? String
        phoneNumber = snap.childSnapshot(forPath: "phoneNumber").value as? String
        
        status = snap.childSnapshot(forPath: "status").value as? Int
        if let picURL =  snap.childSnapshot(forPath: "pictureURL").value as? String{
            pictureURL =   picURL
        }
    }
    
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        if uid == nil {
            if json["uid"].string != nil {
                uid = json["uid"].stringValue
            }
        }
        
        let jsonObject = json["driver"]
        fullName = jsonObject["fullName"].stringValue
        
        let locationJson = jsonObject["currentPosition"]
        
        if !locationJson.isEmpty{

            currentPosition = Position(fromJson: locationJson)
        }
        agencyId = jsonObject["agencyId"].stringValue
        vehicleId = jsonObject["vehicleId"].stringValue
        deviceToken = jsonObject["deviceToken"].stringValue
        residence = jsonObject["residence"].stringValue
        phoneNumber = jsonObject["phoneNumber"].stringValue
        
        status = jsonObject["status"].intValue
        pictureURL = jsonObject["pictureURL"].stringValue

    }
    
    static func fetchDriverByID(driverId : String, completionHandler : @escaping (Driver?) -> Void, _ handlerError : @escaping((Error) -> Void) ){
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("Driver").child(driverId).observeSingleEvent(of: .value, with: { (snap) in
            if snap.exists(){
                
                let driver = Driver(snap: snap)
                
                Vehicle.fetchVehicleById(vehicleID: driver.vehicleId ?? "", completionHandler: { (vehicle) in
                    
                    if let vehicle = vehicle{
                        driver.vehicle = vehicle
                    }
                    completionHandler(driver)
                    
                }, handlerError)
                
            }else {
                completionHandler(nil)

            }
        }, withCancel: handlerError)

    }
    
    func updateLocation(_ handler : @escaping((DataSnapshot) -> Void) , _ handlerError : @escaping((Error) -> Void)) {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()

        self.handleUint = ref.child("Driver").child(self.uid!).child("currentPosition").observe(.value, with: handler, withCancel: handlerError)
    }
    
    static func updateDriverLocationByUID(driverID : String , _ handler : @escaping((DataSnapshot) -> Void) , _ handlerError : @escaping((Error) -> Void)) -> UInt {
    
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        let handleUINT = ref.child("Driver").child(driverID).child("currentPosition").observe(.value, with: handler, withCancel: handlerError)
        return handleUINT
    }
    
    static func removeCourseObserver(driverId : String , handle : UInt){
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Driver").child(driverId).child("currentPosition").removeObserver(withHandle: handle)
    }
    
    
    static func getNearbyDrivers(json : JSON ) -> [Driver]? {
        
        var nearbyCars = [Driver]()
        
        for car in json.arrayValue {
            let vehicle = Vehicle(fromJsonWithUID: car)
            let driver = Driver(fromJson: car)
            driver.vehicle = vehicle
            nearbyCars.append(driver)
        }
        return nearbyCars
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if fullName != nil{
            dictionary["fullName"] = fullName
        }

        if currentPosition != nil{
            dictionary["currentPosition"] = ["lat": currentPosition!.latitude , "lng" : currentPosition!.longitude , "bearing" : currentPosition!.bearing ]
        }
        if vehicleId != nil{
            dictionary["vehicleId"] = vehicleId
        }
        if agencyId != nil{
            dictionary["agencyId"] = agencyId
        }
        if pictureURL != nil{
            dictionary["pictureURL"] = pictureURL!
        }

        if residence != nil{
            dictionary["residence"] = residence
        }
        
        if status != nil{
            dictionary["status"] = status
        }
        
        if phoneNumber != nil{
            dictionary["phoneNumber"] = phoneNumber
        }

        return dictionary
    }
    
}
