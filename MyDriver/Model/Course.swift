//
//  Course.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseDatabase
import CoreLocation
import FirebaseAuth
import Alamofire


struct WaitingTimeStruct {
    var duration : Int!
    var status : String!
}

class Course : NSObject{
    
    var uid : String!
    
    
    var clientId : String!
    var driverId : String?
    var vehicleId : String?
    
    var createdAt : Date!
    var startedAt : Date?
    var finishedAt : Date?
    var acceptedAt : Date?
    var arrivedAt : Date?
    
    var destination : Position!
    var pickUpPoint : Position!
    
    var classCar : String!
    var estimatedCost : Double?
    var finalCost : Double?
    
    var waitingTime : [WaitingTimeStruct]?
    
    var estimatedTimeArrival : Int!
    var rating : Int?
    var status : String!
    
    var courseHasChanged : Int!

    var initialDistance : Int?
    
    var driver : Driver?
    
    var ref : DatabaseReference!
    
    override init() {
        super.init()
    }
    
    convenience init(snap: DataSnapshot) {
        
        let userDict = JSON(snap.value!)
        
        self.init(fromJson: userDict)
        
        self.uid = snap.key
        
    }
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    convenience init(fromJson json: JSON!){
        self.init()

        if json.isEmpty{
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        if json["startedAt"].stringValue != "" {
            startedAt = dateFormatter.date(from: json["startedAt"].stringValue)
            
        }
        if json["createdAt"].stringValue != "" {
            createdAt = dateFormatter.date(from: json["createdAt"].stringValue)
            
        }
        if json["arrivedAt"].stringValue != "" {
            arrivedAt = dateFormatter.date(from: json["arrivedAt"].stringValue)
            
        }

        if json["finishedAt"].stringValue != "" {
            finishedAt = dateFormatter.date(from: json["finishedAt"].stringValue)
            
        }
        if json["acceptedAt"].stringValue != "" {
            acceptedAt = dateFormatter.date(from: json["acceptedAt"].stringValue)
            
        }
        
        classCar = json["classCar"].stringValue
        clientId = json["clientId"].stringValue
        vehicleId = json["vehicleId"].stringValue
        driverId = json["driverId"].stringValue

        let destinationJson = json["destination"]
        if !destinationJson.isEmpty{
            destination = Position(fromJson: destinationJson)
        }
        let pickUpPointJson = json["pickUpPoint"]
        if !pickUpPointJson.isEmpty{
            pickUpPoint = Position(fromJson: pickUpPointJson)
        }
        
        initialDistance = json["initialDistance"].int
        if json["eta"].stringValue != "" {
            estimatedTimeArrival = json["eta"].int!
            
        }
        estimatedCost = json["ec"].doubleValue
        finalCost = json["fc"].doubleValue
        
        rating = json["rating"].intValue
        status = json["status"].stringValue
        
        if let json = json["wt"].dictionary{
            waitingTime = [WaitingTimeStruct]()
            
            json.forEach { (arg) in
                let value = arg.value
                let wtStruct = WaitingTimeStruct(duration: value.dictionaryValue["duration"]?.intValue, status: value.dictionaryValue["status"]?.stringValue)
                if waitingTime == nil {
                    waitingTime = [wtStruct]
                }else{
                    waitingTime!.append(wtStruct)
                }
            }
            
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        dateFormatter.locale = Locale.init(identifier: "EN")

        
        var dictionary = [String:Any]()
        
        if acceptedAt != nil{
            dictionary["acceptedAt"] = dateFormatter.string(from: acceptedAt!)
        }else {
            
            dictionary["acceptedAt"] = ""
        }
        
        if arrivedAt != nil{
            dictionary["arrivedAt"] = dateFormatter.string(from: arrivedAt!)
        }else {
            
            dictionary["arrivedAt"] = ""
        }
        
        if initialDistance != nil{
            dictionary["initialDistance"] = initialDistance
        }else {
            
            dictionary["initialDistance"] = 0
        }
        
        if finishedAt != nil{
            dictionary["finishedAt"] = dateFormatter.string(from: finishedAt!)
        }else {
            
            dictionary["finishedAt"] = ""
        }
        
        if createdAt != nil{
            dictionary["createdAt"] = dateFormatter.string(from: createdAt)
        }else {
            
            dictionary["createdAt"] = ""
        }
        
        if startedAt != nil{
            dictionary["startedAt"] = dateFormatter.string(from: startedAt!)
        }else {
            
            dictionary["startedAt"] = ""
        }
        
        if estimatedTimeArrival != nil{
            dictionary["eta"] = estimatedTimeArrival
        }else {
            
            dictionary["eta"] = 0
        }
        
        if classCar != nil{
            dictionary["classCar"] = classCar
        }
        
        if clientId != nil{
            dictionary["clientId"] = clientId
        }else {
            
            dictionary["clientId"] = ""
        }
        
        if vehicleId != nil{
            dictionary["vehicleId"] = vehicleId
        }else {
            
            dictionary["vehicleId"] = ""
        }

        
        if driverId != nil{
            dictionary["driverId"] = driverId
        }else {
            
            dictionary["driverId"] = ""
        }
        
        if destination != nil{
            dictionary["destination"] = ["lat": destination.latitude! , "lng" : destination.longitude!, "bearing" : destination.bearing!, "address" : destination.address! ]
            
        }
        
        if estimatedCost != nil{
            dictionary["ec"] = estimatedCost
        }else {
            
            dictionary["ec"] = 0
        }
        
        if finalCost != nil{
            dictionary["fc"] = finalCost
        }else {
            
            dictionary["fc"] = 0
        }
        
        if pickUpPoint != nil{
            dictionary["pickUpPoint"] = ["lat": pickUpPoint.latitude! , "lng" : pickUpPoint.longitude!, "bearing" : pickUpPoint.bearing!, "address" : pickUpPoint.address! ]
        }
        
        
        if rating != nil{
            dictionary["rating"] = rating
        }else {
            
            dictionary["rating"] = 0
        }
        
        
        if status != nil{
            dictionary["status"] = status
        }
        
        
//        if waitingTime != nil{
//            dictionary["wt"] = waitingTime
//        }else {
//
//            dictionary["wt"] = 0
//        }
        
        dictionary["courseHasChanged"] = 0

        
        return dictionary
    }
    
    init(from : Position ,to : Position , category : String , estimatedDuration : Int, estimatedDistance : Int , estimatedCost : Double){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        self.createdAt = dateFormatter.date(from:dateFormatter.string(from: Date()))
        self.pickUpPoint = from
        self.destination = to
        self.classCar = category
        self.estimatedTimeArrival = estimatedDuration
        self.initialDistance = estimatedDistance
        self.status = "pending"
        self.clientId = Auth.auth().currentUser!.uid
        self.estimatedCost = estimatedCost
    }
    
    static func calculateRaceCostByCategory(tripDetails : JSON , price : Price) -> Double {
        
//        let duration = tripDetails["duration"]
//        let durationValue = duration["value"].floatValue
//
//        let distance = tripDetails["distance"]
//        let distanceValue = distance["value"].floatValue
        
        let traffic = tripDetails["duration_in_traffic"]
        let trafficValue = traffic["value"].floatValue
        
        let pricev = trafficValue > 0 ? (Double(trafficValue / 60) / 10).rounded(.up) : 1 //price per 10 mn
        let value = ((Double(pricev) - 1) * price.priceTenMin) + price.baseFare
        
        return value
    }
    
    static func getDriverPosition(driverId : String, _ handler : @escaping((DataSnapshot) -> Void) , _ handlerError : @escaping((Error) -> Void)) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Driver").child(driverId).child("currentPosition").observeSingleEvent(of: .value, with: handler, withCancel: handlerError)
    }
    
    func createCourse() {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        let newRef = ref.child("Course").childByAutoId()
        newRef.setValue(self.toDictionary())
        self.uid = newRef.key!
        
    }
    
    func cancelTrip()  {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(self.uid).child("status").setValue("canceled")
        
        UserDefaults.standard.removeObject(forKey: "acceptedCourseID")
        UserDefaults.standard.removeObject(forKey: "notifType20")
    }
    
    func rateTrip(rating : Int) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(self.uid).child("rating").setValue(rating)
        self.rating = rating
    }
    
    func courseOnListening( _ handler : @escaping((DataSnapshot) -> Void) , _ handlerError : @escaping((Error) -> Void))  -> UInt {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        let handleUint = ref.child("Course").child(self.uid).child("status").observe(.value, with: handler, withCancel: handlerError)
        
        return handleUint
    }
    
    static func removeCourseOnListeningObserver(courseId : String , handle : UInt){
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(courseId).child("status").removeObserver(withHandle: handle)
    }
    
    
    static func fetchCurrentUserCourses(courseId : String ,completionBlock: @escaping (Course?)->Void , _ handlerError : @escaping((Error) -> Void)) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        if !courseId.isBlank{
            ref.child("Course").child(courseId).observeSingleEvent(of: .value) { (snap) in
                if snap.exists(){
                    let course = Course(snap: snap)
                    if let status = snap.childSnapshot(forPath: "status").value as? String{
                        if status == "postponed" || status == "canceled" || status == "pending"{
                            completionBlock(course)
                        }else {//if (snap.childSnapshot(forPath: "driverId").value as! String).isBlank
                            Driver.fetchDriverByID(driverId: snap.childSnapshot(forPath: "driverId").value as! String, completionHandler: { (driver) in
                                course.driver = driver
                                completionBlock(course)
                            }, handlerError)
                        }
                    }else {
                        completionBlock(nil)
                    }
                }else {
                    completionBlock(nil)
                }
            }
        }

    }
    
    static func getMyCourses(clientID : String , completionBlock: @escaping (DataResponse<Any>) -> Void){
        
        Alamofire.request(CURRENT_DB_REF_APP + "api/raceData", method: .post, parameters: ["idClient": clientID], encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: completionBlock)
        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    
}
