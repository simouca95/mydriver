//
//  Message.swift
//  MyDriver
//
//  Created by sami hazel on 5/20/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import FirebaseDatabase
import SwiftyJSON
import FirebaseAuth

class Message: NSObject  {
    
    var uid : String!

    var fileURL : String!
    var messageBody : String!
    var sentAt : Date!
    var typeSender : String!
    var subject : String!

    var senderId : String!

    override init() {
        super.init()
    }
    
    convenience init(subject : String , typeSender : String , message : String , picture : String? = nil) {
        self.init()
        
        self.subject = subject
        self.typeSender = typeSender
        self.messageBody = message
        self.fileURL = picture
        
        self.senderId = Auth.auth().currentUser?.uid ?? ""
        
        self.sentAt = Date().getFormattedDate("yyyy-MM-dd'T'HH:mm:ssZ")
    }
    
    convenience init(snap: DataSnapshot) {
        
        let userDict = JSON(snap.value!)
        
        self.init(fromJson: userDict)
        
        self.uid = snap.key

    }
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    convenience init(fromJson json: JSON!){
        self.init()

        if json.isEmpty{
            return
        }
        subject = json["subject"].stringValue
        senderId = json["senderId"].stringValue

        typeSender = json["typeSender"].stringValue
        fileURL = json["file"].stringValue
        messageBody = json["messageBody"].stringValue
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        if json["sentAt"].stringValue != "" {
            sentAt = dateFormatter.date(from: json["sentAt"].stringValue)
        }
        
    }
    
    func createMessage() {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        let newRef = ref.child("Message").childByAutoId()
        newRef.setValue(self.toDictionary())
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if subject != nil{
            dictionary["subject"] = subject
        }else {
            dictionary["subject"] = ""
            
        }
        
        if typeSender != nil{
            dictionary["typeSender"] = typeSender
        }else {
            dictionary["typeSender"] = ""
            
        }
        
        if messageBody != nil{
            dictionary["messageBody"] = messageBody
        }else {
            dictionary["messageBody"] = ""
            
        }
        
        if senderId != nil{
            dictionary["senderId"] = Auth.auth().currentUser?.uid
        }else {
            dictionary["senderId"] = ""
            
        }
        
        if fileURL != nil{
            dictionary["file"] = fileURL
        }else {
            dictionary["file"] = ""

        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        if sentAt != nil{
            dictionary["sentAt"] = dateFormatter.string(from: sentAt)
        }
        
        return dictionary
    }

    
}
