//
//  PickerPopup.swift
//  MyDriver
//
//  Created by sami hazel on 4/10/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit


protocol DidPickLocationProtocol {
    func didPickStartLocation()
    func didPickDestinationLocation()
}
class PickerPopup: UIViewController {
    static let storyboardId = "PickerPopup"

    var delegate : DidPickLocationProtocol!
    
    @IBAction func didPickLocation(sender : UIButton){
        if sender.tag == 1 {
            delegate.didPickDestinationLocation()
        }else {
            delegate.didPickStartLocation()
        }
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.contentSizeInPopup = CGSize(width:UIScreen.main.bounds.width - 60,height:200)
        self.popupController?.containerView.layer.cornerRadius = 4.0

        self.popupController?.setNavigationBarHidden(true, animated: false)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
