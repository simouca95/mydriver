//
//  RecentAddressTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/1/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class RecentAddressTableViewController: UITableViewController {

    static let storyboardId = "RecentAddressTableViewController"
    //var delegate : DidPickFromRegistredAddressProtocol!
    var recentFavoritesDataSource : [FavoriteAddress]!
    
    var loadingView: LoadingView!

    var currentUser : User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        currentUser = User.fetchCurrentUserFromLocal()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if recentFavoritesDataSource == nil || recentFavoritesDataSource.count == 0 {
            
            self.loadingView = LoadingView.loadFromNibNamed(nibNamed: "LoadingView", bundle: nil)

            self.loadingView.tryAgainButton.isHidden = true
            
            self.loadingView.loadingLabel.text = NSLocalizedString("No recent search yet", comment: "") 
            
            self.loadingView.activityIndicator.isHidden = true
            
            self.loadingView.frame = self.view.bounds
            
            self.view.addSubview(self.loadingView)

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let _ = recentFavoritesDataSource {
            return recentFavoritesDataSource.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recentSearchCell", for: indexPath) as! RecentSearchTableViewCell
        
        // Configure the cell...
        cell.address.text = recentFavoritesDataSource[indexPath.row].name!
        cell.favButton.tag = indexPath.row
        cell.delegate = self
        return cell

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //self.delegate.didPickLocation(recentFavoritesDataSource[indexPath.row])
        NotificationCenter.default.post(name: Notification.Name("did_pick_location"), object: recentFavoritesDataSource[indexPath.row])

        //delegate.didSelectFavorite(fav: recentFavoritesDataSource[indexPath.row])
    }
}

extension RecentAddressTableViewController : RecentSearchSelectorDelegate{
    func didAddToFavorite(index: Int) {
        currentUser.deleteFavFromLocal(fav: self.recentFavoritesDataSource[index])
        
        self.recentFavoritesDataSource[index].type = "fav"
        currentUser.addToUserFavourites(fav: self.recentFavoritesDataSource[index])
        
        NotificationCenter.default.post(name: Notification.Name("did_add_fav"), object: self.recentFavoritesDataSource[index])

        self.recentFavoritesDataSource.remove(at: index)
        self.tableView.reloadData()
        

    }
}

