//
//  FavoritesTableViewCell.swift
//  MyDriver
//
//  Created by sami hazel on 4/30/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit


protocol DeleteFavoritesSelectorDelegate {
    
    func didDeleteFavorite(index : Int)
}

class FavoritesTableViewCell: UITableViewCell {

    @IBOutlet weak var address : UILabel!
    
    @IBOutlet weak var favButton : UIButton!

    var delegate : DeleteFavoritesSelectorDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        favButton.isSelected = false
        address.text = ""
    }
    
    @IBAction func favAction(sender : UIButton){
        
        sender.isSelected = !sender.isSelected
        
        delegate.didDeleteFavorite(index : favButton.tag)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
