//
//  RecentSearchTableViewCell.swift
//  MyDriver
//
//  Created by sami hazel on 5/2/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit


protocol RecentSearchSelectorDelegate {
    
    func didAddToFavorite(index : Int)
}


class RecentSearchTableViewCell: UITableViewCell {

    
    @IBOutlet weak var address : UILabel!
    
    @IBOutlet weak var favButton : UIButton!
    
    var delegate : RecentSearchSelectorDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        favButton.isSelected = false
        address.text = ""
    }
    
    @IBAction func favAction(sender : UIButton){
        
        sender.isSelected = !sender.isSelected
        
        delegate.didAddToFavorite(index : favButton.tag)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
