//
//  FavoritesTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/30/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class FavoritesTableViewController: UITableViewController {

    static let storyboardId = "FavoritesTableViewController"
    //var delegate : DidPickFromRegistredAddressProtocol!
    var favoritesDataSource : [FavoriteAddress]!

    var loadingView : LoadingView!
    
    var currentUser : User!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didAddFavourite(_:)), name: NSNotification.Name("did_add_fav"), object: nil)

        currentUser = User.fetchCurrentUserFromLocal()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if favoritesDataSource == nil || favoritesDataSource.count == 0 {
            
            self.loadingView = LoadingView.loadFromNibNamed(nibNamed: "LoadingView", bundle: nil)
            
            self.loadingView.tryAgainButton.isHidden = false
            
            self.loadingView.tryAgainButton.isEnabled = true
            
            self.loadingView.tryAgainButton.setTitle(NSLocalizedString("Add Favorite", comment: ""), for: .normal)
            
            self.loadingView.tryAgainButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toFavoriteVC)))
            
            self.loadingView.loadingLabel.text = NSLocalizedString("You dont have favorite address", comment: "")
            
            self.loadingView.activityIndicator.isHidden = true
            
            self.loadingView.frame = self.view.bounds
            
            self.view.addSubview(self.loadingView)
            
        }else {
            currentUser = User.fetchCurrentUserFromLocal()
            favoritesDataSource = self.currentUser.favoriteAddress?.filter({ (fav) -> Bool in
                return fav.type == "fav"
            })
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
        }
    }
    
    @objc func didAddFavourite(_ notification: NSNotification){
        
        if let fav = notification.object as? FavoriteAddress {
            favoritesDataSource.append(fav)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
        }
    }
    
    @objc func toFavoriteVC() {
        
        let favNVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favNVC") as! UINavigationController
        let favVC = favNVC.viewControllers.first as! FavouritAddressTableViewController
        favVC.fromSetting = false
        favVC.currentUser = self.currentUser
        
        favVC.fromFav = true
        
        self.present(favNVC, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let _ = favoritesDataSource {
            return favoritesDataSource.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favPopupCell", for: indexPath) as! FavoritesTableViewCell

        // Configure the cell...
        cell.address.text = favoritesDataSource[indexPath.row].name!
        cell.favButton.tag = indexPath.row
        cell.delegate = self
        cell.favButton.isSelected = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        NotificationCenter.default.post(name: Notification.Name("did_pick_location"), object: favoritesDataSource[indexPath.row])

    }

}
extension FavoritesTableViewController : DeleteFavoritesSelectorDelegate{
    func didDeleteFavorite(index : Int) {
        self.currentUser.deleteFav(fav: favoritesDataSource[index], completionBlock: {
            (error , ref) in
            if error == nil {
                //self.currentUser.deleteFavFromLocal(fav: self.favoritesDataSource[index])
                self.favoritesDataSource.remove(at: index)
                self.tableView.reloadData()
            }
        })

    }
}

