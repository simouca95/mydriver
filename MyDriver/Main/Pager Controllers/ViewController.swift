//
//  ViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/4/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit


protocol DidPickFromRegistredAddressProtocol {
    func didPickLocation(_ fav : FavoriteAddress)
}

class ViewController: UIViewController {
    
    static let storyboardId = "ViewController"
    
    var isHome = false
    var isWork = false
    var isPin = false
    
    var loadingView : LoadingView!
    var currentUser : User!
    
    var favoritesDataSource : [FavoriteAddress]!
    
    var delegate : PagerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isPin {
            //self.delegate.didPickLocation(FavoriteAddress(name: "", lat: 0, lng: 0, type: "pin"))
            if delegate.wantedPageIndex == 4 {
                NotificationCenter.default.post(name: Notification.Name("did_pick_location"), object: FavoriteAddress(name: "", lat: 0, lng: 0, type: "pin"))
            }
        }
        else if isWork {
            if favoritesDataSource == nil || favoritesDataSource.count == 0 {
                
                self.loadingView = LoadingView.loadFromNibNamed(nibNamed: "LoadingView", bundle: nil)
                
                self.loadingView.tryAgainButton.isHidden = false
                
                self.loadingView.tryAgainButton.isEnabled = true

                self.loadingView.tryAgainButton.setTitle(NSLocalizedString("Add Work Address", comment: ""), for: .normal)
                
                self.loadingView.tryAgainButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toFavoriteVC)))
                
                self.loadingView.loadingLabel.text = NSLocalizedString("You dont have work address", comment: "")
                
                self.loadingView.activityIndicator.isHidden = true
                
                self.loadingView.frame = self.view.bounds
                
                self.view.addSubview(self.loadingView)
                
            }else{
                if delegate.wantedPageIndex == 3 {
                    
                    NotificationCenter.default.post(name: Notification.Name("did_pick_location"), object: favoritesDataSource.first!)
                    
                }
                //self.delegate.didPickLocation(favoritesDataSource.first!)
                
            }
        }
        else if isHome{
            
            if favoritesDataSource == nil || favoritesDataSource.count == 0 {
                
                self.loadingView = LoadingView.loadFromNibNamed(nibNamed: "LoadingView", bundle: nil)
                
                self.loadingView.tryAgainButton.isHidden = false
                
                self.loadingView.tryAgainButton.isEnabled = true
                
                self.loadingView.tryAgainButton.setTitle(NSLocalizedString("Add Home Address", comment: ""), for: .normal)
                
                self.loadingView.tryAgainButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.toFavoriteVC)))
                
                self.loadingView.loadingLabel.text = NSLocalizedString("You dont have home address", comment: "")
                
                self.loadingView.activityIndicator.isHidden = true
                
                self.loadingView.frame = self.view.bounds
                
                self.view.addSubview(self.loadingView)
                
            }else{
                if delegate.wantedPageIndex == 2 {
                    NotificationCenter.default.post(name: Notification.Name("did_pick_location"), object: favoritesDataSource.first!)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func toFavoriteVC() {

        let favNVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favNVC") as! UINavigationController
        let favVC = favNVC.viewControllers.first as! FavouritAddressTableViewController
        favVC.fromSetting = false
        favVC.currentUser = self.currentUser
        favVC.fromWork = self.isWork
        favVC.fromHome = self.isHome
        
        self.present(favNVC, animated: true, completion: nil)
    }
}

