//
//  MainViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import STPopup
import GoogleMaps
import GooglePlaces
import DropDown
import Alamofire
import SwiftyJSON
import FirebaseDatabase
import SDWebImage
import Toaster
import FirebaseAuth
import PullUpController
import CoreData
import NotificationBannerSwift
import SideMenu

protocol TripDidFinishProtocol {
    func tripDidFinishfunction()
}

class MainViewController: UIViewController {
    static let storyboardId = "MainViewController"
    
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var pickupTextField: UITextField!
    
    @IBOutlet weak var pickerDestinationView: UIView!
    @IBOutlet weak var destinationViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var anchorTopView: UIView!
    @IBOutlet weak var anchorBottomView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tripPickerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tripPickerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapCEnterPinImageBottomAnchor: NSLayoutConstraint!
    
    
    @IBOutlet weak var myLocationBarBtnItem: UIBarButtonItem!
    @IBOutlet weak var clearPickupTextField: UIButton!
    @IBOutlet weak var clearDestinationTextField: UIButton!
    
    @IBOutlet weak var mapView: GMSMapView!
    let bounds = GMSCoordinateBounds(
        coordinate: CLLocationCoordinate2D(latitude: 30.125665,
                                           longitude: 46.578025),
        coordinate: CLLocationCoordinate2D(latitude: 28.540784,
                                           longitude: 48.741183))
    
    @IBOutlet weak var mapCenterPinImage: UIImageView!
    @IBOutlet weak var mapCenterButton: UIButton!
    
    var reachability = Reachability()!
    
    var banner : StatusBarNotificationBanner!
    //var loadingBanner : NotificationBanner!
    
    private var locationManager : CLLocationManager!
    private let dataProvider = GoogleDataProvider()
    private let searchRadius: Double = 50
    
    var dropDown : DropDown!
    var token : GMSAutocompleteSessionToken!
    var placesClient: GMSPlacesClient!
    
    var places = [GoogleAddressStruct]()
    //var placesDetails = [String]()
    //var placeIDs = [String]()
    
    var config : Configs!
    var prices : [Price]!
    var roadDedtails : JSON!
    
    var markerPickUp : GMSMarker!
    var markerDestination : GMSMarker!
    
    var pickupPosition : Position!
    var destinationPosition : Position!
    
    var polyline : GMSPolyline!
    var requiredPlace: GooglePlace!
    
    var selectTripController : SelectRaceViewController!
    
    var jsonCourseData : JSON!
    var createdCourse: Course!
    
    var currentUser : User!
    var currentPosition : CLLocation!
    var favouriteCoodinate : CLLocationCoordinate2D!
    
    var isMenuOpen = false
    
    var hasHomeAddress = false
    var hasWorkAddress = true
    var hasFavouriteAddress = false
    
    var fromLocationButton = false
    var isProcessingDrawRoute = false
    var isProcessingChoosingManually = false

    var firstTextFieldResponder : UITextField!
    
    var favAddressHeight : CGFloat = 250
    
    
    @IBOutlet weak var searching: UILabel!
    
    @IBOutlet weak var loaderLoadingView: UIView!
    @IBOutlet weak var loaderView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        User.fetchCurrentUser { (snap) in
            if Auth.auth().currentUser != nil{
                _ = User(snap: snap!)
            }
        }
        
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        
        token = GMSAutocompleteSessionToken.init()
        placesClient = GMSPlacesClient()
        
        
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        mapView.camera = GMSCameraPosition(latitude: 29.3733578, longitude: 47.9678728, zoom: 12)
        mapView.isTrafficEnabled = false
        mapView.settings.rotateGestures = false
        mapView.isMyLocationEnabled = false
        mapView.settings.myLocationButton = false
        
        fetchPrices()
        
        fetchConfig()
        
        

        
        
        self.pickerDestinationView.isHidden = true
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didPickLocation(_:)), name: NSNotification.Name("did_pick_location"), object: nil)
        
        checkConnection()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.appEnteredToBackground()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.transparentNavigationBar()
        
        appEnteredToForeground()
        
        
        if let type = UserDefaults.standard.string(forKey: "mapType"){
            switch type{
            case "hybrid" :
                mapView.mapType = .hybrid
            case "terrain" :
                mapView.mapType = .terrain
            case "satellite" :
                mapView.mapType = .satellite
            default:
                mapView.mapType = .normal
            }
        }
        
        self.currentUser = User.fetchCurrentUserFromLocal()
        
        if let fromReg = UserDefaults.standard.value(forKey: "fromRegistrationWithReferalCode"){
            UserDefaults.standard.removeObject(forKey: "fromRegistrationWithReferalCode")
            if fromReg as? Bool == true {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: GiftViewController.storyboardId) as! GiftViewController
                
                let popupVC = STPopupController(rootViewController: vc)
                popupVC.style = .formSheet
                popupVC.present(in: self)
            }
        }
        //        UserDefaults.standard.removeObject(forKey: "acceptedCourseID")
        //        UserDefaults.standard.removeObject(forKey: "notifType20")
        //        UserDefaults.standard.removeObject(forKey: "createdCourseID")
        
        // check if user has unfinished courses
        checkUnfinishedCourses()
    }
    
    
    func fetchPrices(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchCountReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Prices")
        do {
            let test = try context.fetch(fetchCountReq)
            self.prices = [Price]()
            for object in test as! [NSManagedObject]{
                let price = Price(category: (object.value(forKey:"category") as! String),
                                  pricePerKm: (object.value(forKey:"pricePerKm") as! Double),
                                  pricePerMin: (object.value(forKey:"pricePerMin") as! Double),
                                  waintingCost: (object.value(forKey:"waitingCost") as! Double),
                                  baseFare: (object.value(forKey:"baseFare") as! Double),
                                  priceTenMin: (object.value(forKey:"priceTenMin") as! Double))
                self.prices.append(price)
            }
        } catch  {
            print(error)
        }
    }
    
    func fetchConfig()  {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchCountReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Config")
        do {
            let test = try context.fetch(fetchCountReq)
            if test.count > 0 {
                let object = test[0] as? NSManagedObject
                config = Configs(trafficModel: object?.value(forKey: "trafficModel") as! String, welcomeGift: object?.value(forKey: "welcomeGift") as! Float)
            }
        } catch  {
            print(error)
        }
    }
    
    
    func checkUnfinishedCourses() {
        
        if UserDefaults.standard.value(forKey: "acceptedCourseID") != nil || UserDefaults.standard.value(forKey: "createdCourseID") != nil{
            self.showSpinner(onView: self.view)
            let value = UserDefaults.standard.string(forKey: "acceptedCourseID") == nil ? UserDefaults.standard.string(forKey: "createdCourseID") : UserDefaults.standard.string(forKey: "acceptedCourseID")
            
            Course.fetchCurrentUserCourses(courseId: value ?? "", completionBlock: { (course) in
                self.removeSpinner()

                if course != nil {
                    
                    self.createdCourse = course
                    self.removeSpinner()
                    if self.createdCourse.status == "pending"{
                        
                        print("still loading")
                        
                    } else{
                        
                        NotificationCenter.default.removeObserver(self, name : Notification.Name.UIApplicationWillEnterForeground, object: nil)
                        
                        if  self.createdCourse.status == "canceled" || self.createdCourse.status == "postponed"{//self.createdCourse.status == "finished" ||
                            self.removeloadingView()
                            
                            UserDefaults.standard.removeObject(forKey: "createdCourseID")
                            UserDefaults.standard.removeObject(forKey: "acceptedCourseID")
                            UserDefaults.standard.removeObject(forKey: "notifType20")
                            
                        }else{
                            self.removeloadingView()
                            if UserDefaults.standard.value(forKey: "createdCourseID") != nil {
                                UserDefaults.standard.removeObject(forKey: "createdCourseID")
                                UserDefaults.standard.set(self.createdCourse.uid, forKey: "acceptedCourseID")
                            }
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let tripInProgressNC = storyboard.instantiateViewController(withIdentifier: "tripInProgress") as! UINavigationController
                            let tripInProgress = tripInProgressNC.viewControllers.first as! TripInProgressViewController
                            
                            if let json = UserDefaults.standard.value(forKey: "notifType20") as? String {
                                self.jsonCourseData = JSON.init(parseJSON: json)
                                tripInProgress.jsonCourseData = self.jsonCourseData
                            }
                            
                            tripInProgress.currentCourse = self.createdCourse
                            tripInProgress.pickupPosition = self.createdCourse.pickUpPoint
                            tripInProgress.destinationPosition = self.createdCourse.destination
                            tripInProgress.didFinishDelegate = self
                            tripInProgress.firstLoad = false
                            tripInProgress.modalPresentationStyle = .popover
                            tripInProgress.modalTransitionStyle = .partialCurl
                            self.present(tripInProgressNC, animated: true, completion: nil)
                            
                        }
                    }
                }else {
                    let banner = NotificationBanner(title: "Check Unfinished Trips", subtitle: "Error getting Current Trip", style: .danger)
                    banner.show()
                    UserDefaults.standard.removeObject(forKey: "createdCourseID")
                    UserDefaults.standard.removeObject(forKey: "acceptedCourseID")
                    UserDefaults.standard.removeObject(forKey: "notifType20")
                    
                }
            }){ (error) in
                print(error.localizedDescription)
                self.handleError(error)
                self.removeSpinner()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "tripPicker" {
            if let viewController = segue.destination as? SelectRaceViewController {
                self.selectTripController = viewController
                selectTripController.controller = self
                selectTripController.createTripDelegate = self
                
                selectTripController.tripDetail = self.roadDedtails
                selectTripController.prices = self.prices
                selectTripController.destination = destinationPosition
                selectTripController.pickup = pickupPosition
                selectTripController.selectedCell = -1
                
                if self.prices != nil {
                    selectTripController.collectionView.delegate = viewController
                    selectTripController.collectionView.dataSource = viewController
                    selectTripController.collectionView.reloadData()
                }
            }
        }
    }
    
    @IBAction func cancelTrip(_ sender: UIButton) {
        UserDefaults.standard.removeObject(forKey: "createdCourseID")
        self.removeloadingView()
        self.createdCourse.cancelTrip()
    }
    
    func showLoadingView()  {
        self.searching.text = NSLocalizedString("Searching for driver", comment: "")
        self.showSpinner(onView: self.loaderLoadingView)
        self.loaderView.isHidden = false
        
    }
    
    func removeloadingView()  {
        self.removeSpinner()
        self.loaderView.isHidden = true
    }
    
    @IBAction func openMenu(_ sender: UIBarButtonItem) {
        if isMenuOpen{
            SideMenuManager.default.menuLeftNavigationController!.dismiss(animated: true, completion: nil)
        }else{
            self.view.endEditing(true)
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        }
        isMenuOpen = !isMenuOpen
        
    }
    
    @IBAction func doneChoosingManually(_ sender: Any) {
        if favouriteCoodinate != nil {
            if bounds.contains(favouriteCoodinate) {
                self.mapCenterPinImage.isHidden = true
                self.mapCenterButton.isHidden = true
                
                let name = self.firstTextFieldResponder.text!
                
                let newFav = FavoriteAddress(name: name, lat: favouriteCoodinate.latitude, lng: favouriteCoodinate.longitude, type: "")
                
                addMarker(place: GooglePlace(fromUser: newFav.toDictionary()), isDestination: firstTextFieldResponder == destinationTextField)//self.destinationTextField.isFirstResponder
                
                self.drawRoute()
            }else {
                let banner = NotificationBanner(title: NSLocalizedString("outOfRange", comment: ""), subtitle: NSLocalizedString("Please choose another location", comment: ""), style: .danger)
                banner.show()
            }
        }else {
            let banner = NotificationBanner(title: NSLocalizedString("Invalid Address", comment: ""), subtitle: NSLocalizedString("Please enter a valid address", comment: ""), style: .danger)
            banner.show()
        }
        
    }
    
    func setMyLocationFunc()  {
        if !hasLocationPermission() {
            let alertController = UIAlertController(title: NSLocalizedString("Location Permission Required", comment: ""),
                                                    message: NSLocalizedString("Please enable location permissions in settings.", comment: ""),
                                                    preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel)
            alertController.addAction(cancelAction)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            self.myLocationBarBtnItem.isEnabled = true
            
        }else{
            self.fromLocationButton = true
            self.locationManager.startUpdatingLocation()
        }
    }
    
    @IBAction func setMyLocation(_ sender: Any) {
        self.myLocationBarBtnItem.isEnabled = false
        self.setMyLocationFunc()
    }
    
    @IBAction func clearTextField(_ sender: UIButton) {
        if sender.tag == 0{
            pickupTextField.text = ""
            
            if markerPickUp != nil {
                markerPickUp.map = nil
                
                markerPickUp = nil
            }
            
            
            hideDestinationTextField()
            
        }else{
            
            destinationTextField.text = ""
            if markerDestination != nil {
                markerDestination.map = nil
                
                markerDestination = nil
            }
            
            
            self.firstTextFieldResponder = destinationTextField
        }
        if !mapCenterPinImage.isHidden{
            self.mapCenterPinImage.isHidden = true
            self.mapCenterButton.isHidden = true
            self.removePullUpController()
            self.addPullUpController(initialState: .expanded)
        }
        
        self.clearPath()
        //self.startButton.isHidden = true
        
        sender.isHidden = true
    }
    
    func startRace(_ sender: Any) {
        
        if markerPickUp != nil , markerDestination != nil{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: SelectRaceViewController.storyboardId) as! SelectRaceViewController
            vc.tripDetail = self.roadDedtails
            vc.prices = self.prices
            vc.destination = destinationPosition
            vc.pickup = pickupPosition
            vc.createTripDelegate = self
            let popupVC = STPopupController(rootViewController: vc)
            popupVC.style = .bottomSheet
            popupVC.present(in: self)
            
        }else{
            if markerPickUp == nil ,  markerDestination == nil{
                Toast(text: "Please fill the needed address", duration: Delay.long).show()
            }else if markerPickUp == nil {
                Toast(text: "Please enter your pickup location", duration: Delay.long).show()
            }else{
                Toast(text: "Please enter your destination", duration: Delay.long).show()
            }
        }
        
    }
    
    @IBAction func search(_ sender: UITextField) {
        // The view to which the drop down will appear on
        
        self.dropDown.customCellConfiguration = { (index: Indexd, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? DropDownViewCell else { return }
            // Setup your custom UI components
            cell.optionLabel.text = self.places[index].primaryText
            cell.result.text = self.places[index].secondaryText
        }
        
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            sender.text = "\(self.places[index].primaryText) ,\(self.places[index].secondaryText)"
            
            self.placesClient.lookUpPlaceID(self.places[index].placeID, callback: { (place, error) in
                
                if let error = error {
                    print("An error occurred: \(error.localizedDescription)")
                    return
                }
                if let place = place {
                    if sender == self.destinationTextField{
                        
                        self.addMarker(place: GooglePlace(place: place), isDestination: true)
                        
                    }else if sender == self.pickupTextField{
                        
                        self.markerPickUp = nil
                        
                        self.addMarker(place: GooglePlace(place: place), isDestination: false)
                    }
                    self.drawRoute()
                    
                    let addressStr =   GooglePlace(place: place).name == "" ? "\(GooglePlace(place: place).address)"  : "\(GooglePlace(place: place).name), \(GooglePlace(place: place).address)"
                    
                    let newFav = FavoriteAddress(name: addressStr, lat: GooglePlace(place: place).coordinate.latitude, lng: GooglePlace(place: place).coordinate.longitude, type: "rec")
                    
                    self.currentUser.addRecentAddressSearch(fav: newFav)
                    
                    self.mapView.animate(toLocation: place.coordinate)
                }
            })
        }
        
        if (sender.text?.count)! > 0{
            
            if sender == destinationTextField {
                clearDestinationTextField.isHidden = false
            }else {
                clearPickupTextField.isHidden = false
            }
            
            self.dropDown.hide()
            let filter = GMSAutocompleteFilter()
            //filter.type = .address
            filter.country = "KW"
            
            placesClient.findAutocompletePredictions(fromQuery: sender.text!,
                                                     bounds: bounds,
                                                     boundsMode: GMSAutocompleteBoundsMode.bias,
                                                     filter: filter,
                                                     sessionToken: token,
                                                     callback: { (results, error) in
                                                        if let error = error {
                                                            print("Autocomplete error: \(error)")
                                                            return
                                                        }
                                                        // The list of items to display. Can be changed dynamically
                                                        
                                                        if let results = results {
                                                            //self.placesDetails = [String]()
                                                            self.places = [GoogleAddressStruct]()
                                                            //self.placeIDs = [String]()
                                                            for result in results {
                                                                let place = GoogleAddressStruct(primaryText: result.attributedPrimaryText.string, secondaryText: result.attributedSecondaryText?.string ?? " ", placeID: result.placeID)
                                                                self.places.append(place)
                                                                
                                                                //                                                                self.placesDetails.append(result.attributedSecondaryText!.string)
                                                                //                                                                self.places.append(result.attributedPrimaryText.string)
                                                                //                                                                self.placeIDs.append(result.placeID)
                                                            }
                                                            self.dropDown.dataSource = self.places.compactMap({ (place) -> String? in
                                                                return place.primaryText
                                                            })
                                                            self.dropDown.reloadAllComponents()
                                                            self.dropDown.show()
                                                        }
            })
        }else{
            if destinationTextField.isFirstResponder {
                clearDestinationTextField.isHidden = true
            }else {
                clearPickupTextField.isHidden = true
            }
            self.dropDown.hide()
            self.view.endEditing(true)
            sender.becomeFirstResponder()
        }
    }
}
// MARK: - Extentions
extension MainViewController {
    // Reachability
    
    func appEnteredToForeground(){
        
        print("discussion appEnteredToForeground")
        
        reachability.whenUnreachable = { reachability in
            if self.banner != nil {
                self.banner.dismiss()
            }
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Please verify your internet connexion", comment: ""), style: .danger)
            self.banner.autoDismiss = false
            self.banner.show()
        }
        reachability.whenReachable = { reachability in
            if self.banner != nil {
                self.banner.dismiss()
            }
            self.banner = StatusBarNotificationBanner(title: "Connected", style: .success)
            self.banner.autoDismiss = true
            self.banner.show()
        }
        
    }
    
    func appEnteredToBackground(){
        
        print("discussion appEnteredToBackground")
        if self.banner != nil {
            self.banner.dismiss()
        }
        reachability.stopNotifier()
        
    }
    
    func checkConnection(){
        reachability = Reachability()!
        if !reachability.isReachable{
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Please verify your internet connexion", comment: ""), style: .danger)
            self.banner.autoDismiss = false
            self.banner.show()
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
    }
    
    private func makePagerViewControllerIfNeeded() -> PagerViewController {
        let currentPullUpController = childViewControllers
            .filter({ $0 is PagerViewController })
            .first as? PagerViewController
        let pullUpController: PagerViewController = currentPullUpController ?? UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: PagerViewController.storyboardId) as! PagerViewController
        pullUpController.initialState = .contracted
        pullUpController.wantedPageIndex = 0
        //pullUpController.initialState = .expanded
        
        return pullUpController
    }
    
    private func addPullUpController(initialState : InitialState) {
        self.currentUser = User.fetchCurrentUserFromLocal()
        let pullUpController = makePagerViewControllerIfNeeded()
        pullUpController.initialState = initialState
        _ = pullUpController.view // call pullUpController.viewDidLoad()
        addPullUpController(pullUpController,
                            initialStickyPointOffset: pullUpController.initialPointOffset,
                            animated: true)
    }
    
    
    private func removePullUpController(){
        let pullUpController = makePagerViewControllerIfNeeded()
        removePullUpController(pullUpController, animated: true)
    }
    
    
    
    // MARK: - pick location from pager
    @objc func didPickLocation(_ notification: NSNotification) {
        if let fav = notification.object as? FavoriteAddress{
            print(fav.type!)
            if fav.type != "pin" {
                addMarker(place: GooglePlace(fromUser: fav.toDictionary()), isDestination: self.destinationTextField == self.firstTextFieldResponder)
                drawRoute()
            }else if fav.type == "pin"{
                self.mapCenterPinImage.isHidden = false
                self.mapCenterButton.isHidden = false
                self.mapCenterButton.isEnabled = false
                self.removePullUpController()
            }
            self.view.endEditing(true)
        }
    }
    
    // MARK: - Driver methode
    
    @objc func openDriverInfos(_ notification: NSNotification) {
        
        if let object = notification.object as? JSON{
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("race_accepted"), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("race_postponed"), object: nil)
            var uid = ""
            if let course = createdCourse , let courseId = course.uid {
                uid = courseId
            }else if let courseId =  UserDefaults.standard.string(forKey: "createdCourseID"){
                uid = courseId
            }
            Course.fetchCurrentUserCourses(courseId: uid, completionBlock: { (course) in
                
                self.removeloadingView()

                if course != nil {
                    self.createdCourse = course
                    
                    self.jsonCourseData = object
                    
                    UserDefaults.standard.removeObject(forKey: "createdCourseID")
                    UserDefaults.standard.set(self.jsonCourseData.rawString(), forKey: "notifType20")
                    UserDefaults.standard.set(self.createdCourse.uid, forKey: "acceptedCourseID")
                    
                    self.createdCourse.status = "accepted"
                    //self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tripInProgress") as! UINavigationController, animated: true, completion: nil)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let tripInProgressNC = storyboard.instantiateViewController(withIdentifier: "tripInProgress") as! UINavigationController
                    let tripInProgress = tripInProgressNC.viewControllers.first as! TripInProgressViewController
                    
                    if let json = UserDefaults.standard.value(forKey: "notifType20") as? String {
                        self.jsonCourseData = JSON.init(parseJSON: json)
                        tripInProgress.jsonCourseData = self.jsonCourseData
                    }
                    tripInProgress.currentCourse = self.createdCourse
                    tripInProgress.pickupPosition = self.createdCourse.pickUpPoint
                    tripInProgress.destinationPosition = self.createdCourse.destination

                    tripInProgress.didFinishDelegate = self
                    tripInProgress.modalPresentationStyle = .popover
                    tripInProgress.modalTransitionStyle = .partialCurl
                    self.present(tripInProgressNC, animated: true, completion: nil)
                }else {
                    let banner = NotificationBanner(title: "Driver Infos", subtitle: "Error getting Current Trip", style: .danger)
                    banner.show()
                }
            }){ (error) in
                print(error.localizedDescription)
                self.handleError(error)
                self.removeloadingView()
                
            }
            
        }
    }
    
    @objc func racePostponed() {
        self.removeloadingView()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("race_accepted"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("race_postponed"), object: nil)
        
        UserDefaults.standard.removeObject(forKey: "createdCourseID")
        UserDefaults.standard.removeObject(forKey: "acceptedCourseID")
        UserDefaults.standard.removeObject(forKey: "notifType20")
    }
    
    private func loadNearByDrivers() {
        
        Alamofire.request(CURRENT_DB_REF_APP + "api/driversnear", method: .post, parameters: ["latPickUp":markerPickUp.position.latitude,"longPickUp":markerPickUp.position.longitude], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            switch response.result {
                
            case .failure(_):
                print(response.description)
                break;
                
            case .success(let value):
                
                let json = JSON(value)
                
                if  json.isEmpty {
                    print("no drivers available in the area")
                    break
                }
                
                guard let drivers = Driver.getNearbyDrivers(json: json) else {
                    print("errorParsingData")
                    return;
                }
                //self.nearbyCarArray = cars
                for driver in drivers {
                    self.setupDriverMarker(driver: driver)
                }
                
                break;
            }
        }
    }
    
    func showDestinationTextField() {
        UIView.animate(withDuration: 0.7, animations: {
            self.destinationViewHeightConstraint.constant = 65
            self.pickerDestinationView.isHidden = false
            //self.destinationTextField.becomeFirstResponder()
            self.view.layoutIfNeeded()
        })
    }
    
    func hideDestinationTextField() {
        self.pickerDestinationView.isHidden = true
        if markerDestination == nil {
            self.mapView.clear()
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.destinationViewHeightConstraint.constant = 0
            self.view.layoutIfNeeded()
        }) { (done) in
            //self.view.endEditing(true)
            self.firstTextFieldResponder =  self.pickupTextField
        }
        
    }
    
    // MARK: - mapView and marker methodes
    func addMarker(place : GooglePlace , isDestination : Bool) {
        if bounds.contains(place.coordinate){
            if isDestination {
                if self.markerDestination == nil {
                    self.markerDestination = GMSMarker()
                    self.markerDestination.icon = #imageLiteral(resourceName: "pos1-1")
                    self.markerDestination.groundAnchor = CGPoint(x: 0.5, y: 1)
                }
                self.markerDestination.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                
                self.markerDestination.title = place.name
                self.markerDestination.snippet = place.address
                self.destinationTextField.text = place.name.count > 0 ?  "\(place.name) ,\( place.address)" : "\( place.address)"
                
                self.markerDestination.map = self.mapView
                self.clearDestinationTextField.isHidden = false
                self.destinationPosition = createPosition(marker: self.markerDestination, address: destinationTextField.text!)
                
            }else{
                if self.markerPickUp == nil {
                    self.markerPickUp = GMSMarker()
                    self.markerPickUp.icon = #imageLiteral(resourceName: "pos-1")
                    self.markerPickUp.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                }
                self.markerPickUp.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                
                self.markerPickUp.title = place.name
                self.markerPickUp.snippet = place.address
                self.pickupTextField.text = place.name.count > 0 ?  "\(place.name) ,\( place.address)" : "\( place.address)"
                
                self.markerPickUp.map = self.mapView
                self.clearPickupTextField.isHidden = false
                self.pickupPosition = createPosition(marker: self.markerPickUp, address: pickupTextField.text!)
                self.showDestinationTextField()
                self.firstTextFieldResponder =  destinationTextField
                
            }
            
        }else {
            let banner = NotificationBanner(title: "We don't serve in this location", subtitle: "Please choose another location", style: .danger)
            banner.show()
        }
        
    }
    
    private func setupDriverMarker( driver : Driver){
        if let vehicle = driver.vehicle , let _ = driver.currentPosition{
            
            switch vehicle.category{
            case "eco" :
                driver.currentPosition!.marker.icon = #imageLiteral(resourceName: "carEco")
            case "lux":
                driver.currentPosition!.marker.icon = #imageLiteral(resourceName: "carLux")
            default :
                driver.currentPosition!.marker.icon = #imageLiteral(resourceName: "carVan")
                
            }
            driver.currentPosition!.marker.map = self.mapView
            
            driver.updateLocation({ (snap) in
                if let value = snap.value as? NSDictionary{
                    if UserDefaults.standard.value(forKey: "acceptedCourseID") != nil || !self.headerView.isHidden { //
                        if driver.handleUint != nil {
                            print("removeCourseObserver : " ,driver.uid)
                            driver.currentPosition!.marker.map = nil
                            Driver.removeCourseObserver(driverId: driver.uid, handle: driver.handleUint)
                        }
                    }else {
                        print("update driver Location : " ,driver.uid)
                        driver.currentPosition!.marker.rotation = value["bearing"] as! Double
                        driver.currentPosition!.marker.position = CLLocationCoordinate2D(latitude: value["lat"] as! Double, longitude: value["lng"] as! Double)
                    }
                }
            }, { (error) in
                print(error.localizedDescription)
            })
        }
    }
    
    // MARK: - Places methodes
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D , completionHandler : @escaping GMSReverseGeocodeCallback) {
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate, completionHandler: completionHandler)
    }
    
    private func fetchPlaceAddress(coordinate: CLLocationCoordinate2D , completionHandler : @escaping (GooglePlace) -> Void) {
        var requiredPlace = GooglePlace()
        
        dataProvider.fetchPlacesNearCoordinate(coordinate, radius: searchRadius ) { places in
            
            if places.count > 0 {
                requiredPlace = places.first!
                completionHandler(requiredPlace)
            }else{
                self.reverseGeocodeCoordinate(coordinate, completionHandler: {response, error in
                    
                    guard let address = response?.firstResult(), let lines = address.lines else {
                        return
                    }
                    requiredPlace.address = lines.joined(separator: "\n")
                    requiredPlace.coordinate = address.coordinate
                    completionHandler(requiredPlace)
                })
            }
        }
    }
    
    
    
    // MARK: - Draw roads methodes
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D, completionHandler : @escaping (JSON) -> Void){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=\(directionsApiKey)")!
        //let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=Disneyland&destination=Universal+Studios+Hollywood&key=AIzaSyCm2kW7TPsr8F4gWWKy7blOIteUkvDOmHU")
        
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                Toast(text: error!.localizedDescription, duration: Delay.long).show()
                completionHandler(JSON.null)
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        guard let routes = json["routes"] as? NSArray else {
                            completionHandler(JSON.null)
                            Toast(text: NSLocalizedString("No Routes available", comment: ""), duration: Delay.long).show()
                            
                            return
                        }
                        
                        if (routes.count > 0) {
                            let overview_polyline = routes[0] as? NSDictionary
                            let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                            
                            let points = dictPolyline?.object(forKey: "points") as? String
                            DispatchQueue.main.async {
                                self.showPath(polyStr: points!)
                                let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(100, 100, 450, 100))
                                self.mapView!.moveCamera(update)
                                self.loadNearByDrivers()
                                self.getRoadDetails(from: source, to: destination, handler: { json in
                                    completionHandler(json)
                                })
                            }
                        }
                        else {
                            completionHandler(JSON.null)
                            Toast(text: "No Routes available", duration: Delay.long).show()
                            
                        }
                    }else {
                        completionHandler(JSON.null)
                    }
                }
                catch {
                    completionHandler(JSON.null)
                    print("error in JSONSerialization")
                    //DispatchQueue.main.async {
                    Toast(text: "error in JSONSerialization", duration: Delay.long).show()
                    //}
                }
            }
        })
        task.resume()
    }
    
    func drawRoute() {
        if isProcessingDrawRoute{
            return
        }
        self.removePullUpController()
        if self.markerPickUp != nil , self.markerDestination != nil  {
            if reachability.isReachable{
                if bounds.contains(markerPickUp.position) , bounds.contains(markerDestination.position){
                    self.showSpinner(onView: self.view)
                    
                    self.isProcessingDrawRoute = true
                    
                    self.getPolylineRoute(from: self.markerPickUp.position, to: self.markerDestination.position, completionHandler: { response in
                        //DispatchQueue.main.async {
                        self.removeSpinner()
                        self.myLocationBarBtnItem.isEnabled = true
                        if response != JSON.null{
                            self.showTripPicker()
                            self.mapView.selectedMarker =  self.markerDestination
                        }
                        self.isProcessingDrawRoute = false
                        
                        //}
                    })
                    self.view.endEditing(true)
                }else {
                    self.myLocationBarBtnItem.isEnabled = true
                }
            }else {
                self.myLocationBarBtnItem.isEnabled = true
            }
        }else if markerPickUp == nil {
            print("markerPickUp nil")
            self.myLocationBarBtnItem.isEnabled = true
        }else {
            print("markerDestination nil")
            self.myLocationBarBtnItem.isEnabled = true
        }
    }
    
    func showTripPicker() {
        
        self.navigationController?.hideNavigationBar(hide: true)
        
        if self.selectTripController != nil {
            self.selectTripController.selectTypeViewHeight.constant = 300
            self.selectTripController.enableConfirmButton(false)

        }
        self.tripPickerHeightConstraint.constant = 300
        
        self.prepare(for: UIStoryboardSegue(identifier: "tripPicker", source: self, destination: selectTripController), sender: self)
        
        
        UIView.animate(withDuration: 0.5, animations: {
            self.tripPickerBottomConstraint.constant = 0
            self.headerView.isHidden = true
            self.view.layoutIfNeeded()
        })
        
    }
    
    func hideTripPicker() {
        self.navigationController?.hideNavigationBar(hide: false)
        
        self.tripPickerHeightConstraint.constant = 300
        self.addPullUpController(initialState : .contracted)
        UIView.animate(withDuration: 0.5, animations: {
            self.tripPickerBottomConstraint.constant = -450
            self.headerView.isHidden = false
            self.view.layoutIfNeeded()
        })
    }
    
    func createPosition(marker : GMSMarker , address : String) -> Position{
        let position = Position(lat: marker.position.latitude, lng: marker.position.longitude, bearing: marker.rotation, address: address)
        return position
    }
    
    
    func getRoadDetails(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D , handler : @escaping (JSON) -> Void)  {
        
        //  url = https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&departure_time=\(String(describing: Date().addSeconds(secondsToAdd: 300).timeIntervalSince1970))&traffic_model=
        Alamofire.request("https://maps.googleapis.com/maps/api/distancematrix/json?departure_time=now&origins=\(source.latitude),\(source.longitude)&destinations=\(destination.latitude),\(destination.longitude)&traffic_model=\(self.config.trafficModel)&key=\(matrixApiKey)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(_):
                handler(JSON.null)
                print(response.description)
                Toast(text: response.description, duration: Delay.long).show()
                break;
            case .success(let value):
                let json = JSON(value)
                if  json.isEmpty || json["status"].stringValue != "OK"{
                    print("no data available")
                    handler(JSON.null)
                }else {
                    let rows = json["rows"].arrayValue
                    let elements = rows.first!["elements"].arrayValue
                    self.roadDedtails = elements.first!
                    handler(elements.first!)
                }
                
            }
        }
    }
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        //polyline.map = nil
        if polyline == nil {
            polyline = GMSPolyline(path: path)
        }
        polyline.path = path
        polyline.strokeWidth = 3.0
        polyline.strokeColor = UIColor(hex: 0x0e8dcf)
        polyline.map = mapView // Your map view
    }
    
    func clearPath(){
        if polyline != nil {
            polyline.path = nil
            polyline.map = nil
        }
    }
    
    func initMap(){
        
        self.mapView.clear()
        
        self.jsonCourseData = nil
        self.createdCourse = nil
        
        self.markerPickUp = nil
        self.markerDestination = nil
        
        self.destinationTextField.text = ""
        self.clearDestinationTextField.isHidden = true
        
        self.hideDestinationTextField()
        
        self.hideTripPicker()
        
        self.clearPath()
        self.currentPosition = nil
        
        self.firstTextFieldResponder = self.pickupTextField
        self.locationManager.startUpdatingLocation()
    }
    
    //    @objc func keyboardWillHide(notification:NSNotification){
    //
    //        self.hideFavContainerView()
    //
    //    }
    //
    //    @objc func keyboardWillShow(notification:NSNotification){
    //        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
    //
    //            let keyboardRectangle = keyboardFrame.cgRectValue
    //            let keyboardHeight = keyboardRectangle.height
    //
    //            if destinationTextField.isFirstResponder , destinationTextField.text?.count == 0 {
    //                self.favAddressHeight = self.view.frame.height - keyboardHeight - pickerDestinationView.frame.origin.y - destinationViewHeightConstraint.constant
    //            }
    //
    //            if pickupTextField.isFirstResponder , pickupTextField.text?.count == 0 {
    //                self.favAddressHeight = self.view.frame.height - keyboardHeight - pickerDestinationView.frame.origin.y
    //            }
    //        }
    //    }
    
}
// MARK: - CLLocationManagerDelegate

extension MainViewController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse ||  status == .authorizedAlways else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        locationManager.stopUpdatingLocation()
        
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15)
        
        currentPosition = location
        
        fetchPlaceAddress(coordinate: currentPosition.coordinate) { (place) in
            self.requiredPlace = place
            
            if self.fromLocationButton{
                if self.markerPickUp == nil || self.markerDestination == nil {
                    self.addMarker(place: self.requiredPlace, isDestination: self.firstTextFieldResponder == self.destinationTextField)
                    self.fromLocationButton = false
                }
            }else if self.markerPickUp == nil {
                self.addMarker(place: self.requiredPlace, isDestination: self.firstTextFieldResponder == self.destinationTextField)
            }
            self.drawRoute()
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
}
// MARK: - UITextFieldDelegate

extension MainViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.firstTextFieldResponder = textField
        self.dropDown = DropDown()
        self.dropDown.anchorView = anchorTopView
        if textField == destinationTextField{
            self.dropDown.anchorView = anchorBottomView
        }
        self.dropDown.direction = .bottom
        self.dropDown.width = self.view.frame.width
        self.dropDown.cellHeight = 50
        self.dropDown.dataSource = []
        self.dropDown.cellNib = UINib(nibName: "DropDownViewCell", bundle: nil)
        
        if firstTextFieldResponder == self.pickupTextField , self.destinationTextField.text?.count == 0{
            self.hideDestinationTextField()
        }
        if textField.text!.count == 0 {
            self.dropDown.hide()
        }
        if !mapCenterPinImage.isHidden{
            self.mapCenterPinImage.isHidden = true
            self.mapCenterButton.isHidden = true
        }
        self.addPullUpController(initialState: .expanded)
    }
}

// MARK: - GMSMapViewDelegate

extension MainViewController : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        // Get a reference for the custom overlay
        //let index:Int! = Int(marker.accessibilityLabel!)
        
        if marker == markerPickUp{
            let customInfoWindow = Bundle.main.loadNibNamed("PickupMarkerCustomView", owner: self, options: nil)?[0] as! PickupMarkerCustomView
            customInfoWindow.primaryAdress.text = marker.title?.count == 0 ? marker.snippet! :  marker.title! + " " + marker.snippet!
            return customInfoWindow
            
        }else if marker == markerDestination{//
            let customInfoWindow = Bundle.main.loadNibNamed("DestinationMarkerCustomView", owner: self, options: nil)?[0] as! DestinationMarkerCustomView
            customInfoWindow.primaryAdress.text = marker.title?.count == 0 ? marker.snippet! :  marker.title! + " " + marker.snippet!
            if let road = self.roadDedtails{
                let traffic = road["duration_in_traffic"]
                let trafficDurationText = traffic["text"].stringValue
                
                customInfoWindow.estimatedTime.text = trafficDurationText
            }
            return customInfoWindow
        }
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker == markerPickUp || marker == markerDestination{
            return true
        }
        if marker == destinationPosition.marker || marker == pickupPosition.marker{
            return true
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        if !mapCenterPinImage.isHidden{
            let point = mapView.projection.point(for: mapView.camera.target)
            self.mapCEnterPinImageBottomAnchor.constant = point.y
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        if !mapCenterPinImage.isHidden{
            
            
            let coo = mapView.camera.target
            
            if !isProcessingChoosingManually{
                isProcessingChoosingManually = true
                self.showSpinner(onView: self.mapCenterButton)
            }
            
            fetchPlaceAddress(coordinate: coo) { (place) in
                
                self.isProcessingChoosingManually = false
                
                self.removeSpinner()
                
                self.mapCenterButton.isEnabled = true
                
                self.firstTextFieldResponder.text = place.name.count == 0 ? "\(place.address)"  : "\(place.name), \(place.address)"
                
                self.favouriteCoodinate = coo
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        if !mapCenterPinImage.isHidden{
            if (gesture) {
                
                self.mapCenterButton.isEnabled = false
                
                if firstTextFieldResponder == destinationTextField  , markerDestination != nil{
                    markerDestination.map = nil
                    markerDestination = nil
                    
                }else if firstTextFieldResponder == markerPickUp, markerPickUp != nil{
                    markerPickUp.map = nil
                    markerPickUp = nil
                }
                self.clearPath()
            }
        }
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if !mapCenterPinImage.isHidden{
            if firstTextFieldResponder == destinationTextField, markerDestination != nil {
                markerDestination.map = nil
                markerDestination = nil
            }else if firstTextFieldResponder == markerPickUp, markerPickUp != nil {
                markerPickUp.map = nil
                markerPickUp = nil
            }
            return false
        }
        return false
    }
}

extension MainViewController : DidCreateTrip{
    
    
    @objc func appMovedToForeground() {
        Toast(text: "App moved to ForeGround!", duration: Delay.short).show()
        
        if let _ = UserDefaults.standard.value(forKey: "createdCourseID") as? String{
            self.checkUnfinishedCourses()
        }else {
            NotificationCenter.default.removeObserver(self, name : Notification.Name.UIApplicationWillEnterForeground, object: nil)
        }
    }
    
    func didCreateTrip(_ course: Course) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openDriverInfos(_:)), name: NSNotification.Name("race_accepted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.racePostponed), name: NSNotification.Name("race_postponed"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        
        self.createdCourse = course
        UserDefaults.standard.set(createdCourse.uid, forKey: "createdCourseID")
        
        self.showLoadingView()
        
    }
}
extension MainViewController : TripDidFinishProtocol {
    func tripDidFinishfunction() {
        self.initMap()
    }
}
