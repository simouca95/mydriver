//
//  RaceDetailsTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 6/10/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class RaceDetailsTableViewController: UITableViewController {
    
    static let storyboardId = "RaceDetailsTableViewController"
    
    @IBOutlet weak var baseFare: UILabel!
    
    @IBOutlet weak var pricePerMin: UILabel!
    
    @IBOutlet weak var waitingCost: UILabel!
    
    var price : Price!
    var controller : MainViewController!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        pricePerMin.text = "\(price.priceTenMin!) KWD"
        baseFare.text = "\(price.baseFare!) KWD"
        waitingCost.text = "\(price.waintingCost!) KWD"
    }

    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    
    
}
