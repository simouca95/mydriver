//
//  SelectRaceViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/10/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

protocol DidCreateTrip {
    func didCreateTrip(_ course : Course)
}

class SelectRaceViewController: UIViewController {
    
    static let storyboardId = "SelectRaceViewController"
    
    @IBOutlet weak var typeTitle: UILabel!
    @IBOutlet weak var typeCapacity: UILabel!
    @IBOutlet weak var confirm: UIButton!
    @IBOutlet weak var selectTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var backBtn: UIButton!
    
    var controller : MainViewController!
    
    var destination : Position!
    var pickup : Position!
    
    
    var tripDetail : JSON!
    var prices : [Price]!
    
    var selectedCell = -1
    
    let raceCategories = ["eco","lux","van","access"]
    
    var createTripDelegate : DidCreateTrip!
    
    var willDisplayCellIndex : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let screenSize = UIScreen.main.bounds.size
        
        let popupWidth = screenSize.width
        
        let popupSize = CGSize(width: popupWidth, height: 450)
        
        self.contentSizeInPopup = popupSize
        popupController?.backgroundView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))
        self.backgroundView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))
        
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.containerView.backgroundColor = UIColor.clear
        self.popupController?.hidesCloseButton = true
        self.popupController?.navigationBarHidden = true
        
        
        backBtn.setImage(getBlackBackImage(), for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func enableConfirmButton(_ enable : Bool) {
        if enable{
            confirm.isEnabled = true
            confirm.backgroundColor = UIColor.black
        }else {
            confirm.isEnabled = false
            confirm.backgroundColor = UIColor.lightGray
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.selectedCell = -1
        self.selectTypeViewHeight.constant = 300
        self.controller.tripPickerHeightConstraint.constant = 300
        self.enableConfirmButton(false)
        self.controller.hideTripPicker()

    }
    
    @IBAction func createCourse(_ sender: Any) {
        
        let duration = tripDetail["duration"]
        let durationValue = duration["value"].int
        
        let distance = tripDetail["distance"]
        let distanceValue = distance["value"].int
        
        let traffic = tripDetail["duration_in_traffic"]
        let trafficDurationValue = traffic["value"].intValue
        
        var category = ""
        
        switch selectedCell {
        case 0 , 4:
            category = "eco"
        case 1,5:
            category = "lux"
            
        case 2,6:
            category = "van"
            
        default:
            category = "access"
            
        }
        
        let price = prices.first { (item) -> Bool in
            return item.category == category
        }
        
        let cost = Course.calculateRaceCostByCategory(tripDetails: self.tripDetail, price: price!)
        
        let course = Course(from: pickup,
                            to: destination,
                            category: category,
                            estimatedDuration: trafficDurationValue,
                            estimatedDistance: distanceValue!,
                            estimatedCost :cost )
        
        course.createCourse()
        
        self.enableConfirmButton(false)

        //self.selectTypeViewHeight.constant = 300
        
        createTripDelegate.didCreateTrip(course)
    }
    
    @objc func dismissPopup(){
        
        self.popupController?.dismiss()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SelectRaceViewController : UICollectionViewDelegateFlowLayout , UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return prices == nil ? 0 : prices.count //3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "raceTypeCell", for: indexPath) as! RaceTypeCollectionViewCell
        cell.index = indexPath.row
        
        let price = prices.first { (item) -> Bool in
            return item.category == raceCategories[indexPath.row]
        }
        cell.price = price
        cell.tripDetails = self.tripDetail
        cell.controller = self.controller
        cell.iniCell()
        
        if  selectedCell == -1 || (indexPath.row != selectedCell && selectedCell < self.prices.count) {//3
            cell.raceImage.image = UIImage(named: "type_\(indexPath.row)d")
        }
        else {
            cell.raceImage.image = UIImage(named: "type_\(indexPath.row)")
        }
        
        if selectedCell > self.prices.count - 1 { //2
            cell.expandCell()
            self.collectionView.isPagingEnabled = true
        }
            
        else if selectedCell < self.prices.count  { //3 // , selectedCell != -1
            cell.collapseCell()
            self.collectionView.isPagingEnabled = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.enableConfirmButton(true)

        let cell = collectionView.cellForItem(at: indexPath) as! RaceTypeCollectionViewCell
        
        switch cell.price.category {
        case "eco":
            typeTitle.text = NSLocalizedString("Economic", comment: "")
            typeCapacity.text = "1-4"
        case "lux":
            typeTitle.text = NSLocalizedString("Luxury", comment: "")
            
            typeCapacity.text = "1-3"
        case "van":
            typeTitle.text = NSLocalizedString("Van", comment: "")
            
            typeCapacity.text = "1-6"
        default:
            typeTitle.text = NSLocalizedString("Access", comment: "")
            
            typeCapacity.text = "1-3"
        }
        
        if selectedCell == -1 || (selectedCell != indexPath.row && selectedCell < self.prices.count) {//3 none selected or selected and still collapsed
            if selectedCell != -1  {//selected already collapsed
                
                if let oldcell = collectionView.cellForItem(at: IndexPath(item: selectedCell, section: 0)) as? RaceTypeCollectionViewCell{
                    oldcell.raceImage.image =  UIImage(named: "type_\(selectedCell)d")
                }
                
                cell.raceImage.image =  UIImage(named: "type_\(indexPath.row)")
                
            }else {//none selected
                cell.raceImage.image =  UIImage(named: "type_\(indexPath.row)")
            }
            self.selectedCell = indexPath.row
            
        }else if selectedCell == indexPath.row { // will expand cell
            
            
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.selectedCell = indexPath.row + self.prices.count //3
            cell.expandCell()
            self.collectionView.isPagingEnabled = true

            UIView.transition(with: cell,
                              duration: 0.3,
                              options: .allowAnimatedContent,
                              animations: {
                                self.selectTypeViewHeight.constant = 450
                                self.controller.tripPickerHeightConstraint.constant = 450
                                self.view.layoutIfNeeded()
                                
                                DispatchQueue.main.async {
                                    collectionView.scrollToItem(at: IndexPath(item: indexPath.row , section: 0), at: [], animated: false)
                                }
                                
            })
            
        }else if selectedCell > self.prices.count - 1 { // 2 will collapse cell
            
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.selectedCell = indexPath.row
            cell.collapseCell()
            self.collectionView.isPagingEnabled = false

            UIView.transition(with: cell,
                              duration: 0.3,
                              options: .allowAnimatedContent,
                              animations: {
                                self.selectTypeViewHeight.constant = 300
                                self.controller.tripPickerHeightConstraint.constant = 300

                                self.view.layoutIfNeeded()
                                
                                DispatchQueue.main.async {
                                    collectionView.scrollToItem(at: IndexPath(item: indexPath.row , section: 0), at: [], animated: false)
                                }
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let smallWidth = (self.view.bounds.width - 40) / 3
        let smallHeight = CGFloat(120)
        
        let bigWidth = (self.view.bounds.width - 40)
        let bigHeight = CGFloat(270)
        
        if selectedCell == -1 || selectedCell < self.prices.count { //3
            //isExpanded = false
            return CGSize(width: smallWidth, height: smallHeight)
        }
        //isExpanded = true
        return CGSize(width: bigWidth, height: bigHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //when the cells are expanded the visible cell is the selected one
        if selectedCell > self.prices.count - 1 { //2
            self.selectedCell = indexPath.row + self.prices.count //3
            
            switch raceCategories[indexPath.row] {
            case "eco":
                typeTitle.text = NSLocalizedString("Economic", comment: "")
                typeCapacity.text = "1-4"
            case "lux":
                typeTitle.text = NSLocalizedString("Luxury", comment: "")
                
                typeCapacity.text = "1-3"
            case "van":
                typeTitle.text = NSLocalizedString("Van", comment: "")
                
                typeCapacity.text = "1-6"
            default:
                typeTitle.text = NSLocalizedString("Access", comment: "")
                
                typeCapacity.text = "1-3"
            }
        }
        
        self.willDisplayCellIndex = indexPath.row
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == collectionView{
            
            if selectedCell != -1 , selectedCell < self.prices.count , willDisplayCellIndex != nil {
                if collectionView.indexPathsForVisibleItems.contains(IndexPath(item: self.willDisplayCellIndex, section: 0)){

                    if selectedCell != willDisplayCellIndex{
                        let cell = collectionView.cellForItem(at: IndexPath(item: self.willDisplayCellIndex, section: 0)) as! RaceTypeCollectionViewCell //3
                        cell.raceImage.image = UIImage(named: "type_\(willDisplayCellIndex!)d") //3
                    }
                }
                
                
            }
            if collectionView.indexPathsForVisibleItems.contains(IndexPath(item: self.selectedCell - self.prices.count, section: 0)){ //3
                let cell = collectionView.cellForItem(at: IndexPath(item: self.selectedCell - self.prices.count, section: 0)) as! RaceTypeCollectionViewCell //3
                cell.raceImage.image = UIImage(named: "type_\(self.selectedCell - self.prices.count)") //3
                cell.expandCell()
                self.collectionView.isPagingEnabled = true

            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
