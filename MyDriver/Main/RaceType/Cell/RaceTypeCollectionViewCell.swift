//
//  RaceTypeCollectionViewCell.swift
//  MyDriver
//
//  Created by sami hazel on 4/10/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import SwiftyJSON
class RaceTypeCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var raceCostDetail: UILabel!
    @IBOutlet weak var detailViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var raceImage: UIImageView!
    @IBOutlet weak var raceName: UILabel!
    @IBOutlet weak var raceCost: UILabel!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var waitingCost: UILabel!
    
    @IBOutlet weak var estimatedTimeDetail: UILabel!
    @IBOutlet weak var pricePerTen: UILabel!
    @IBOutlet weak var raceBaseFare: UILabel!
    @IBOutlet weak var priceDetailLabel: UILabel!
    
    var price : Price!
    var tripDetails : JSON!
    var controller : MainViewController!
    var index = 0
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func iniCell(){
        
        if price.category == "eco"{
            raceName.text = NSLocalizedString("Economic", comment: "")
            
        }else if price.category == "lux"{
            raceName.text = NSLocalizedString("Luxury", comment: "")
            
        }else if price.category == "van" {
            raceName.text = NSLocalizedString("Van", comment: "")
            
        }else if price.category == "access" {
            raceName.text = NSLocalizedString("Access", comment: "")
        }
        
        if price == nil {
            raceCostDetail.text = "--"
            waitingCost.text = "--"
            raceCost.text = "--"
            raceBaseFare.text = "--"
            pricePerTen.text = "--"
            
        }else {
            raceCost.text =  "\(Course.calculateRaceCostByCategory(tripDetails: self.tripDetails, price: self.price)) KWD"
            
            raceBaseFare.text = "\(price.baseFare!) KWD"

            waitingCost.text = "\(price.waintingCost!) KWD"
            
            raceCostDetail.text = "\(Course.calculateRaceCostByCategory(tripDetails: self.tripDetails, price: self.price)) KWD"
            
            pricePerTen.text = "\(price.priceTenMin!) KWD"
        }
        
        if tripDetails == nil {
            estimatedTimeDetail.text = "--"
        }else {
//            let duration = tripDetails["duration"]
//            let durationStr = duration["text"].stringValue
            let traffic = tripDetails["duration_in_traffic"]
            let trafficDurationStr = traffic["text"].stringValue
            estimatedTimeDetail.text = trafficDurationStr
        }
        
        priceDetailLabel.text = NSLocalizedString("tripPickerInfo", comment: "")
    
    }
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        raceImage.image =  UIImage(named: "type_\(index)d")
//    }
    
    func collapseCell(){
        detailViewBottomConstraint.constant = -200
        detailsView.isHidden = true
        raceCost.isHidden = false
    }
    
    func expandCell() {
        detailViewBottomConstraint.constant = 0
        detailsView.isHidden = false
        raceCost.isHidden = true
    }
    @IBAction func help(_ sender: Any) {
        
        let nvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "raceDetails") as! UINavigationController
        let vc = nvc.viewControllers.first! as! RaceDetailsTableViewController
        vc.price = self.price
        vc.controller = self.controller
        self.controller.present(nvc, animated: true, completion: nil)
    }
    
}
