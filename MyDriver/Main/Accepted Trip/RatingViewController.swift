//
//  RatingViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/28/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import Cosmos


protocol DidRateTrip {
    func didRateTrip(currentCourse : Course)
}
class RatingViewController: UIViewController {

    static let storyboardId = "RatingViewController"

    @IBOutlet weak var ratingView: CosmosView!
    var currentCourse : Course!
    
    var rateDelegate : DidRateTrip!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let screenSize = UIScreen.main.bounds.size
        
        let popupWidth = screenSize.width - 40
        
        let popupHeight = CGFloat(200)
        
        let popupSize = CGSize(width: popupWidth, height: popupHeight)
        
        self.contentSizeInPopup = popupSize
        //popupController?.backgroundView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))
        
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.containerView.backgroundColor = UIColor.clear
        self.popupController?.hidesCloseButton = true
        self.popupController?.navigationBarHidden = true
        
        // Show only fully filled stars
        ratingView.settings.fillMode = .full

    }
    
    @IBAction func submit(_ sender: Any) {
        if ratingView.rating == 0 {
            self.ratingView.shake()
        }else {
            self.currentCourse.rateTrip(rating: Int(ratingView.rating))
            self.dismiss(animated: true){
                self.rateDelegate.didRateTrip(currentCourse: self.currentCourse)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
