//
//  DriverInfoViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import Cosmos
import SwiftyJSON
import GoogleMaps
import PullUpController
import Alamofire
import NotificationBannerSwift
import FirebaseStorage
import Toaster
import CoreData

class DriverInfoViewController: UIViewController {
    
    static let storyboardId = "DriverInfoViewController"
    
    var isStarted = false
    
    // MARK :- driver
    @IBOutlet weak var driverFullName: UILabel!
    @IBOutlet weak var driverPicture: UIImageView!
    @IBOutlet weak var driverRatings: CosmosView!
    @IBOutlet weak var carCategoryAvatar: UIImageView!
    @IBOutlet weak var carMark: UILabel!
    @IBOutlet weak var carRegistrationNumber: UILabel!
    
    @IBOutlet weak var estimatedDriverArrivalTime: UILabel!
    @IBOutlet weak var estimatedDestinationArrivalTime: UILabel!
    
    @IBOutlet weak var userAddress: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var callButton: UIButton!
    
    //var jsonCourseData : JSON!
    
    var controller : TripInProgressViewController!
    
    var driverPictureUrl : String!
    
    var timer : Timer!
    var timerValue = 0
    var config : Configs!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchConfig()
        
        // Do any additional setup after loading the view.
        if let course = controller.currentCourse{
            self.initDriverInfo(course)
        }else if let courseId = UserDefaults.standard.string(forKey: "acceptedCourseID"){
            Course.fetchCurrentUserCourses(courseId: courseId, completionBlock: { (course) in
                if let course = course {
                    self.initDriverInfo(course)
                }
            }) { (error) in
                print(error.localizedDescription)
                self.handleError(error)
                self.removeSpinner()
            }
        }
    }
    
    func updateDriverInfos()  {
        if self.controller.currentCourse.status == "accepted"{
            
            self.cancelButton.isHidden = false
            
        }else if self.controller.currentCourse.status == "arrived"{
            self.estimatedDriverArrivalTime.text = "--"
            if timer != nil {
                if timer.isValid{
                    timer.invalidate()
                    timerValue = 0
                }
            }
        }else if self.controller.currentCourse.status == "started"{
            self.estimatedDriverArrivalTime.text = "--"
            if timer != nil {
                if timer.isValid{
                    timer.invalidate()
                    timerValue = 0
                }
            }
            self.cancelButton.isHidden = true
        }
    }
    
    
    func initDriverInfo(_ course : Course) {
        
        //distanceLeftToArrival.text = String(describing: self.getNotification(from: object).estimatedTime / 60) + " min"
        
            destinationAddress.text = course.destination.address ?? ""
            
            userAddress.text = course.pickUpPoint.address ?? ""
            
        if let driver = course.driver  {
            self.setDriverInfos(driver: driver)
        }else {
            if let id = course.driverId, !id.isBlank{
                Driver.fetchDriverByID(driverId: id, completionHandler: { (driver) in
                    
                    if let driver = driver {
                        course.driver = driver
                        self.setDriverInfos(driver: driver)
                    }
                    
                }) { (error) in
                    print(error.localizedDescription)
                    self.handleError(error)
                }
            }

        }
            
            Course.getDriverPosition(driverId: course.driverId ?? "", { (snap) in
                if snap.exists(){
                    self.getRoadDetails(from: CLLocationCoordinate2D(latitude: snap.childSnapshot(forPath: "lat").value as! Double ,
                                                                     longitude: snap.childSnapshot(forPath: "lng").value as! Double),
                                        to: CLLocationCoordinate2D(latitude: course.pickUpPoint.latitude!,
                                                                   longitude: course.pickUpPoint.longitude!))
                }else {
                    self.getRoadDetails(from: CLLocationCoordinate2D(latitude: course.destination.latitude!,
                                                                     longitude: course.destination.longitude!),
                                        to: CLLocationCoordinate2D(latitude: course.pickUpPoint.latitude!,
                                                                   longitude: course.pickUpPoint.longitude!))
                }
            }) { (error) in
                print(error.localizedDescription)
                self.handleError(error)
                self.getRoadDetails(from: CLLocationCoordinate2D(latitude: course.destination.latitude!,
                                                                 longitude: course.destination.longitude!),
                                    to: CLLocationCoordinate2D(latitude: course.pickUpPoint.latitude!,
                                                               longitude: course.pickUpPoint.longitude!))
            }
        
        self.cancelButton.isHidden = isStarted
    }
    
    func setDriverInfos(driver : Driver)  {
        
        driverFullName.text = driver.fullName
        
        if let car = driver.vehicle {
            self.setVehicleInfos(vehicle: car)
        }else {
            
            Vehicle.fetchVehicleById(vehicleID: driver.vehicleId ?? "", completionHandler: { (car) in
                
                if let car = car {
                    driver.vehicle = car
                    self.setVehicleInfos(vehicle: car)
                }
                
            }) { (error) in
                print(error.localizedDescription)
                self.handleError(error)
            }
        }
        
        if let picURL = driver.pictureURL{
            driverPictureUrl = picURL
            
            self.driverPicture.image = UIImage(named: "profileRegestration")
            
            if driverPictureUrl.isValidURL(){
                driverPicture.sd_setImage(with: URL(string: driverPictureUrl)) { (image, error, cache, url) in
                    if error == nil {
                        self.driverPicture.image = image
                    }
                }
            }else if driverPictureUrl.count > 0{
                
                let storageRef = Storage.storage().reference()
                let profileImagesRef = storageRef.child("ProfilePictures/\(driverPictureUrl!)")
                
                // You can also access to download URL after upload.
                profileImagesRef.downloadURL { (url, error) in
                    guard let downloadURL = url else {
                        // Uh-oh, an error occurred!
                        return
                    }
                    self.driverPicture.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                        if error == nil {
                            self.driverPicture.image = image
                        }
                    })
                    
                }
            }
        }
    }
    
    func setVehicleInfos(vehicle : Vehicle)  {
        
        carMark.text = vehicle.brand + " " + vehicle.color
        
        carRegistrationNumber.text = vehicle.registrationNumber
        
        switch vehicle.category{
        case "eco" :
            carCategoryAvatar.image = #imageLiteral(resourceName: "voiture-1")
        case "access" :
            carCategoryAvatar.image = #imageLiteral(resourceName: "voiture-1")
        case "lux":
            carCategoryAvatar.image = #imageLiteral(resourceName: "voiture1-1")
        default :
            carCategoryAvatar.image = #imageLiteral(resourceName: "voiture2-1")
        }
    }
    
    func fetchConfig()  {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchCountReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Config")
        do {
            let test = try context.fetch(fetchCountReq)
            if test.count > 0 {
                let object = test[0] as? NSManagedObject
                config = Configs(trafficModel: object?.value(forKey: "trafficModel") as! String, welcomeGift: object?.value(forKey: "welcomeGift") as! Float)
            }
        } catch  {
            print(error)
        }
    }
    
    func getRoadDetails(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D)  {
        
        //  url = https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&departure_time=\(String(describing: Date().addSeconds(secondsToAdd: 300).timeIntervalSince1970))&traffic_model=
        Alamofire.request("https://maps.googleapis.com/maps/api/distancematrix/json?departure_time=now&origins=\(source.latitude),\(source.longitude)&destinations=\(destination.latitude),\(destination.longitude)&traffic_model=\(self.config.trafficModel)&key=\(matrixApiKey)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(_):
                print(response.description)
                break;
            case .success(let value):
                let json = JSON(value)
                
                if  json.isEmpty || json["status"].stringValue != "OK"{
                    print("no data available")
                    break
                }
                let rows = json["rows"].arrayValue
                let elements = rows.first!["elements"].arrayValue
                let roadDetails = elements.first!
                if (roadDetails["duration_in_traffic"] != JSON.null) {
                    let traffic = roadDetails["duration_in_traffic"]
                    let trafficDurationValue = traffic["value"].intValue
                    
                    
                    if self.controller.currentCourse.status == "accepted"{
                        self.timerValue = trafficDurationValue
                        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
                    }else {
                        self.estimatedDriverArrivalTime.text = "--"
                    }
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a" //Your date format
                    //self.estimatedDriverArrivalTime.text = dateFormatter.string(from: Date().addSeconds(secondsToAdd: trafficDurationValue))
                    self.estimatedDestinationArrivalTime.text = String(format:"%02i:%02i min", (trafficDurationValue + self.controller.currentCourse.estimatedTimeArrival) / 60, (trafficDurationValue + self.controller.currentCourse.estimatedTimeArrival) % 60)
                }else {
                    self.estimatedDriverArrivalTime.text = "--"
                    self.estimatedDestinationArrivalTime.text = "--"
                }
            }
        }
    }
    
    @objc func updateTimer(){
        // 120 sec : 2 min
        
        DispatchQueue.main.async {
            self.estimatedDriverArrivalTime.text = String(format:"%02i:%02i", self.timerValue / 60, self.timerValue % 60) //This will update the label.
        }
        if timerValue == 0 {
            self.timer.invalidate()
        }else{
            timerValue -= 1
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelTrip(_ sender: Any) {
        
        self.showAlertView(isOneButton: false, title: NSLocalizedString("Are You Sure you want to cancel the trip", comment: ""), cancelButtonTitle: NSLocalizedString("Cancel", comment: ""), submitButtonTitle: NSLocalizedString("Proceed", comment: ""), cancelHandler: nil) {
            if self.controller.currentCourse.status != "started"{
                self.controller.currentCourse.cancelTrip()
            }else {
                let banner = NotificationBanner(title: NSLocalizedString("Cancel Trip Failed", comment: ""), subtitle: NSLocalizedString("You can't cancel a started trip", comment: ""), style: .danger)
                banner.show()
            }
            
            
            //self.navigationController?.dismiss(animated: true, completion: nil)
        }
        //        self.showAlertView(isOneButton: false, title: "Your driver is on the way, would you like to cancel the trip ?", cancelButtonTitle: "No", submitButtonTitle: "Yes", cancelHandler: nil) {
        //            self.currentCourse.cancelTrip()
        //        }
    }
    @IBAction func callAction(_ sender: Any) {
        if let number = controller.currentCourse.driver!.phoneNumber{
            guard let numberTel = URL(string: "tel://" + number) else { return }
            UIApplication.shared.open(numberTel)
        }else {
            Toast(text: "Unknown phone number", duration: Delay.long).show()
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func getNotification(from userInfo: JSON) -> TripAcceptedNotificationStruct {
        
        var alert = ""
        
        var type = 0
        
        var driverPositionLatitude : Double = 0
        
        var driverPositionLongitude : Double = 0
        
        var driverPositionBearing : Double = 0
        
        var capacity : Int = 0
        
        var category = ""
        
        var registrationNumber = ""
        
        var mark = ""
        
        var color = ""
        
        var fullName = ""
        
        var phoneNumber = ""
        
        var pictureURL = ""
        
        var driverId = ""
        
        if let aps = userInfo["aps"] as? NSDictionary{
            
            if let a = aps["alert"] as? NSDictionary{
                
                if let body = a.value(forKey: "body") as? String {
                    
                    alert = body
                }
            }
        }
        
        if userInfo != nil {
            if let t = userInfo["type"].int {
                type = t
            }
            
            if  let objectId =  userInfo["driverId"].string{
                driverId = objectId
            }
            
            if  let objectId =  userInfo["lat"].double{
                driverPositionLatitude = objectId
            }
            
            if  let objectId =  userInfo["lng"].double{
                driverPositionLongitude = objectId
            }
            
            if  let objectId =  userInfo["bearing"].double{
                driverPositionBearing = objectId
            }
            
            if  let objectId =  userInfo["capacity"].int{
                capacity = objectId
            }
            if  let objectId =  userInfo["category"].string{
                category = objectId
            }
            if  let objectId =  userInfo["plateNumber"].string{
                registrationNumber = objectId
            }
            if  let objectId =  userInfo["brand"].string{
                mark = objectId
            }
            if  let objectId =  userInfo["color"].string{
                color = objectId
            }
            
            if  let objectId =  userInfo["fullName"].string{
                fullName = objectId
            }
            
            if  let objectId =  userInfo["phoneNumber"].string{
                phoneNumber = objectId
            }
            if  let objectId =  userInfo["pictureURL"].string {
                pictureURL = objectId
            }
        }else if controller.currentCourse != nil {
            type = 20
            driverId = controller.currentCourse.driverId!
            driverPositionLatitude = controller.currentCourse.driver!.currentPosition!.latitude!
            driverPositionLongitude = controller.currentCourse.driver!.currentPosition!.longitude!
            driverPositionBearing = controller.currentCourse.driver!.currentPosition!.bearing!
            capacity = controller.currentCourse.driver!.vehicle!.capacity
            category = controller.currentCourse.driver!.vehicle!.category
            registrationNumber =  controller.currentCourse.driver!.vehicle!.registrationNumber
            mark =  controller.currentCourse.driver!.vehicle!.brand
            color =  controller.currentCourse.driver!.vehicle!.color
            fullName =  controller.currentCourse.driver!.fullName
            phoneNumber = controller.currentCourse.driver!.phoneNumber
            if  let objectId =  controller.currentCourse.driver!.pictureURL{
                pictureURL = objectId
            }
        }
        
        
        
        let notification = TripAcceptedNotificationStruct.init(type: type, driverLatitude: driverPositionLatitude, driverLongitude: driverPositionLongitude, driverBearing: driverPositionBearing, capacity: capacity, color: color, mark: mark, registrationNumber: registrationNumber, category: category, driverID: driverId, driverFullName: fullName, pictureURL: pictureURL, phoneNumber: phoneNumber)
        
        return notification
    }
}
