//
//  FactureTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/9/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import Cosmos
import CoreData
import Foundation
import SwiftyJSON
import FirebaseStorage

protocol TripDidFinish {
    func tripDidFinish()
}
class FactureTableViewController: UITableViewController {

    
    static let storyboardId = "FactureTableViewController"

    
    @IBOutlet weak var done: UIButton!
    @IBOutlet weak var waitingTime: UILabel!
    @IBOutlet weak var tripBaseFare: UILabel!
    @IBOutlet weak var tripDuration: UILabel!
    @IBOutlet weak var totalFare: UILabel!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var waitingCost: UILabel!
    @IBOutlet weak var costPerMin: UILabel!
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var ratingView: CosmosView!
    
    var currentCourse : Course!
    var tripFinishedDelegate : TripDidFinish!

    var prices : [Price]!
    //var jsonCourseData : JSON!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let screenSize = UIScreen.main.bounds.size
        
        let popupWidth = screenSize.width - 40
        
        let popupHeight = screenSize.height - 100
        
        let popupSize = CGSize(width: popupWidth, height: popupHeight)
        
        self.contentSizeInPopup = popupSize
        //popupController?.backgroundView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))
        
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.containerView.backgroundColor = UIColor.clear
        self.popupController?.hidesCloseButton = true
        self.popupController?.navigationBarHidden = true
        
        self.driverImage.image = UIImage(named: "profileRegestration")
        self.driverImage.borderWithCornder(raduis: driverImage.frame.height / 2, borderColor: UIColor.black.cgColor, borderWidth: 1)
        
        fetchPrices()
        
        setCourseDetails()
    }

    func setCourseDetails() {
        
        self.ratingView.rating = Double(self.currentCourse.rating ?? 0)
        
        if let driver = self.currentCourse.driver{
            
            if  let fullName =  driver.fullName{
                driverName.text = fullName
            }
            
            if let picURL = driver.pictureURL {
                
                if picURL.isValidURL(){
                    
                    self.driverImage.sd_setImage(with: URL(string: picURL)) { (image, error, cache, url) in
                        if error == nil {
                            self.driverImage.image = image
                        }
                    }
                    
                }else if picURL.count > 0{
                    let storageRef = Storage.storage().reference()
                    let profileImagesRef = storageRef.child("ProfilePictures/\(picURL)")
                    
                    // You can also access to download URL after upload.
                    profileImagesRef.downloadURL { (url, error) in
                        guard let downloadURL = url else {
                            // Uh-oh, an error occurred!
                            return
                        }
                        self.driverImage.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                            if error == nil {
                                self.driverImage.image = image
                            }
                        })
                        
                    }
                }
            }
        }

        self.totalFare.text = "\(self.currentCourse.finalCost ?? 0) KWD"

        let tripDurationInSec = Int(self.currentCourse.finishedAt!.timeIntervalSince(self.currentCourse.startedAt!))
        
        if tripDurationInSec >= 3600{
            self.tripDuration.text = String(format:"%02ih, %02i min, %02i sec", tripDurationInSec / 3600, tripDurationInSec % 3600 / 60, tripDurationInSec % 60)
            
        }else if tripDurationInSec >= 60{
            self.tripDuration.text = String(format:"%02i min, %02i sec",tripDurationInSec / 60, tripDurationInSec % 60)
            
        }else {
            self.tripDuration.text = String(format:"%02i sec", tripDurationInSec % 60)
            
        }
        
        let tripPrice = self.prices.first(where: { (price) -> Bool in
            price.category == self.currentCourse.classCar
        })
        self.tripBaseFare.text = "\(tripPrice!.baseFare!) KWD"
        self.costPerMin.text = "\(tripPrice!.priceTenMin!) KWD"
        
        if let wt = self.currentCourse.waitingTime{
            var freeWT = 0
            var costWT : Double = 0
            for i in wt {
                
                freeWT += i.duration
                if i.duration > 300{//5 mn in seconds
                    costWT += Double(Double(i.duration - 300) / 60).rounded(.up)
                }
            }
            
            if freeWT >= 3600{
                self.waitingTime.text = String(format:"%02ih, %02i min, %02i sec", freeWT / 3600, freeWT % 3600 / 60, freeWT % 60)
                
            }else if freeWT >= 60{
                self.waitingTime.text = String(format:"%02i min, %02i sec",freeWT / 60, freeWT % 60)
                
            }else {
                self.waitingTime.text = String(format:"%02i sec", freeWT % 60)
                
            }
            
            self.waitingCost.text = "\(((costWT * tripPrice!.waintingCost) * 1000).rounded(.up) / 1000) KWD"
            
        }else{
            self.waitingTime.text = "00:00 min"
            self.waitingCost.text = "0 KWD"
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchPrices(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchCountReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Prices")
        do {
            let test = try context.fetch(fetchCountReq)
            self.prices = [Price]()
            for object in test as! [NSManagedObject]{
                let price = Price(category: object.value(forKey:"category") as? String,
                                  pricePerKm: object.value(forKey:"pricePerKm") as? Double,
                                  pricePerMin: object.value(forKey:"pricePerMin") as? Double,
                                  waintingCost: object.value(forKey:"waitingCost") as? Double,
                                  baseFare: object.value(forKey:"baseFare") as? Double,
                                  priceTenMin: object.value(forKey:"priceTenMin") as? Double)
                self.prices.append(price)
            }
        } catch  {
            print(error)
        }
    }
    

    @IBAction func done(_ sender: Any) {

        self.dismiss(animated: true) {
            self.tripFinishedDelegate.tripDidFinish()
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0 :
            return 100
        case 1:
            return 180
        case 8:
            return 80
        default:
            return 40
        }
    }
}
