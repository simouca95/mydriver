//
//  TripInProgressViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import SwiftyJSON
import GooglePlaces
import GoogleMaps
import NotificationBannerSwift
import STPopup
import FirebaseDatabase

class TripInProgressViewController: UIViewController {
    
    static let storyboardId = "TripInProgressViewController"
    
    var jsonCourseData : JSON!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    private var locationManager : CLLocationManager!
    var currentPosition : CLLocation!
    
    var currentCourse : Course!
    
    var markerPickUp : GMSMarker!
    var markerDestination : GMSMarker!
    var markerDriver : GMSMarker!
    
    var polylineTrip : GMSPolyline!
    
    var polylineDriverUser : GMSPolyline!
    
    var pickupPosition : Position!
    var destinationPosition : Position!
    var driverPosition : Position!
    
    var didFinishDelegate : TripDidFinishProtocol!
    
    var driverInfoVC : DriverInfoViewController!
    
    
    var reachability : Reachability!
    var banner : StatusBarNotificationBanner!
    var firstLoad = true
    
    var courseHandleUint : UInt!
    var driverLocationTrackingHandleUint : UInt!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // coordinate -33.86,151.20 at zoom level 6.
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        self.navigationController?.navigationBar.transparentNavigationBar()
        
        if let course = self.currentCourse{
            self.setCourseInfo(course)
        }else if let courseId = UserDefaults.standard.string(forKey: "acceptedCourseID"){
            Course.fetchCurrentUserCourses(courseId: courseId, completionBlock: { (course) in
                if let course = course {
                    self.currentCourse = course
                    self.setCourseInfo(course)
                }
            }) { (error) in
                print(error.localizedDescription)
                self.handleError(error)
                self.removeSpinner()
            }
        }
    }
    
    
    func startUpdatingCourse() {
        
        self.courseHandleUint = currentCourse.courseOnListening({ (snap) in
            let jsonCourse = JSON(snap.value!)
            print(jsonCourse)
            
            self.currentCourse.status = jsonCourse.stringValue
            
            if jsonCourse.stringValue == "canceled"{
                
                Course.removeCourseOnListeningObserver(courseId: self.currentCourse.uid, handle: self.courseHandleUint)
                
                if self.driverLocationTrackingHandleUint != nil , self.currentCourse.driverId != nil  {
                    Driver.removeCourseObserver(driverId: self.currentCourse.driverId!, handle: self.driverLocationTrackingHandleUint)
                }
                
                UserDefaults.standard.removeObject(forKey: "acceptedCourseID")
                UserDefaults.standard.removeObject(forKey: "notifType20")
                
                self.dismiss(animated: true){
                    self.didFinishDelegate.tripDidFinishfunction()
                }
            }else if jsonCourse.stringValue == "finished" {
                
                UserDefaults.standard.removeObject(forKey: "acceptedCourseID")
                UserDefaults.standard.removeObject(forKey: "notifType20")
                
                let banner = NotificationBanner(title: NSLocalizedString("You have reached your destination", comment: ""), subtitle: NSLocalizedString("Have a nice day!", comment: ""), style: .success)
                banner.show()
                
                Course.removeCourseOnListeningObserver(courseId: self.currentCourse.uid, handle: self.courseHandleUint)
                
                if self.driverLocationTrackingHandleUint != nil , self.currentCourse.driverId != nil  {
                    Driver.removeCourseObserver(driverId: self.currentCourse.driverId!, handle: self.driverLocationTrackingHandleUint)
                }
                
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: RatingViewController.storyboardId) as! RatingViewController
                vc.currentCourse = self.currentCourse
                vc.rateDelegate = self
                
                let popupVC = STPopupController(rootViewController: vc)
                popupVC.style = .formSheet
                popupVC.present(in: self)
                
            }else if jsonCourse.stringValue == "started" {
                let banner = NotificationBanner(title: NSLocalizedString("Your trip has been started", comment: ""),
                                                subtitle: NSLocalizedString("Have a nice trip!", comment: ""),
                                                style: .success)
                banner.show()
                if !self.firstLoad {
                    self.updateDriverInfo()
                }
                
            }else if jsonCourse.stringValue == "arrived" {
                self.clearPath()
                self.getPolylineRoute(from: self.pickupPosition.marker.position, to: self.destinationPosition.marker.position)
                let banner = NotificationBanner(title: NSLocalizedString("Your driver is here", comment: ""),
                                                subtitle: NSLocalizedString("Have a nice trip!", comment: ""),
                                                style: .success)
                banner.show()
                if !self.firstLoad {
                    self.updateDriverInfo()
                }
                
            }
            //}
            self.firstLoad = false
        }) { (error) in
            print(error.localizedDescription)
            self.handleError(error)
        }
    }
    
    func setCourseInfo(_ course : Course) {
        
        //distanceLeftToArrival.text = String(describing: self.getNotification(from: object).estimatedTime / 60) + " min"
        
        pickupPosition = course.pickUpPoint
        pickupPosition.marker.icon = #imageLiteral(resourceName: "pos-1")
        pickupPosition.marker.map = self.mapView
        
        destinationPosition = course.destination
        destinationPosition.marker.icon = #imageLiteral(resourceName: "MARKER 1")
        destinationPosition.marker.map = self.mapView
        
        if currentCourse.status == "arrived" || currentCourse.status == "started"{
            
            self.getPolylineRoute(from: pickupPosition.marker.position, to: destinationPosition.marker.position)
        }
        
        self.startUpdatingCourse()
        
        if let driver = course.driver  {
            self.setDriverInfos(driver: driver)
        }else {
            if let id = course.driverId, !id.isBlank{
                Driver.fetchDriverByID(driverId: id, completionHandler: { (driver) in
                    
                    if let driver = driver {
                        course.driver = driver
                        self.setDriverInfos(driver: driver)
                    }
                    
                }) { (error) in
                    print(error.localizedDescription)
                    self.handleError(error)
                }
            }
            
        }
    }
    
    func setDriverInfos(driver : Driver)  {
        
        driverPosition = Position(lat: driver.currentPosition!.latitude!, lng: driver.currentPosition!.longitude!, bearing: driver.currentPosition!.bearing!, address: "")
    
        
        if currentCourse.status == "accepted"{
            
            self.getPolylineRoute(from: driverPosition.marker.position, to: pickupPosition.marker.position)
            
        }
        
        self.driverLocationTrackingHandleUint =  Driver.updateDriverLocationByUID(driverID: driver.uid , { (snap) in
            if let value = snap.value as? NSDictionary{
                print(value)
                self.driverPosition.marker.rotation = value["bearing"] as! Double
                self.driverPosition.marker.position = CLLocationCoordinate2D(latitude: value["lat"] as! Double, longitude: value["lng"] as! Double)
            }
        }, { (error) in
            print(error.localizedDescription)
        })
        
        if let car = driver.vehicle {
            self.setVehicleInfos(vehicle: car)
        }else {
            
            Vehicle.fetchVehicleById(vehicleID: driver.vehicleId ?? "", completionHandler: { (car) in
                
                if let car = car {
                    driver.vehicle = car
                    self.setVehicleInfos(vehicle: car)
                }
                
            }) { (error) in
                print(error.localizedDescription)
                self.handleError(error)
            }
        }
        
    }
    
    func setVehicleInfos(vehicle : Vehicle)  {
        
        switch vehicle.category{
        case "eco" :
            driverPosition.marker.icon = #imageLiteral(resourceName: "carEco")
        case "lux":
            driverPosition.marker.icon = #imageLiteral(resourceName: "carLux")
        default :
            driverPosition.marker.icon = #imageLiteral(resourceName: "carVan")
        }
        
        driverPosition.marker.map = self.mapView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateDriverInfo() {
        self.prepare(for: UIStoryboardSegue(identifier: "driverInfo", source: self, destination: self.driverInfoVC), sender: self)
        self.driverInfoVC.updateDriverInfos()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "driverInfo"{
            if let viewController = segue.destination as? DriverInfoViewController {
                driverInfoVC = viewController
                //driverInfoVC.jsonCourseData = self.jsonCourseData
                driverInfoVC.controller = self
            }
        }
    }
    
    // MARK :- Draw roads
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=\(directionsApiKey)")!
        //let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=Disneyland&destination=Universal+Studios+Hollywood&key=AIzaSyCm2kW7TPsr8F4gWWKy7blOIteUkvDOmHU")
        
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                //self.activityIndicator.stopAnimating()
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        guard let routes = json["routes"] as? NSArray else {
                            DispatchQueue.main.async {
                                //self.activityIndicator.stopAnimating()
                            }
                            return
                        }
                        
                        if (routes.count > 0) {
                            let overview_polyline = routes[0] as? NSDictionary
                            let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                            
                            let points = dictPolyline?.object(forKey: "points") as? String
                            DispatchQueue.main.async {
                                //self.activityIndicator.stopAnimating()
                                self.showPath(polyStr: points!)
                                let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(100, 100, 500, 100))
                                self.mapView!.moveCamera(update)
                                //self.getRoadDetails(from: source, to: destination)
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                //self.activityIndicator.stopAnimating()
                            }
                        }
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    DispatchQueue.main.async {
                        //self.activityIndicator.stopAnimating()
                    }
                }
            }
        })
        task.resume()
    }
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        //polyline.map = nil
        if polylineDriverUser == nil {
            polylineDriverUser = GMSPolyline(path: path)
        }
        polylineDriverUser.path = path
        polylineDriverUser.strokeWidth = 3.0
        polylineDriverUser.strokeColor = UIColor(hex: 0x0e8dcf)
        polylineDriverUser.map = mapView // Your map view
    }
    
    func clearPath(){
        if polylineDriverUser != nil {
            polylineDriverUser.path = nil
            polylineDriverUser.map = nil
        }
    }
    
    
    func getNotification(from userInfo: JSON) -> TripAcceptedNotificationStruct {
        
        var alert = ""
        
        var type = 0
        
        var driverPositionLatitude : Double = 0
        
        var driverPositionLongitude : Double = 0
        
        var driverPositionBearing : Double = 0
        
        var capacity : Int = 0
        
        var category = ""
        
        var registrationNumber = ""
        
        var mark = ""
        
        var color = ""
        
        var fullName = ""
        
        var phoneNumber = ""
        
        var pictureURL = ""
        
        var driverId = ""
        
        if let aps = userInfo["aps"] as? NSDictionary{
            
            if let a = aps["alert"] as? NSDictionary{
                
                if let body = a.value(forKey: "body") as? String {
                    
                    alert = body
                }
            }
        }
        
        if userInfo != nil {
            if let t = userInfo["type"].int {
                type = t
            }
            
            if  let objectId =  userInfo["driverId"].string{
                driverId = objectId
            }
            
            if  let objectId =  userInfo["lat"].double{
                driverPositionLatitude = objectId
            }
            
            if  let objectId =  userInfo["lng"].double{
                driverPositionLongitude = objectId
            }
            
            if  let objectId =  userInfo["bearing"].double{
                driverPositionBearing = objectId
            }
            
            if  let objectId =  userInfo["capacity"].int{
                capacity = objectId
            }
            if  let objectId =  userInfo["category"].string{
                category = objectId
            }
            if  let objectId =  userInfo["plateNumber"].string{
                registrationNumber = objectId
            }
            if  let objectId =  userInfo["brand"].string{
                mark = objectId
            }
            if  let objectId =  userInfo["color"].string{
                color = objectId
            }
            
            if  let objectId =  userInfo["fullName"].string{
                fullName = objectId
            }
            
            if  let objectId =  userInfo["phoneNumber"].string{
                phoneNumber = objectId
            }
            if  let objectId =  userInfo["pictureURL"].string{
                pictureURL = objectId
            }
        }else if currentCourse != nil {
            type = 20
            driverId = currentCourse.driverId!
            driverPositionLatitude = currentCourse.driver!.currentPosition!.latitude!
            driverPositionLongitude = currentCourse.driver!.currentPosition!.longitude!
            driverPositionBearing = currentCourse.driver!.currentPosition!.bearing!
            capacity = currentCourse.driver!.vehicle!.capacity
            category = currentCourse.driver!.vehicle!.category
            registrationNumber =  currentCourse.driver!.vehicle!.registrationNumber
            mark =  currentCourse.driver!.vehicle!.brand
            color =  currentCourse.driver!.vehicle!.color
            fullName =  currentCourse.driver!.fullName
            phoneNumber = currentCourse.driver!.phoneNumber
            if  let objectId =  currentCourse.driver!.pictureURL{
                pictureURL = objectId
            }
        }
        
        let notification = TripAcceptedNotificationStruct.init(type: type, driverLatitude: driverPositionLatitude, driverLongitude: driverPositionLongitude, driverBearing: driverPositionBearing, capacity: capacity, color: color, mark: mark, registrationNumber: registrationNumber, category: category, driverID: driverId, driverFullName: fullName, pictureURL: pictureURL, phoneNumber: phoneNumber)
        
        return notification
    }
    
}


extension TripInProgressViewController{
    // Reachability
    
    func appEnteredToForeground(){
        
        print("discussion appEnteredToForeground")
        
        
        reachability = Reachability()!
        
        reachability.whenUnreachable = { reachability in
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Please verify your internet connexion", comment: ""), style: .danger)
            self.banner.autoDismiss = false
            self.banner.show()
        }
        reachability.whenReachable = { reachability in
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Connected", comment: ""), style: .success)
            self.banner.autoDismiss = true
            self.banner.show()
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func appEnteredToBackground(){
        
        print("discussion appEnteredToBackground")
        
        reachability.stopNotifier()
        
    }
}
// MARK: - CLLocationManagerDelegate

extension TripInProgressViewController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        locationManager.stopUpdatingLocation()
        
        //mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 12, bearing: 0, viewingAngle: 0)
        
        //        currentPosition = location
        //        
        //        if self.markerPickUp == nil {
        //            self.markerPickUp = GMSMarker()
        //            self.markerPickUp.icon = #imageLiteral(resourceName: "pos1-1")
        //            self.markerPickUp.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        //        }
        //        self.markerPickUp.position = CLLocationCoordinate2D(latitude: currentPosition.coordinate.latitude, longitude: currentPosition.coordinate.longitude)
        //
        //        self.markerPickUp.title = pickupPosition.address
        //
        //        self.markerPickUp.map = self.mapView
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
}

extension TripInProgressViewController : TripDidFinish {
    func tripDidFinish() {
        self.dismiss(animated: true) {
            self.didFinishDelegate.tripDidFinishfunction()
        }
    }
}

extension TripInProgressViewController : DidRateTrip {
    func didRateTrip(currentCourse : Course) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: FactureTableViewController.storyboardId) as! FactureTableViewController
        self.showSpinner(onView: self.view)
        Course.fetchCurrentUserCourses(courseId: self.currentCourse.uid, completionBlock: { (course) in
            self.removeSpinner()

            if course != nil {
                self.currentCourse = course
                vc.currentCourse = self.currentCourse
                //vc.jsonCourseData = self.jsonCourseData
                vc.tripFinishedDelegate = self
                
                let popupVC = STPopupController(rootViewController: vc)
                popupVC.style = .formSheet
                popupVC.present(in: self)
            }else {
                let banner = NotificationBanner(title: "Loading Facture", subtitle: "Error getting Current Trip", style: .danger)
                banner.show()
            }

        }) { (error) in
            print(error.localizedDescription)
            self.handleError(error)
            self.removeSpinner()
        }
    }
}
