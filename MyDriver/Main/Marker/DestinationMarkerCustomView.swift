//
//  DestinationMarkerCustomView.swift
//  MyDriver
//
//  Created by sami hazel on 4/9/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class DestinationMarkerCustomView: UIView {
    @IBOutlet weak var primaryAdress: UILabel!
    @IBOutlet weak var estimatedTime: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */


}
