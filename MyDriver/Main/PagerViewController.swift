//
//  PagerViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/1/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import PullUpController



enum InitialState {
    case contracted
    case expanded
}

class PagerViewController: PullUpController {

    var initialPointOffset: CGFloat {
        switch initialState {
        case .contracted:
            return 50
        case .expanded:
            return pullUpControllerPreferredSize.height
        }
    }
    static let storyboardId = "PagerViewController"

    @IBOutlet weak var firstPreviewView: UIView!
    
    var pageMenu: CAPSPageMenu!
    var controller : MainViewController!
    var currentUser : User!
    
    var loadingView: LoadingView!

    var initialState: InitialState = .contracted

    public var portraitSize: CGSize = .zero
    
    var wantedPageIndex = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.currentUser = User.fetchCurrentUserFromLocal()
        NotificationCenter.default.addObserver(self, selector: #selector(self.didPickPage(_:)), name: NSNotification.Name("did_pick_page_menu"), object: nil)
        
        setupPager()
        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                              height: self.view.frame.maxY - 200)
        
        
    }
    
    // MARK: - pick page from pager
    @objc func didPickPage(_ notification: NSNotification) {
        if let indexPage = notification.object as? Int{
            print(indexPage)
            self.wantedPageIndex = indexPage
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.layer.cornerRadius = 12
    }
    
    override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
        //        print("will move to \(stickyPoint)")
    }
    
    override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
        //        print("did move to \(stickyPoint)")
    }
    
    override func pullUpControllerDidDrag(to point: CGFloat) {
        //        print("did drag to \(point)")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.currentUser = User.fetchCurrentUserFromLocal()
        self.setupPager()
    }
    
    func setupPager(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let recentAddressController = storyboard.instantiateViewController(withIdentifier: RecentAddressTableViewController.storyboardId) as! RecentAddressTableViewController
        
        recentAddressController.title = ""
        
        //recentAddressController.delegate = controller
        
        recentAddressController.recentFavoritesDataSource = self.currentUser.favoriteAddress?.filter({ (fav) -> Bool in
            return fav.type == "rec"
        })
        
        
        let favsController = storyboard.instantiateViewController(withIdentifier: FavoritesTableViewController.storyboardId) as! FavoritesTableViewController
        
        favsController.title = ""
        
        //favsController.delegate = controller
        
        favsController.favoritesDataSource = self.currentUser.favoriteAddress?.filter({ (fav) -> Bool in
            return fav.type == "fav"
        })
        
        let workFavController = storyboard.instantiateViewController(withIdentifier: ViewController.storyboardId) as! ViewController
        
        workFavController.title = ""
        
        workFavController.isWork = true
        
        workFavController.currentUser = currentUser
        
        workFavController.delegate = self
        
        workFavController.favoritesDataSource = self.currentUser.favoriteAddress?.filter({ (fav) -> Bool in
            return fav.type == "work"
        })
        
        let homeFavController = storyboard.instantiateViewController(withIdentifier: ViewController.storyboardId) as! ViewController
        
        homeFavController.title = ""
        
        homeFavController.isHome = true
        
        homeFavController.currentUser = currentUser
        
        homeFavController.delegate = self
        
        homeFavController.favoritesDataSource = self.currentUser.favoriteAddress?.filter({ (fav) -> Bool in
            return fav.type == "home"
        })
        
        
        let pinPagerController = storyboard.instantiateViewController(withIdentifier: ViewController.storyboardId) as! ViewController
        
        pinPagerController.title = ""
        
        pinPagerController.isPin = true
        
        pinPagerController.currentUser = currentUser
        
        pinPagerController.delegate = self
        
        let controllerArray : [UIViewController] = [recentAddressController,favsController, homeFavController, workFavController, pinPagerController]
        
        let parameters: [CAPSPageMenuOption] = [
            
            .MenuItemSeparatorColor(UIColor(hex:0x99aab8).withAlphaComponent(0.5)),
            
            .ViewBackgroundColor(.white),
            
            .menuWithImage(true),
            
            .MenuHeight(40),
            
            .menuIconUnSelected(["time-left1","star1","home","sac1","map1"]),
            
            .menuIconSelected(["time-left","star","home-2","sac1-1","map"]),
            
            .CenterMenuItems(true),
            
            .ScrollMenuBackgroundColor(.white),
            
            .UseMenuLikeSegmentedControl(true),
            
            .centeredMenu(true)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        pageMenu.delegate = self
        
        self.addChildViewController(pageMenu!)
        
        pageMenu!.view.frame = CGRect(x: 0, y:40, width: self.view.frame.width, height: self.view.frame.height)
        
        pageMenu!.view.autoresizesSubviews = false
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            pageMenu!.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            pageMenu!.menuItems.forEach { (item) in
                item.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            pageMenu!.controllerArray.forEach { (viewController) in
                viewController.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
        }
        
        view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController: self)
    }

    
    
    // MARK: - PullUpController
    
    override var pullUpControllerPreferredSize: CGSize {
        return portraitSize
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        switch initialState {
        case .contracted:
            return [firstPreviewView.frame.maxY]
        case .expanded:
            return [firstPreviewView.frame.minY, firstPreviewView.frame.maxY]
        }
    }
    
    override var pullUpControllerBounceOffset: CGFloat {
        return 20
    }
    
    override func pullUpControllerAnimate(action: PullUpController.Action,
                                          withDuration duration: TimeInterval,
                                          animations: @escaping () -> Void,
                                          completion: ((Bool) -> Void)?) {
        switch action {
        case .move:
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           usingSpringWithDamping: 0.7,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: animations,
                           completion: completion)
        default:
            UIView.animate(withDuration: 0.3,
                           animations: animations,
                           completion: completion)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PagerViewController : CAPSPageMenuDelegate{
    func willMoveToPage(controller: UIViewController, index: Int) {
        print(index)
    }
    func didMoveToPage(controller: UIViewController, index: Int) {
        print(index)
        if [4].contains(index) {
            self.pageMenu.moveToPage(index: 0, animated: false)
        }
    }
}
