//
//  InviteViewController.swift
//  MyDriver
//
//  Created by sami hazel on 6/12/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import MessageUI

class InviteViewController: UIViewController {//, MFMessageComposeViewControllerDelegate

    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    var currentUser : User!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backBtn.image = getBackImage()
        self.currentUser = User.fetchCurrentUserFromLocal()
        self.code.text = self.currentUser.referralCode
    }
    
    @IBAction func invite(_ sender: Any) {
//        if (MFMessageComposeViewController.canSendText()) {
//            let controller = MFMessageComposeViewController()
//            var phoneNumbers = [String]()
//            controller.body = "Hi, i am using Hola, a new application for real time Video chat, 100% free & secure, download it now from AppStore"
//            for contact in participantsOriginal {
//                if contact.1 {
//                    phoneNumbers.append(contact.0.phoneNumber!.first!)
//                }
//            }
//            controller.recipients = phoneNumbers
//            controller.messageComposeDelegate = self
//            self.present(controller, animated: true, completion: nil)
//        }
        // set up activity view controller
        let text = "\(NSLocalizedString("InviteFriend1", comment: "")) '\(self.currentUser.referralCode ?? "")' \(NSLocalizedString("InviteFriend2", comment: ""))"

        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
//    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
//        switch (result.rawValue) {
//        case MessageComposeResult.cancelled.rawValue:
//            print("Message was cancelled")
//            self.dismiss(animated: true, completion: nil)
//        case MessageComposeResult.failed.rawValue:
//            print("Message failed")
//            self.dismiss(animated: true, completion: nil)
//        case MessageComposeResult.sent.rawValue:
//            print("Message was sent")
//            self.dismiss(animated: true, completion: nil)
//        default:
//            break;
//        }
//        self.navigationController?.popViewController(animated: true)
//    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
