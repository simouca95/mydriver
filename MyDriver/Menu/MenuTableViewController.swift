//
//  MenuTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import SDWebImage
import FirebaseStorage

class MenuTableViewController: UITableViewController {

    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    @IBOutlet weak var promoBalance: UILabel!
    
    var currentUser : User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avatar.layer.cornerRadius = self.avatar.frame.height / 2
        avatar.borderWithCornder(raduis: self.avatar.frame.height / 2, borderColor: UIColor.white.cgColor, borderWidth: 2)
        avatar.clipsToBounds = true
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.transparentNavigationBar()

        currentUser = User.fetchCurrentUserFromLocal()

        self.fullName.text = currentUser.firstName + " " + currentUser.lastName
        
        promoBalance.text = "\(currentUser.promoWallet ?? 0) KWD"
        
        if let picURL = currentUser.pictureURL{
            
            if picURL.isValidURL(){
                avatar.sd_setImage(with: URL(string: picURL)) { (image, error, cache, url) in
                    if error == nil {
                        self.avatar.image = image
                    }
                }
            }else if picURL.count > 0{
                
                let storageRef = Storage.storage().reference()
                let profileImagesRef = storageRef.child("ProfilePictures/\(picURL)")
                
                // You can also access to download URL after upload.
                profileImagesRef.downloadURL { (url, error) in
                    guard let downloadURL = url else {
                        // Uh-oh, an error occurred!
                        return
                    }
                    self.avatar.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                        if error == nil {
                            self.avatar.image = image
                        }
                    })
                    
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)

//        switch indexPath.row {
//        case 1:
//            tableView.deselectRow(at: indexPath, animated: true)
//            if let nvc = self.navigationController {
//                nvc.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: TripsViewController.storyboardId), animated: true)
//
//            }
//
//        case 3 :
//            tableView.deselectRow(at: indexPath, animated: true)
//            if let nvc = self.navigationController {
//                nvc.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ContactUsViewController.storyboardId), animated: true)
//            }
//
//        case 2 :
//            tableView.deselectRow(at: indexPath, animated: true)
//            if let nvc = self.navigationController {
//                nvc.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: SettingsTableViewController.storyboardId), animated: true)
//            }
//
//        default:
//            tableView.deselectRow(at: indexPath, animated: true)
//
//        }
    }
}
