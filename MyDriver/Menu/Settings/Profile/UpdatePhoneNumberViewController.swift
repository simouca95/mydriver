//
//  UpdatePhoneNumberViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import STPopup
import FirebaseAuth

class UpdatePhoneNumberViewController: UIViewController {

    static let storyboardId = "UpdatePhoneNumberViewController"
    
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryFlag: UILabel!
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var verify: LoadingButton!
    var countryCode: String!
    var countrySelector: STPopupController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backBtn.image = getBackImage()

        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            self.countryFlag.text = countryCode.emojiFlag()
            self.countryLabel.text = countryCode
            
        }else{
            self.countryFlag.text = "KW".emojiFlag()
            self.countryLabel.text = "+965"
        }
        
        let countryVC = CountrySelectorViewController(nibName: "CountrySelectorViewController", bundle: nil)
        countryVC.delegate = self
        self.countrySelector = STPopupController(rootViewController: countryVC)
        self.countryFlag.isUserInteractionEnabled = true
        self.countryFlag.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectCountry(_:))))
        
        phoneNumber.text = Auth.auth().currentUser!.phoneNumber?.replacingOccurrences(of: "+965", with: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func update(_ sender: Any) {
        
        let phoneNum = (countryLabel.text! + phoneNumber.text!).replacingOccurrences(of: " ", with: "")
        if phoneNum.isPhoneNumber{
            if phoneNum == Auth.auth().currentUser!.phoneNumber{
                self.showAlertView(isOneButton: true, title: "Please enter a new phone number", cancelButtonTitle: "OK", submitButtonTitle: "Cancel")
            }else{
                self.verify.loading = true
                PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationID, error) in
                    self.verify.loading = false
                    if let error = error {
                        print(error.localizedDescription)
                        self.handleError(error)
                        return
                    }
                    // Sign in using the verificationID and the code sent to the user
                    // ...
                    UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                    
                    let stb = UIStoryboard(name: "Main", bundle: nil)
                    let introMain = stb.instantiateViewController(withIdentifier: ConfirmUpdatePhoneNumberViewController.storyboardId) as! ConfirmUpdatePhoneNumberViewController
                    introMain.phoneNumberStr = phoneNum
                    self.navigationController?.pushViewController(introMain, animated: true)
                }
            }
        }
        
    }
    
    @objc func selectCountry(_ sender: UITapGestureRecognizer){
        self.countrySelector.present(in: self)
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UpdatePhoneNumberViewController: CountrySelectorDelegate{
    
    func didSelectCountry(country: Country, viewController: UIViewController) {
        
        viewController.dismiss(animated: true) {
            
            self.countryFlag.text = country.code.emojiFlag()
            
            self.countryLabel.text = country.dialCode
            
        }
    }
}
