//
//  ConfirmUpdatePhoneNumberViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import Toaster

class ConfirmUpdatePhoneNumberViewController: UIViewController {

    static let storyboardId = "ConfirmUpdatePhoneNumberViewController"

    var verificationID : String?
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var textfield2: UITextField!
    @IBOutlet weak var textfield3: UITextField!
    @IBOutlet weak var textfield4: UITextField!
    @IBOutlet weak var textfield5: UITextField!
    @IBOutlet weak var textfield6: UITextField!
    
    @IBOutlet weak var activateButton: LoadingButton!
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    var timer : Timer!
    // This test verification code is specified for the given test phone number in the developer console.
    var testVerificationCode = "000000"
    var phoneNumberStr = "+21687555555"
    var value = 59
    var currentUser : User!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        
        currentUser = User.fetchCurrentUserFromLocal()
        backBtn.image = getBackImage()
        textfield1.becomeFirstResponder()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimerLaber)), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func updateTimerLaber(){
        DispatchQueue.main.async {
            self.timerLabel.text = String(format:"%02i:%02i", self.value / 60, self.value % 60) //This will update the label.
        }
        value -= 1
        if value == 0 {
            timer.invalidate()
            resendButton.isEnabled = true
        }
    }
    @IBAction func resendSMS(_ sender: Any) {
        resendButton.isEnabled = false
        
        value = 59
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimerLaber)), userInfo: nil, repeats: true)
        
    }
    
    @IBAction func activate(_ sender: Any) {
        activateButton.loading = true
        
        let activationCode = "\(textfield1.text!)\(textfield2.text!)\(textfield3.text!)\(textfield4.text!)\(textfield5.text!)\(textfield6.text!)"
        
        if activationCode.count == 6 {
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID!,
                verificationCode: activationCode)
            Auth.auth().currentUser?.updatePhoneNumber(credential, completion: { (error) in
                self.activateButton.loading = false
                if let error = error {
                    print(error.localizedDescription)
                    self.handleError(error)
                    return
                }
                self.currentUser.updateUserPhoneNumber(number: self.phoneNumberStr)
                self.navigationController?.popToRootViewController(animated: true)//popToViewController(vc, animated: true)
            })
        }else {
            self.activateButton.loading = false
            self.textfield1.superview?.superview?.shake()
        }
    }
    
    
    @IBAction func editingChanged(_ sender: UITextField) {
        print("editing changed")
        if sender == textfield1{
            if sender.text?.count == 1 {
                textfield2.becomeFirstResponder()
            }
        }
        if sender == textfield2{
            if sender.text?.count == 1 {
                textfield3.becomeFirstResponder()
            }else if sender.text?.count == 0{
                textfield1.becomeFirstResponder()
            }
        }
        if sender == textfield3{
            if sender.text?.count == 1 {
                textfield4.becomeFirstResponder()
            }else if sender.text?.count == 0{
                textfield2.becomeFirstResponder()
            }
        }
        if sender == textfield4{
            if sender.text?.count == 1 {
                textfield5.becomeFirstResponder()
            }else if sender.text?.count == 0{
                textfield3.becomeFirstResponder()
            }
        }
        if sender == textfield5{
            if sender.text?.count == 1 {
                textfield6.becomeFirstResponder()
            }else if sender.text?.count == 0{
                textfield4.becomeFirstResponder()
            }
        }
        if sender == textfield6{
            if sender.text?.count == 1 {
                textfield6.resignFirstResponder()
            }else if sender.text?.count == 0{
                textfield5.becomeFirstResponder()
            }
        }
    }
}
