//
//  ConfirmPasswordViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import Toaster
import FirebaseAuth

class ConfirmPasswordViewController: UIViewController {

    static let storyboardId = "ConfirmPasswordViewController"
   
    @IBOutlet weak var confirm: LoadingButton!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var confirmPassword: UITextField!
    
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        backBtn.image = getBackImage()
    }

    @IBAction func confirm(_ sender: Any) {
        let password = self.password.text
        let cpwd = self.confirmPassword.text
        if password == cpwd {
            confirm.loading = true
            Auth.auth().currentUser!.updatePassword(to: password!, completion: { (error) in
                self.confirm.loading  = false
                if let error = error {
                    // An error happened.
                    self.handleError(error)
                    return
                }
                self.navigationController?.popToRootViewController(animated: true)
            })

        }else {
            Toast(text: "Passwords does not confirm", duration: Delay.long).show()
            self.password.shake()
            self.confirmPassword.shake()
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
