//
//  UpdateEmailViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class UpdateEmailViewController: UIViewController {

    static let storyboardId = "UpdateEmailViewController"

    @IBOutlet weak var verify: LoadingButton!
    @IBOutlet weak var clearTextField: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backBtn.image = getBackImage()
    }

    @IBAction func clear(_ sender: Any) {
    }
    
    @IBAction func update(_ sender: Any) {
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
