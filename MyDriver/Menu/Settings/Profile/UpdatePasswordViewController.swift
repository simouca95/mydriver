//
//  UpdatePasswordViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth

class UpdatePasswordViewController: UIViewController {

    static let storyboardId = "UpdatePasswordViewController"

    @IBOutlet weak var currentPassword: UITextField!
    
    @IBOutlet weak var verify: LoadingButton!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backBtn.image = getBackImage()
    }

    @IBAction func verify(_ sender: Any) {
        if currentPassword.text!.count > 0 {
            self.verify.loading = true
            let credential = EmailAuthProvider.credential(withEmail: Auth.auth().currentUser!.email!, password: currentPassword.text!)
            
            Auth.auth().currentUser!.reauthenticateAndRetrieveData(with: credential, completion: { (result, error) in
                self.verify.loading = false
                if let error = error {
                    // An error happened.
                    self.handleError(error)
                } else {
                    // User re-authenticated.
                    
                    let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ConfirmPasswordViewController.storyboardId) as! ConfirmPasswordViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }

            })
        }

        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
