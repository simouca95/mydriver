//
//  SettingsTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import FBSDKLoginKit
import NotificationBannerSwift

class SettingsTableViewController: UITableViewController {
    
    static let storyboardId = "SettingsTableViewController"
    
    @IBOutlet weak var currentMapType: UILabel!
    @IBOutlet weak var currentLanguage: UILabel!
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        backBtn.image = getBackImage()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
        if !Auth.auth().currentUser!.isEmailVerified{
            let banner = NotificationBanner(title: "Email Verification", subtitle: "Please check your email.", style: .warning)
            banner.show()
        }
        
        if Languages.currentLanguage().starts(with: "ar"){
            
            self.currentLanguage.text = NSLocalizedString("Arabic", comment: "")
            
        }else if Languages.currentLanguage().starts(with: "en"){
            
            self.currentLanguage.text = NSLocalizedString("English", comment: "")
            
        }else {
            self.currentLanguage.text = NSLocalizedString("French", comment: "")
        }

        
        if let type = UserDefaults.standard.string(forKey: "mapType"){
            self.currentMapType.text = type
        }else {
            self.currentMapType.text = "Normal"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.cancelTransparentNavigationBar()
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1 {
            let message = NSLocalizedString("Change the language", comment: "")
            
            let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .actionSheet)
            
            let firstChoice = UIAlertAction.init(title: NSLocalizedString("Arabic", comment: ""), style: .default) { (alert) in
                if !Languages.currentLanguage().starts(with: "ar") {
                    
                    Languages.setAppLanguage("ar")
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
                    
                    self.flipView()
                }
            }
            alertController.addAction(firstChoice)
            
            let secondChoice = UIAlertAction.init(title: NSLocalizedString("English", comment: ""), style: .default){ (alert) in
                if !Languages.currentLanguage().starts(with: "en"){
                    
                    Languages.setAppLanguage("en")
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                    
                    self.flipView()
                }
            }
            alertController.addAction(secondChoice)
            
            let thirdChoice = UIAlertAction.init(title: NSLocalizedString("French", comment: ""), style: .default){ (alert) in
                if !Languages.currentLanguage().starts(with: "fr") {
                    
                    Languages.setAppLanguage("fr")
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                    
                    self.flipView()
                }
            }
            alertController.addAction(thirdChoice)
            
            let cancelChoice = UIAlertAction.init(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(cancelChoice)
            
            if let popoverController = alertController.popoverPresentationController {
                
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
                
            }
            
            self.present(alertController, animated: true, completion: nil)
        }
        if indexPath.row == 2 {
            let alertController = UIAlertController.init(title: NSLocalizedString("Change map type", comment: ""), message: NSLocalizedString("Choose", comment: ""), preferredStyle: .actionSheet)
            
            let firstChoice = UIAlertAction.init(title: NSLocalizedString("Normal", comment: ""), style: .default) { (alert) in
                UserDefaults.standard.set("normal", forKey: "mapType")
            }
            
            alertController.addAction(firstChoice)
            
            let secondChoice = UIAlertAction.init(title: NSLocalizedString("Terrain", comment: ""), style: .default) { (alert) in
                UserDefaults.standard.set("terrain", forKey: "mapType")
            }
            alertController.addAction(secondChoice)
            
            let thirdChoice = UIAlertAction.init(title: NSLocalizedString("Satellite", comment: ""), style: .default) { (alert) in
                UserDefaults.standard.set("satellite", forKey: "mapType")
            }
            alertController.addAction(thirdChoice)
            
            let forthChoice = UIAlertAction.init(title: NSLocalizedString("Hybrid", comment: ""), style: .default) { (alert) in
                UserDefaults.standard.set("hybrid", forKey: "mapType")
            }
            alertController.addAction(forthChoice)
            
            let cancelChoice = UIAlertAction.init(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(cancelChoice)
            
            if let popoverController = alertController.popoverPresentationController {
                
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
                
            }
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        if indexPath.row == 0 {
            
            self.navigationController?.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ProfileTableViewController.storyboardId) as! ProfileTableViewController, animated: true)
        }
        
        if indexPath.row == 3 {
            let firebaseAuth = Auth.auth()
            
            self.showAlertView(isOneButton: false, title: NSLocalizedString("Would you like to log out", comment: ""), cancelButtonTitle: NSLocalizedString("Cancel", comment: ""), submitButtonTitle: NSLocalizedString("Proceed", comment: ""), cancelHandler: nil) {
                
                    
                    if let nav = self.navigationController{
                        nav.dismiss(animated: true, completion: {
                            let mainStb = UIStoryboard.init(name: "Main", bundle: nil)

                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            
                            appDelegate.window!.rootViewController?.dismiss(animated: false, completion: {
                                do {
                                    try firebaseAuth.signOut()
                                    
                                    GIDSignIn.sharedInstance().signOut()
                                    
                                    if let _ = AccessToken.current {
                                        // User is logged in, use 'accessToken' here.
                                        let loginManager = LoginManager()
                                        loginManager.logOut()
                                    }
                                } catch let signOutError as NSError {
                                    print ("Error signing out: %@", signOutError)
                                }
                            })
                            
                            appDelegate.window?.rootViewController = mainStb.instantiateInitialViewController()
                            
                            UIView.transition(with: appDelegate.window!,
                                              duration: 0.5,
                                              options: .curveEaseIn,
                                              animations: nil,
                                              completion: nil)
                        })
                    }
            }
            
        }
    }
    
    func flipView(){
        
        Localizer.DoTheExchange()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController?.dismiss(animated: false, completion: nil)
        
        let mainStb = UIStoryboard.init(name: "Main", bundle: nil)
        //appDelegate.window?.rootViewController = mainStb.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController
        appDelegate.window?.rootViewController = mainStb.instantiateInitialViewController()

        UIView.transition(with: appDelegate.window!,
                          duration: 0.5,
                          options: .transitionFlipFromLeft,
                          animations: nil,
                          completion: nil)
        
        //    let window = (UIApplication.shared.delegate as? AppDelegate)?.window
        //
        //    let mainStb = UIStoryboard.init(name: "Main", bundle: nil)
        //
        //    window?.rootViewController = mainStb.instantiateViewController(withIdentifier: TabBarViewController.storyboardId)
        //
        //    UIView.transition(with: window!, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: nil, completion: nil)
        
    }
    
}
