//
//  FavouritAddressTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/16/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import DLRadioButton
import GoogleMaps
import Toaster
import DropDown
import GooglePlaces

class FavouritAddressTableViewController: UITableViewController {
    
    static let storyboardId = "FavouritAddressTableViewController"
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var mapCenterPinImage: UIImageView!
    
    private let locationManager = CLLocationManager()
    private let dataProvider = GoogleDataProvider()
    private let searchRadius: Double = 50
    
    var openMap = false
    var openFav = false
    var currentUser : User!
    var fromSetting = false
    
    var otherButtons : [DLRadioButton] = [];
    
    var fromFav = false
    var fromWork = false
    var fromHome = false
    
    var fromMap = false
    
    var favourites = [FavoriteAddress]()
    var favWork : FavoriteAddress!
    var favHome : FavoriteAddress!
    
    var dropDown : DropDown!
    var token : GMSAutocompleteSessionToken!
    var placesClient: GMSPlacesClient!
    
    var currentPosition : CLLocationCoordinate2D!
    var places = [GoogleAddressStruct]()

    
    var favouriteCoodinate : CLLocationCoordinate2D!
    
    @IBOutlet weak var favoritTableView: UITableView!
    @IBOutlet weak var favoritAddressCount: UILabel!

    @IBOutlet weak var clearTextField: UIButton!
    @IBOutlet weak var favoritRD: DLRadioButton!
    @IBOutlet weak var homeRD: DLRadioButton!
    @IBOutlet weak var workRB: DLRadioButton!
    @IBOutlet weak var homeAddressLabel: UILabel!
    @IBOutlet weak var workAddressLabel: UILabel!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var workButton: UIButton!
    @IBOutlet weak var addressTextField: UITextField!
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var anchorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        mapView.camera = GMSCameraPosition(latitude: 29.3733578, longitude: 47.9678728, zoom: 12)
        mapView.delegate = self
        mapView.settings.rotateGestures = false
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        token = GMSAutocompleteSessionToken.init()
        placesClient = GMSPlacesClient()
        backBtn.image = getBackImage()
        
        currentUser = User.fetchCurrentUserFromLocal()

        setButtons()
        setUserAddress()
        
    }
    
    func setUserAddress() {
        workAddressLabel.text = NSLocalizedString("Undefined", comment: "")
        workAddressLabel.textColor = UIColor.gray
        
        homeAddressLabel.text = NSLocalizedString("Undefined", comment: "")
        homeAddressLabel.textColor = UIColor.gray
        
        favourites.removeAll()
        
        currentUser.favoriteAddress?.forEach({ (fav) in
            if fav.type == "work" {
                workAddressLabel.text = fav.name
                workAddressLabel.textColor = UIColor.black
                self.favWork = fav
            }
            if fav.type == "home" {
                homeAddressLabel.text = fav.name
                homeAddressLabel.textColor = UIColor.black
                self.favHome = fav

            }
            if fav.type == "fav" {
                favourites.append(fav)
            }
        })
        self.favoritAddressCount.text = "\(self.favourites.count)"
        
        self.favouriteCoodinate = nil
        
        self.tableView.beginUpdates()
        self.favoritTableView.reloadData()
        self.tableView.endUpdates()
    }
    
    @IBAction func editAddress(_ sender: UIButton) {
        if sender == homeButton{
            if favHome != nil {
                self.currentUser.deleteFav(fav: favHome) { (error, ref) in
                    if let error = error {
                        self.handleError(error)
                        print(error.localizedDescription)
                        return
                    }
                    self.homeAddressLabel.text = NSLocalizedString("Undefined", comment: "")
                    self.homeAddressLabel.textColor = UIColor.gray
                }

            }

        }else if sender == workButton{
            if favWork != nil {
                self.currentUser.deleteFav(fav: favWork) { (error, ref) in
                    if let error = error {
                        self.handleError(error)
                        print(error.localizedDescription)
                        return
                    }
                    self.workAddressLabel.text = NSLocalizedString("Undefined", comment: "")
                    self.workAddressLabel.textColor = UIColor.gray
                }
            }
        }
        self.addressTextField.becomeFirstResponder()
        openMap = true
        self.tableView.reloadData()
    }
    
    fileprivate func setButtons() {
        
        otherButtons.append(homeRD)
        otherButtons.append(workRB)
        
        favoritRD.otherButtons = otherButtons
        
        if fromSetting {
            homeRD.isHidden = true
            favoritRD.isHidden = true
            workRB.isHidden = true
            homeButton.isHidden = false
            workButton.isHidden = false
        }else{
            if fromWork{
                favoritRD.otherButtons[1].isSelected = true
            }else if fromFav {
                favoritRD.isSelected = true
            }else if fromHome{
                favoritRD.otherButtons[0].isSelected = true
            }
            homeButton.isHidden = true
            workButton.isHidden = true
            homeRD.isHidden = false
            favoritRD.isHidden = false
            workRB.isHidden = false
        }
    }
    
    @IBAction func clearText(_ sender: UIButton) {
        self.addressTextField.text = ""
        self.clearTextField.isHidden = true
        if dropDown != nil {
            self.dropDown.hide()
        }
    }
    
    func fetchPlaceAddress(coordinate: CLLocationCoordinate2D , completionHandler : @escaping (GooglePlace) -> Void) {
        mapView.clear()
        let requiredPlace = GooglePlace()
        
        self.reverseGeocodeCoordinate(coordinate, completionHandler: {response, error in
            
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            requiredPlace.name = address.thoroughfare ?? ""
            requiredPlace.address = lines.joined(separator: "\n")
            requiredPlace.coordinate = address.coordinate
            completionHandler(requiredPlace)
        })
//        dataProvider.fetchPlacesNearCoordinate(coordinate, radius: searchRadius ) { places in
//
//            if places.count > 0 {
//                requiredPlace = places.first!
//                completionHandler(requiredPlace)
//            }else{
//                self.reverseGeocodeCoordinate(coordinate, completionHandler: {response, error in
//
//                    guard let address = response?.firstResult(), let lines = address.lines else {
//                        return
//                    }
//                    requiredPlace.address = lines.joined(separator: "\n")
//                    requiredPlace.coordinate = address.coordinate
//                    completionHandler(requiredPlace)
//                })
//            }
//        }
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D , completionHandler : @escaping GMSReverseGeocodeCallback) {
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate, completionHandler: completionHandler)
    }
    
    @IBAction func chooseManually(_ sender: Any) {
        openMap = !openMap
        self.tableView.reloadData()
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        if (addressTextField.text?.count)! > 0 , favouriteCoodinate != nil{

            let newFav = FavoriteAddress(name: addressTextField.text ?? "", lat: favouriteCoodinate.latitude, lng: favouriteCoodinate.longitude, type: "fav")

            if !fromSetting{
                if homeRD.isSelected {
                    print("home selected")
                    newFav.type = "home"
                }
                else if favoritRD.isSelected {
                    print("favoritRD selected")
                    newFav.type = "fav"
                }
                else if workRB.isSelected {
                    print("workRB selected")
                    newFav.type = "work"
                }
                self.currentUser.addToUserFavourites(fav: newFav)
                
                self.setUserAddress()
                
                self.navigationController?.dismiss(animated: true, completion: nil)

            }else{
                let alertController = UIAlertController.init(title: NSLocalizedString("Manage Favorites", comment: ""), message: NSLocalizedString("Which address you want to set", comment: ""), preferredStyle: .actionSheet)
                
                let firstChoice = UIAlertAction.init(title: NSLocalizedString("Work", comment: ""), style: .default) { (alert) in
                    if self.favWork == nil {
                        newFav.type = "work"
                        self.currentUser.addToUserFavourites(fav: newFav)
                        
                        self.setUserAddress()
                    }else {
                        self.showAlertView(isOneButton: false, title: NSLocalizedString("Would you like to delete the registred address", comment: ""), cancelButtonTitle: NSLocalizedString("Cancel", comment: ""), submitButtonTitle: NSLocalizedString("Yes , I do", comment: ""), cancelHandler: nil, doneHandler: {
                            newFav.type = "work"
                            self.currentUser.addToUserFavourites(fav: newFav)
                            
                            self.setUserAddress()
                        })
                    }
                }
                
                alertController.addAction(firstChoice)
                
                let secondChoice = UIAlertAction.init(title: NSLocalizedString("Home", comment: ""), style: .default) { (alert) in
                    if self.favHome == nil {
                        newFav.type = "home"
                        self.currentUser.addToUserFavourites(fav: newFav)
                        
                        self.setUserAddress()
                        self.tableView.reloadData()
                    }else {
                        self.showAlertView(isOneButton: false, title: NSLocalizedString("Would you like to delete the registred address", comment: ""), cancelButtonTitle: NSLocalizedString("Cancel", comment: ""), submitButtonTitle: NSLocalizedString("Yes , I do", comment: ""), cancelHandler: nil, doneHandler: {
                            newFav.type = "home"
                            self.currentUser.addToUserFavourites(fav: newFav)
                            
                            self.setUserAddress()
                            self.tableView.reloadData()
                        })
                    }

                }
                alertController.addAction(secondChoice)
                
                let thirdChoice = UIAlertAction.init(title: NSLocalizedString("Favorite", comment: ""), style: .default) { (alert) in
                    newFav.type = "fav"
                    self.currentUser.addToUserFavourites(fav: newFav)
                    
                    self.setUserAddress()

                }
                alertController.addAction(thirdChoice)
                
                let cancelChoice = UIAlertAction.init(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
                alertController.addAction(cancelChoice)
                
                if let popoverController = alertController.popoverPresentationController {
                    
                    popoverController.barButtonItem = sender
                    
                }
                
                self.present(alertController, animated: true, completion: nil)
            }
            self.navigationController?.popViewController(animated: true)
        }else {
            Toast(text: NSLocalizedString("Please enter a valid address", comment: ""), duration: Delay.long).show()
        }
    }
    
    @IBAction func search(_ sender: UITextField) {
        // The view to which the drop down will appear on
        
        self.dropDown.customCellConfiguration = { (index: Indexd, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? DropDownViewCell else { return }
            // Setup your custom UI components
            cell.optionLabel.text = self.places[index].primaryText
            cell.result.text = self.places[index].secondaryText
        }
        
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            sender.text = "\(self.places[index].primaryText) ,\(self.places[index].secondaryText)"
            
            self.placesClient.lookUpPlaceID(self.places[index].placeID, callback: { (place, error) in
                
                if let error = error {
                    print("An error occurred: \(error.localizedDescription)")
                    return
                }
                if let place = place {
                    // Creates a marker in the center of the map.
                    
                    self.favouriteCoodinate = place.coordinate
                    
                    let gplace = GooglePlace(place: place)
                    
                    let addressStr = gplace.name == "" ? "\(gplace.address)"  : "\(gplace.name), \(gplace.address)"
                    
                    self.addressTextField.text = addressStr
                    
                    let newFav = FavoriteAddress(name: addressStr, lat: gplace.coordinate.latitude, lng: gplace.coordinate.longitude, type: "rec")
                    
                    self.currentUser.addRecentAddressSearch(fav: newFav)
                    
                    self.fromMap = false
                    self.mapView.camera = GMSCameraPosition(target: place.coordinate, zoom: 15)
                    //self.mapView.animate(toLocation: place.coordinate)
                }
            })
        }
        
        if (sender.text?.count)! > 0{
            
            self.clearTextField.isHidden = false
            self.locationManager.stopUpdatingLocation()
            
            self.dropDown.hide()
            
            let filter = GMSAutocompleteFilter()
            //filter.type = .establishment
            filter.country = "KW"

            let bounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2D(latitude: 30.125665, longitude: 46.578025), coordinate: CLLocationCoordinate2D(latitude: 28.540784, longitude: 48.741183))
            
            placesClient?.findAutocompletePredictions(fromQuery: sender.text!,
                                                      bounds: bounds,
                                                      boundsMode: GMSAutocompleteBoundsMode.bias,
                                                      filter: filter,
                                                      sessionToken: token,
                                                      callback: { (results, error) in
                                                        if let error = error {
                                                            print("Autocomplete error: \(error)")
                                                            return
                                                        }
                                                        // The list of items to display. Can be changed dynamically
                                                        
                                                        if let results = results {
                                                            //self.placesDetails = [String]()
                                                            self.places = [GoogleAddressStruct]()
                                                            //self.placeIDs = [String]()
                                                            for result in results {
                                                                let place = GoogleAddressStruct(primaryText: result.attributedPrimaryText.string, secondaryText: result.attributedSecondaryText?.string ?? " ", placeID: result.placeID)
                                                                self.places.append(place)
                                                            }
                                                            self.dropDown.dataSource = self.places.compactMap({ (place) -> String? in
                                                                return place.primaryText
                                                            })
                                                            self.dropDown.reloadAllComponents()
                                                            self.dropDown.show()
                                                        }
            })
        }else{
            self.clearTextField.isHidden = true
            self.dropDown.hide()
        }
    }
    
    @IBAction func back(_ sender: Any) {
        if fromSetting{
            self.navigationController?.popViewController(animated: true)
        }else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if tableView == self.tableView {
            return 3
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if tableView == self.tableView {
            switch section {
            case 0:
                return openMap ? 2 : 1
            case 1:
                return 2
            default:
                return 2//self.favourites.count == 0 ? 1 : 2
            }
        }
        return self.favourites.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tableView {
            
            if indexPath.section == 0 {
                
                if indexPath.row == 0 {
                    
                    return 60
                }
                if openMap {
                    
                    return 300
                }
                return 0
                
            }else if indexPath.section == 1 {
                
                return 60
                
            }else if indexPath.section == 2{
    
                if indexPath.row == 1 {
                    
                    return CGFloat(self.favourites.count * 50)
                }
                return 60
        
            }
            return 0
        }
        return 50
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == favoritTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "favCell", for: indexPath) as? FavouritAddressTableViewCell
            if let cell = cell{
                if fromSetting {
                    cell.deleteButton.isHidden = false
                }else{
                    cell.deleteButton.isHidden = true
                }
                cell.primaryAddressLabel.text = favourites[indexPath.row].name
                cell.deleteButton.tag = indexPath.row
                cell.controller = self
                return cell
            }

            return UITableViewCell()
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 20))
        // Add a bottomBorder
        headerView.backgroundColor = UIColor.groupTableViewBackground
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return section == 0 ? 0 : 30
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        return 0
    }

}
// MARK: - CLLocationManagerDelegate
extension FavouritAddressTableViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        self.currentPosition = location.coordinate
        
        //mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)

    }
}


// MARK: - GMSMapViewDelegate
extension FavouritAddressTableViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if fromMap{
            let coo = mapView.projection.coordinate(for: mapView.center)
            fetchPlaceAddress(coordinate: coo) { (place) in
                self.addressTextField.text = place.name == "" ? "\(place.address)"  : "\(place.name), \(place.address)"
                self.favouriteCoodinate = coo
                self.clearTextField.isHidden = false
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture) {
            fromMap = true
        }
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
       
        self.locationManager.startUpdatingLocation()

        if self.currentPosition == nil {
            currentPosition = mapView.camera.target
        }
        mapView.camera = GMSCameraPosition(target: self.currentPosition, zoom: 15)
        
        fetchPlaceAddress(coordinate: self.currentPosition) { (place) in
            self.addressTextField.text = place.name == "" ? "\(place.address)"  : "\(place.name), \(place.address)"
            self.favouriteCoodinate = self.currentPosition
            self.clearTextField.isHidden = false
        }
        return true
    }
}

// MARK: - UITextFieldDelegate

extension FavouritAddressTableViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.dropDown = DropDown()
        
        self.dropDown.anchorView = anchorView
        
        self.dropDown.direction = .bottom
        self.dropDown.width = self.view.frame.width
        self.dropDown.cellHeight = 50
        self.dropDown.dataSource = []
        self.dropDown.cellNib = UINib(nibName: "DropDownViewCell", bundle: nil)
    }
    
}
