//
//  ProfileTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import SDWebImage
import MobileCoreServices
import FirebaseStorage

class ProfileTableViewController: UITableViewController {

    static let storyboardId = "ProfileTableViewController"

    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var isEmailVerified: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var isPhoneNumberVerified: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var secondName: UILabel!
    @IBOutlet weak var firstName: UILabel!
    
    @IBOutlet weak var walletBalance: UILabel!
    
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var editPhoto: UIImageView!
    var currentUser : User!
    
    var imagePickerController: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        backBtn.image = getBackImage()
        
        profilePhoto.borderWithCornder(raduis: self.profilePhoto.frame.height / 2, borderColor: UIColor.white.cgColor, borderWidth: 2)
        
        editPhoto.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.editUserPicture)))
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        currentUser = User.fetchCurrentUserFromLocal()
        
        firstName.text = currentUser.firstName
        secondName.text = currentUser.lastName
        phoneNumber.text = currentUser.phoneNumber // Auth.auth().currentUser?.phoneNumber
        email.text = Auth.auth().currentUser?.email
        
        walletBalance.text = "\(currentUser.promoWallet ?? 0)"
        
        if let picURL = currentUser.pictureURL{
            
            if picURL.isValidURL(){
                profilePhoto.sd_setImage(with: URL(string: picURL)) { (image, error, cache, url) in
                    if error == nil {
                        self.profilePhoto.image = image
                    }
                }
            }else if picURL.count > 0{
                
                let storageRef = Storage.storage().reference()
                let profileImagesRef = storageRef.child("ProfilePictures/\(picURL)")
                
                // You can also access to download URL after upload.
                profileImagesRef.downloadURL { (url, error) in
                    guard let downloadURL = url else {
                        // Uh-oh, an error occurred!
                        return
                    }
                    self.profilePhoto.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                        if error == nil {
                            self.profilePhoto.image = image
                        }
                    })
                    
                }
            }
        }

        
        if Auth.auth().currentUser?.phoneNumber != nil {
            isPhoneNumberVerified.text = NSLocalizedString("Verified", comment: "")
            isPhoneNumberVerified.textColor = UIColor.green
        }else {
            isPhoneNumberVerified.isHidden = true
        }

        
        if Auth.auth().currentUser!.isEmailVerified{
            isEmailVerified.text = NSLocalizedString("Verified", comment: "")
            isEmailVerified.textColor = UIColor.green
        }else{
            isEmailVerified.text = NSLocalizedString("Not verified", comment: "")
            isEmailVerified.textColor = UIColor.orange
        }
        
    }

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func editUserPicture ( ){
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("TakePhoto", comment:"Take Photo"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("FromLibrary", comment:"From Library"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = .photoLibrary
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.view.tintColor = UIColor.gray
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment:"Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
        
        if let popoverController = alert.popoverPresentationController {
            
            popoverController.sourceView = self.editPhoto
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
            
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 8
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        }
        
        if indexPath.row == 6 {
            
            if let providerData = Auth.auth().currentUser?.providerData {
                for userInfo in providerData {
                    switch userInfo.providerID {
                    case "facebook.com":
                        print("Facebook Login")
                        return 0

                    case "google.com":
                        print("Facebook Login")
                        return 0

                    case "phone":
                        break
                    default:
                        break
                    }
                }
            }
        }
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        switch indexPath.row {
        case 3:
            if let nvc = self.navigationController , Auth.auth().currentUser?.phoneNumber != nil {
                nvc.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UpdatePhoneNumberViewController.storyboardId) as! UpdatePhoneNumberViewController, animated: true)
            }
//
//        case 4 :
//            if let nvc = self.navigationController {
//                nvc.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UpdateEmailViewController.storyboardId) as! UpdateEmailViewController, animated: true)
//            }
            
        case 6 :
            if let nvc = self.navigationController {
                nvc.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UpdatePasswordViewController.storyboardId) as! UpdatePasswordViewController, animated: true)
            }
            
        case 7 :
            if let nvc = self.navigationController {
                
                let favVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: FavouritAddressTableViewController.storyboardId) as! FavouritAddressTableViewController
                favVC.fromSetting = true
                favVC.currentUser = self.currentUser
                favVC.fromWork = false
                favVC.fromHome = false
                favVC.fromFav = false
                nvc.pushViewController(favVC, animated: true)

            }
            
        default:
            tableView.deselectRow(at: indexPath, animated: true)
            
        }
    }
}


extension ProfileTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let mediaType = (info[UIImagePickerControllerMediaType] as! String)
        
        if mediaType == "public.image" {
            
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.showSpinner(onView: profilePhoto)
                self.profilePhoto.image = image
                var data = Data()
                data = UIImageJPEGRepresentation(image, 0.8)!
                let metaData = StorageMetadata()
                metaData.contentType = "image/jpg"
                
                let name = "\(Date.timeIntervalSinceReferenceDate)"
                
                let storageRef = Storage.storage().reference()
                let profileImagesRef = storageRef.child("ProfilePictures/\(name)")
                
                profileImagesRef.putData(data, metadata: metaData) { (metaData, error) in
                    if let error = error {
                        self.removeSpinner()
                        print(error.localizedDescription)
                        self.handleError(error)
                        return
                    }else{
                        // You can also access to download URL after upload.
                        profileImagesRef.downloadURL { (url, error) in
                            guard let downloadURL = url else {
                                // Uh-oh, an error occurred!
                                self.handleError(error!)
                                self.removeSpinner()

                                return
                            }
                            let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                            changeRequest?.photoURL = downloadURL
                            
                            changeRequest?.commitChanges { (error) in
                                if error != nil {
                                    self.removeSpinner()
                                    print("error update user picture url")
                                    self.handleError(error!)
                                    return
                                }
                                self.currentUser.updateUserPhoto(photoURL: name)
                                self.removeSpinner()

                            }
                        }
                    }}

            }
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}
