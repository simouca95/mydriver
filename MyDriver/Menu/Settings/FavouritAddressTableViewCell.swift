//
//  FavouritAddressTableViewCell.swift
//  MyDriver
//
//  Created by sami hazel on 4/17/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class FavouritAddressTableViewCell: UITableViewCell{

    


    var fromSetting = false
    var controller  : FavouritAddressTableViewController!
    @IBOutlet weak var primaryAddressLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!

    @IBAction func deleteFav(_ sender: UIButton) {

        self.controller.currentUser.deleteFav(fav: self.controller.favourites[sender.tag], completionBlock: {
            (error , ref) in
            if error == nil {
                self.controller.favourites.remove(at: sender.tag)
                self.controller.favoritAddressCount.text = "\(self.controller.favourites.count)"
                
                self.controller.tableView.beginUpdates()
                self.controller.favoritTableView.reloadData()
                self.controller.tableView.endUpdates()

            }
        })
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
