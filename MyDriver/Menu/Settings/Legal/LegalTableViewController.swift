//
//  LegalTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 6/9/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class LegalTableViewController: UITableViewController {
    
    static let storyboardId = "LegalTableViewController"

    @IBOutlet weak var versionString: UILabel!
    @IBOutlet weak var buckBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        buckBtn.image = getBackImage()
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String  , let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String{
            versionString.text = "Version " + version + "." + build
            
        }
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return self.view.frame.height - 120 - (self.navigationController?.navigationBar.frame.height ?? 0) - UIApplication.shared.statusBarFrame.size.height
        }
        return 60
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let webViewVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: LegalWebViewViewController.storyboardId) as! LegalWebViewViewController
        
        let privacy = NSLocalizedString("Privacy policy", comment: "")
        let use = NSLocalizedString("Terms & Conditions", comment: "")
        
        var termsURL = ""
        var privacyURL = ""
        
        if Languages.currentLanguage().starts(with: "ar"){
            termsURL = "https://mydriverkw.com/ar/my_driver_conditions_legals/"
            privacyURL = "https://mydriverkw.com/ar/my_driver_privacy_policy/"
        }else {
            termsURL = "https://mydriverkw.com/en/my_driver_conditions_legals/"
            privacyURL = "https://mydriverkw.com/en/my_driver_privacy_policy/"

        }

        
        switch indexPath.row {
        case 0:
            
            webViewVC.titleVc = use
            
            if termsURL.isValidURL(){
                webViewVC.content = termsURL
            }
            navigationController?.pushViewController(webViewVC, animated: true)
            break;
            
        case 1:
            
            webViewVC.titleVc = privacy
            
            if privacyURL.isValidURL(){
                webViewVC.content = privacyURL
            }
            navigationController?.pushViewController(webViewVC, animated: true)
            break;
            
        default:
            return;
        }
    }
   
}
