//
//  LegalWebViewViewController.swift
//  MyDriver
//
//  Created by sami hazel on 6/9/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class LegalWebViewViewController: UIViewController , UIWebViewDelegate{

    static let storyboardId = "LegalWebViewViewController"
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    
    var content = ""
    
    var titleVc = ""
    
    var fromRegistartion = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backBtn.image = getBackImage()
        self.showSpinner(onView: self.view)
        
        if let url = URL(string: content){
            let requestObj = URLRequest(url: url)
            webView.loadRequest(requestObj)
        }

        
       // self.webView.loadHTMLString(content, baseURL: Bundle.main.bundleURL)
        
        self.webView.delegate = self
        
        self.title = titleVc
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        if fromRegistartion{
            self.dismiss(animated: true, completion: nil)
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.removeSpinner()
    }
}
