//
//  ContactUsViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import MobileCoreServices
import FirebaseStorage
import NotificationBannerSwift
import DropDown

class ContactUsViewController: UIViewController {

    static let storyboardId = "ContactUsViewController"

    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var jointFile: UIView!
    @IBOutlet weak var message: UITextView!
    
    @IBOutlet weak var send: LoadingButton!
    @IBOutlet weak var fileNameLabel: UILabel!
    var imagePickerController: UIImagePickerController!
    
    var uploadPhoto = false
    var pictureURL : String!
    
    var reachability = Reachability()!
    
    var banner : StatusBarNotificationBanner!
    
    let subjects = [NSLocalizedString("specialService", comment: ""),
                    NSLocalizedString("lostThings", comment: ""),
                    NSLocalizedString("report", comment: ""),
                    NSLocalizedString("feedback", comment: "")]
    
    let dropDown = DropDown()
    var didSelectSubject = false
    
    @IBOutlet weak var buckBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buckBtn.image = getBackImage()

        // Do any additional setup after loading the view.
        message.borderWithCornder(raduis: 4, borderColor: UIColor.black.cgColor, borderWidth: 1)
        jointFile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openPicker)))
        checkConnection()
        
        
        // The view to which the drop down will appear on
        dropDown.anchorView = self.subjectLabel // UIView or UIBarButtonItem
        dropDown.direction = .bottom
        dropDown.cellHeight = 40
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = subjects
        
        dropDown.width = 300
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.didSelectSubject = true
            self.subjectLabel.text = self.subjects[index]
            self.dropDown.hide()
            
        }
        dropDown.cellConfiguration = { [unowned self] (index, item) in
            return self.subjects[index]
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func openPicker(){
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("TakePhoto", comment:"Take Photo"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("FromLibrary", comment:"From Library"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = .photoLibrary
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.view.tintColor = UIColor.gray
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment:"Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
        
        if let popoverController = alert.popoverPresentationController {
            
            popoverController.sourceView = self.jointFile
            
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func openComboBox(_ sender: Any) {
        self.dropDown.show()
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func send(_ sender: Any) {
        var message : Message!
        
        if !subjectLabel.text!.isBlank , didSelectSubject{
            
            if !self.message.text.isBlank{
                send.loading = true
                if pictureURL == nil {
                    message = Message(subject: subjectLabel.text!, typeSender: "client", message: self.message.text!)
                }else {
                    message = Message(subject: subjectLabel.text!, typeSender: "client", message: self.message.text!, picture: pictureURL)
                }
                
                message.createMessage()
                self.pictureURL = nil 
                let banner = NotificationBanner(title: NSLocalizedString("Message sent successfully", comment: ""), subtitle: "", style: .success)
                banner.show()
                send.loading = false
                self.dismiss(animated: true, completion: nil)
            }else {
                
                self.message.shake()
            }
        }else{
            subjectLabel.shake()
        }

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.cancelTransparentNavigationBar()
        
        self.appEnteredToForeground()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.appEnteredToBackground()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ContactUsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let mediaType = (info[UIImagePickerControllerMediaType] as! String)
        
        if mediaType == "public.image" {
            
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                var data = Data()
                data = UIImageJPEGRepresentation(image, 0.8)!
                let metaData = StorageMetadata()
                metaData.contentType = "image/jpg"
                let name = "\(Date.timeIntervalSinceReferenceDate)"
                let storageRef = Storage.storage().reference()
                let profileImagesRef = storageRef.child("ContactUsPictures/\(name)")
                
                profileImagesRef.putData(data, metadata: metaData) { (metaData, error) in
                    if let error = error {
                        print(error.localizedDescription)
                        self.handleError(error)
                        self.fileNameLabel.text = NSLocalizedString("Photo failed to upload", comment: "")
                        return
                    }else{
                        self.fileNameLabel.text = NSLocalizedString("Photo successfully uploaded", comment: "")
                        self.pictureURL = name
                    }}
                
            }
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension ContactUsViewController{
    // Reachability
    
    func appEnteredToForeground(){
        
        print("discussion appEnteredToForeground")
        
        
        

        reachability.whenUnreachable = { reachability in
            if self.banner != nil {
                self.banner.dismiss()
            }
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Please verify your internet connexion", comment: ""), style: .danger)
            self.banner.autoDismiss = false
            self.banner.show()
        }
        reachability.whenReachable = { reachability in
            if self.banner != nil {
                self.banner.dismiss()
            }
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Connected", comment: ""), style: .success)
            self.banner.autoDismiss = true
            self.banner.show()
        }
        
    }
    
    func appEnteredToBackground(){
        
        print("discussion appEnteredToBackground")
        
        reachability.stopNotifier()
        
    }
    
    func checkConnection(){
        reachability = Reachability()!
        
        if !reachability.isReachable{
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Please verify your internet connexion", comment: ""), style: .danger)
            self.banner.autoDismiss = false
            self.banner.show()
            
            self.send.isEnabled = false
            self.send.backgroundColor = UIColor.gray
            
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
    }
}
