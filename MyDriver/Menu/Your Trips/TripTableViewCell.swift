//
//  TripTableViewCell.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage
import FirebaseStorage

class TripTableViewCell: UITableViewCell {

    static let storyboardId = "tripCell"

    @IBOutlet weak var raceRate: CosmosView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var driverAvatar: UIImageView!
    @IBOutlet weak var destinationAdress: UILabel!
    @IBOutlet weak var pickupAdress: UILabel!
    @IBOutlet weak var raceCost: UILabel!
    @IBOutlet weak var raceAvatar: UIImageView!
    @IBOutlet weak var tripDate: UILabel!
    @IBOutlet weak var carDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        raceRate.settings.updateOnTouch = false
        
        driverAvatar.layer.cornerRadius = self.driverAvatar.frame.height / 2
        driverAvatar.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        driverAvatar.isHidden = false
    }
    
    func initCell(_ course : Course)  {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm a" //Your date format
        
        if course.driver == nil{
            
            driverName.text = "Canceled"
            driverAvatar.isHidden = true
            carDetails.isHidden = true
            raceRate.isHidden = true
           
        }else {
            driverName.text = course.driver!.fullName
            driverAvatar.isHidden = false
            carDetails.isHidden = false
            raceRate.isHidden = false
            
            if course.driver!.vehicle != nil{
                carDetails.text = "\(course.driver!.vehicle!.brand ?? "") \(course.driver!.vehicle!.registrationNumber ?? "") \(course.driver!.vehicle!.color ?? "")"
            }
            if let pic = course.driver!.pictureURL{
                
                self.driverAvatar.image = UIImage(named: "profileRegestration")
                
                if pic.count > 0{
                    let storageRef = Storage.storage().reference()
                    let profileImagesRef = storageRef.child("ProfilePictures/\(pic)")
                    
                    // You can also access to download URL after upload.
                    profileImagesRef.downloadURL { (url, error) in
                        guard let downloadURL = url else {
                            // Uh-oh, an error occurred!
                            return
                        }
                        self.driverAvatar.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                            if error == nil {
                                self.driverAvatar.image = image
                            }
                        })
                        
                    }
                }
            }
            
            destinationAdress.text = course.destination.address
            pickupAdress.text = course.pickUpPoint.address
            raceCost.text = "\(course.finalCost ?? 0) KWD"
            
            tripDate.text = dateFormatter.string(from: course.createdAt)
            raceRate.rating = Double(course.rating!)
            
            switch course.classCar{
            case "eco" :
                raceAvatar.image = #imageLiteral(resourceName: "voiture1")
            case "lux":
                raceAvatar.image = #imageLiteral(resourceName: "voiture")
            case "van":
                raceAvatar.image = #imageLiteral(resourceName: "voiture2")
                
            default :
                raceAvatar.image =  #imageLiteral(resourceName: "car")
                
            }

        }

        
        
        
    }

}
