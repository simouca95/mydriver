//
//  TripsViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseAuth
import NotificationBannerSwift

class TripsViewController: UIViewController {
    
    static let storyboardId = "TripsViewController"
    
    @IBOutlet weak var segmentedController: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var datasource = [Course]()

    var tripsDataSource = [Course]()
    
    var ecoTripsDataSource = [Course]()
    var luxTripsDataSource = [Course]()
    var vanTripsDataSource = [Course]()
    var accessTripsDataSource = [Course]()

    var reachability = Reachability()!
    var loadingView : LoadingView!

    var banner : StatusBarNotificationBanner!
    @IBOutlet weak var backBtn: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.checkConnection()
        backBtn.image = getBackImage()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func indexChanged(_ sender: Any) {
        
        switch segmentedController.selectedSegmentIndex {
        case 0:
            datasource = tripsDataSource
        case 1:
            datasource = ecoTripsDataSource
        case 2:
            datasource = luxTripsDataSource
        case 3:
            datasource = vanTripsDataSource
        case 4:
            datasource = accessTripsDataSource
        default:
            break;
        }
        
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.cancelTransparentNavigationBar()
        self.appEnteredToForeground()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.appEnteredToBackground()
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TripsViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TripTableViewCell.storyboardId) as! TripTableViewCell
        cell.initCell(datasource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
}

extension TripsViewController {
    // Reachability
    
    func appEnteredToForeground(){
        
        print("discussion appEnteredToForeground")
        
        reachability.whenUnreachable = { reachability in
            if self.banner != nil {
                self.banner.dismiss()
            }
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Please verify your internet connexion", comment: ""), style: .danger)
            self.banner.autoDismiss = false
            self.banner.show()
        }
        reachability.whenReachable = { reachability in
            if self.banner != nil {
                self.banner.dismiss()
            }
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Connected", comment: ""), style: .success)
            self.banner.autoDismiss = true
            self.banner.show()
        }
        
    }
    
    func appEnteredToBackground(){
        
        print("discussion appEnteredToBackground")
        if self.banner != nil {
            self.banner.dismiss()
        }
        reachability.stopNotifier()
        
    }
    
    func checkConnection(){
        reachability = Reachability()!
        if !reachability.isReachable{
            self.banner = StatusBarNotificationBanner(title: NSLocalizedString("Please verify your internet connexion", comment: ""), style: .danger)
            self.banner.autoDismiss = false
            self.banner.show()
        }else {
            fetchMyTrips()
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
    }
    
    func showNoDataMessage() {
        self.loadingView = LoadingView.loadFromNibNamed(nibNamed: "LoadingView", bundle: nil)
        
        self.loadingView.tryAgainButton.isHidden = false
        
        self.loadingView.tryAgainButton.isEnabled = true
        
        self.loadingView.tryAgainButton.setTitle( NSLocalizedString("Please retry", comment: ""), for: .normal)
        
        self.loadingView.tryAgainButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tryAgain)))
        
        self.loadingView.loadingLabel.text = NSLocalizedString("No trips found", comment: "")
        
        self.loadingView.activityIndicator.isHidden = true
        
        self.loadingView.frame = self.tableView.bounds
        
        self.view.addSubview(self.loadingView)

    }
    
    func fetchMyTrips() {
        
        Course.getMyCourses(clientID: Auth.auth().currentUser!.uid) { (response) in
            self.showSpinner(onView: self.view)
            switch response.result {
            case .failure(_):
                print(response.description)
                self.removeSpinner()
                break;
                
            case .success(let value):
                
                let json = JSON(value)
                
                print(response.description)

                if  json.isEmpty {
                    self.removeSpinner()
                    print("0 trips")
                    break
                }
                self.tripsDataSource = [Course]()
                
                for item in json.arrayValue{
                    var car : Vehicle!
                    var driver : Driver!
                    
                    if !item["carId"].stringValue.isEmpty , item["car"] != JSON.null{
                        car = Vehicle(fromJson: item["car"])
                        car.uid = item["carId"].stringValue
                    }

                    if !item["driverId"].stringValue.isEmpty , item["driver"] != JSON.null{
                        driver = Driver(fromJson: item)
                        driver.uid = item["driverId"].stringValue
                        driver.vehicle = car
                    }

                    let course = Course(fromJson: item["course"])
                    course.uid = item["courseId"].stringValue
                    course.driver = driver
                    
                    self.tripsDataSource.append(course)
                }
                
                self.tripsDataSource.forEach({ (course) in
                    if course.classCar == "eco"{
                        
                        self.ecoTripsDataSource.append(course)
                    }
                    if course.classCar == "lux"{
                        
                        self.luxTripsDataSource.append(course)
                    }
                    if course.classCar == "van"{
                        
                        self.vanTripsDataSource.append(course)
                    }
                    if course.classCar == "access"{
                        
                        self.accessTripsDataSource.append(course)
                    }
                })
                self.datasource = self.tripsDataSource
                self.removeSpinner()
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func tryAgain() {
        
        fetchMyTrips()
    }
}
