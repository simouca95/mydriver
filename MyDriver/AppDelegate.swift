//
//  AppDelegate.swift
//  MyDriver
//
//  Created by sami hazel on 4/4/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import DropDown
import Firebase
import FirebaseMessaging
import UserNotifications
import SwiftyJSON
import NotificationBannerSwift
import CoreData
import AVFoundation

import GoogleSignIn
import FBSDKCoreKit
import FacebookCore

let googleApiKey = "AIzaSyDn3vWzI2h6bDRNZ9rCKqmvw3MHE9X3LZs"
let matrixApiKey = "AIzaSyDSj8fDdsr2ff4vlhFupag0azI3aVazfzw"
let placesApiKey = "AIzaSyD8xsxwhsxEr_VNiF5Oaz5UmufyObfirnw"
let directionsApiKey = "AIzaSyCm2kW7TPsr8F4gWWKy7blOIteUkvDOmHU"


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(placesApiKey)
        
        DropDown.startListeningToKeyboard()

        FirebaseApp.configure()
        //Database.database().isPersistenceEnabled = true
        Messaging.messaging().delegate = self
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID

        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        loadPrices()

//        let firebaseAuth = Auth.auth()
//        do {
//            try firebaseAuth.signOut()
//
//        } catch let signOutError as NSError {
//            print ("Error signing out: %@", signOutError)
//        }
//        if Auth.auth().currentUser != nil {
//            if launchOptions != nil{
//                if let userInfo = launchOptions![UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any]{
//                    if self.getNotification(from: userInfo) == 20{
//                        let data = JSON.init(parseJSON: JSON(userInfo["data"]!).rawString()!)
//                        self.showDriverInfos(json: data)
//                    }
//                }
//            }
//        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {granted, error in
                    print("Permission granted: \(granted)") // 3
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
            center.removeAllDeliveredNotifications() // To remove all delivered notifications
        }


        return true
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
        -> Bool {
            ApplicationDelegate.shared.application(application, open: url, options: options)
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: [:])
    }
    
    // MARK: - Load Config
    func loadPrices(){
        let ref = Database.database(url: CURRENT_DB_REF).reference().child("config")
        ref.keepSynced(true)
        ref.observe(DataEventType.value) { (snap) in
            self.saveConfigsToLocal(snap: snap)
        }
    }
    
    
    //save config to local
    func saveConfigsToLocal(snap : DataSnapshot) {
        //save to local
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        

        
        ///delete saved prices
        let fetchPricesReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Prices")
        do {
            let test = try context.fetch(fetchPricesReq)
            if test.count > 0{
                for objectTodelete in test as! [NSManagedObject]{
                    context.delete(objectTodelete)
                }
                do {
                    try context.save()
                    print("delete saved prices")

                } catch  {
                    print(error)
                }
            }
        } catch  {
            print(error)
        }

        //save new prices
        let entityPrices = NSEntityDescription.entity(forEntityName: "Prices", in: context)

        if let _ =  snap.childSnapshot(forPath: "prices").value{
            
            for item in snap.childSnapshot(forPath: "prices").children.allObjects as! [DataSnapshot]{
                let prices = NSManagedObject(entity: entityPrices!, insertInto: context)
                prices.setValue(item.key, forKey: "category")

                prices.setValue((item.value as? NSDictionary)?.value(forKey: "pricePerKm"), forKey: "pricePerKm")
                prices.setValue((item.value as? NSDictionary)?.value(forKey: "pricePerMin"), forKey: "pricePerMin")
                prices.setValue((item.value as? NSDictionary)?.value(forKey: "baseFare"), forKey: "baseFare")
                prices.setValue((item.value as? NSDictionary)?.value(forKey: "waitingCost"), forKey: "waitingCost")
                prices.setValue((item.value as? NSDictionary)?.value(forKey: "priceTenMin"), forKey: "priceTenMin")
                do {
                    try context.save()
                    print("save new prices " , item.key)

                } catch {
                    print("Failed saving")
                }
            }
        }
        
        // delete saved configs
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Config")
        do {
            let test = try context.fetch(fetchReq)
            if test.count > 0{
                let objectTodelete = test[0] as! NSManagedObject
                context.delete(objectTodelete)
                print("delete saved configs")
                do {
                    try context.save()
                } catch  {
                    print(error)
                }
            }
        } catch  {
            print(error)
        }
        
        //save new configs

        let entityConfig = NSEntityDescription.entity(forEntityName: "Config", in: context)
        let config = NSManagedObject(entity: entityConfig!, insertInto: context)
        
        if let trafficModelSnap = snap.childSnapshot(forPath: "trafficModel").value{
            config.setValue(trafficModelSnap,forKey: "trafficModel")
        }
        
        if let gift = snap.childSnapshot(forPath: "welcomeGift").value{
            config.setValue(gift, forKey: "welcomeGift")
        }
        
//        if let versionSnap = snap.childSnapshot(forPath: "ios").value as? DataSnapshot{
//            if let version = versionSnap.childSnapshot(forPath: "latestVersion").value{
//                config.setValue(version, forKey: "version")
//            }
//        }
        
        do {
            try context.save()
            print("save new configs")

        } catch {
            print("Failed saving")
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        print("applicationWillResignActive")

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("applicationDidEnterBackground")

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground")

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationDidBecomeActive")

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("applicationWillTerminate")

    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs token retrieved: \(deviceTokenString)")
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    // [END ios_10_message_handling]

    func showDriverInfos(json : JSON){
        if let top = UIApplication.topViewController() {
            
            if top.className == MainViewController.className{
                NotificationCenter.default.post(name: Notification.Name("race_accepted"), object: json)
                
            }else{
                let banner = NotificationBanner(title: NSLocalizedString("Trip accepted", comment: ""), subtitle: NSLocalizedString("Driver is on the way", comment: ""), style: .info)
                banner.show()
            }
        }else{
            //showToast(NO_INTERNET_ERROR_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular!)
        }
    }
    
    func getNotification(from userInfo: [AnyHashable : Any]) -> Int {
        print("userInfo: ", userInfo)
        
        var alert = ""
        
        var type = 0
        
        if let aps = userInfo["aps"] as? NSDictionary{
            
            if let a = aps["alert"] as? NSDictionary{
                
                if let body = a.value(forKey: "body") as? String {
                    
                    alert = body
                }
            }
        }
        
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let t = params.value(forKey: "type") as? Int {
            type = t
        }
        return type
    }
}


// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        let state = UIApplication.shared.applicationState
        
        if state == .background || state == .inactive {
            print("app in background")
        }else if state == .active {
            if self.getNotification(from: userInfo) == 20 {
                
                let data = JSON.init(parseJSON: JSON(userInfo["data"]!).rawString()!)
                self.showDriverInfos(json: data)
            } else if self.getNotification(from: userInfo) == 21{
                
                NotificationCenter.default.post(name: Notification.Name("race_postponed"), object: nil)

                let banner = NotificationBanner(title: NSLocalizedString("No Driver Available Now in this Category", comment: ""), subtitle: NSLocalizedString("Try in another category.", comment: ""), style: .danger)
                banner.show()
            }
        }

        // Don't alert the user for other types.
        completionHandler(UNNotificationPresentationOptions(rawValue: 0))
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        // Get the meeting ID from the original notification.
        
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        let state = UIApplication.shared.applicationState
        
        if state == .background || state == .inactive {
            print("app in background")
            if self.getNotification(from: userInfo) == 20 {
                let data = JSON.init(parseJSON: JSON(userInfo["data"]!).rawString()!)
                self.showDriverInfos(json: data)
            } else if self.getNotification(from: userInfo) == 21{
                NotificationCenter.default.post(name: Notification.Name("race_postponed"), object: nil)
                
                let banner = NotificationBanner(title: NSLocalizedString("No Driver Available Now in this Category", comment: ""), subtitle: NSLocalizedString("Try in another category.", comment: ""), style: .danger)
                banner.show()
            }
        }
        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.set(fcmToken, forKey: "RegistrationToken")

//        if let user = Auth.auth().currentUser {
//            User.updateDeviceToken(user.uid, fcmToken)
//            print("device token driver updated : ",fcmToken)
//        }else {
//            UserDefaults.standard.set(fcmToken, forKey: "RegistrationToken")
//        }
        
        //let dataDict:[String: String] = ["token": fcmToken]
        //NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
