//
//  UpgradeViewController.swift
//  MyDriver
//
//  Created by sami hazel on 6/27/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth

class UpgradeViewController: UIViewController {

    static let storyboardId = "UpgradeViewController"

    @IBOutlet weak var upgradeMondatoryButton: UIButton!
    
    @IBOutlet weak var skipButton: UIButton!
    
    @IBOutlet weak var upgradeButton: UIButton!
    @IBOutlet weak var viewHolder: UIView!

    var isUpgradeMondatory = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        upgradeButton.layer.borderWidth = 1
        upgradeButton.layer.borderColor = UIColor.gray.cgColor
        
        upgradeMondatoryButton.layer.borderWidth = 1
        upgradeMondatoryButton.layer.borderColor = UIColor.gray.cgColor
        
        skipButton.layer.borderWidth = 1
        skipButton.layer.borderColor = UIColor.gray.cgColor
        
        if isUpgradeMondatory {
            viewHolder.isHidden = true
            upgradeButton.isHidden = true
            upgradeButton.isEnabled = false
            skipButton.isHidden = true
            skipButton.isEnabled = false
        } else {
            upgradeMondatoryButton.isHidden = true
            upgradeMondatoryButton.isEnabled = false
        }
    }
    
    @available(iOS 10.0, *)
    @IBAction func upgrade(_ sender: Any) {
        let appleID = "1467444883"
        let url = "itms-apps://itunes.apple.com/app/id"
        UIApplication.shared.open(URL(string: url+appleID)!, options: [:]) { (completion) in
            
        }
    }
    
    @IBAction func skip(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if Auth.auth().currentUser != nil {
            self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController , animated: true, completion: nil)

        } else {
            self.present(storyboard.instantiateViewController(withIdentifier: DemoViewController.storyboardId) as! DemoViewController, animated: true, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
